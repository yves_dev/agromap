<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//*****************************************************************************************************************************************
//*************FRONT END******************************************************************************************************************
//*****************************************************************************************************************************************

Route::group(['prefix'=>'{langue}'],function (){

    Route::get('/acceuil', ['as'=>'acceuil_visiteur','uses'=>'front\frontController@index']);

    //******************************************LA NEWSLETTER****************************************//
    Route::get('/souscrire-newsletter', ['as'=>'souscrire_newsletter','uses'=>'front\frontController@souscrire_newsletter']);


    //******************************************LE BLOG****************************************//
    Route::get('/agronews', ['as'=>'acceuil_blog','uses'=>'front\frontController@blog']);
    Route::get('/agronews/{id_article}', ['as'=>'lire_article','uses'=>'front\frontController@lire_article']);

    //******************************************LES SERVICES****************************************//
    Route::get('/services/', ['as'=>'services','uses'=>'front\frontController@service']);
    Route::get('/service/{id_service}', ['as'=>'un_service','uses'=>'front\frontController@un_service']);

    //******************************************LES FILIERES****************************************//
    Route::get('/filieres', ['as'=>'filieres','uses'=>'front\frontController@filieres']);

    //******************************************LES PROJETS****************************************//
    Route::get('/projets', ['as'=>'projets','uses'=>'front\frontController@projets']);
    Route::get('/projets_pays/{id_pays}', ['as'=>'projets_pays','uses'=>'front\frontController@projets_pays']);
    Route::get('/projets/{id_projet}', ['as'=>'un_projet','uses'=>'front\frontController@un_projet']);


    //******************************************A PROPOS****************************************//
    Route::get('/a-propos-de-nous', ['as'=>'a_propos','uses'=>'front\frontController@a_propos']);

    //******************************************CONTACTEZ NOUS****************************************//
    Route::get('/contact', ['as'=>'contact','uses'=>'front\frontController@contact']);


});
Route::redirect('/',\route('acceuil_visiteur',app()->getLocale()));

//************************************************************************************************************************************************************************
//**************BACK END******************************************************************************************************************************
//******************************************************************************************************************************



    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin','middleware'=>'auth'],function () {
//******************************************AUTHENTIFICATION ****************************************//

    Route::get('/newsletter', ['as' => 'newsletter', 'uses' => 'admin\newsletterController@newsletter']);
    Route::post('/envoyer_mail', ['as' => 'envoyer_mail', 'uses' => 'admin\newsletterController@envoyer_mail']);
    Route::post('/ajouter-email', ['as' => 'ajouter_email', 'uses' => 'admin\newsletterController@ajouter_email']);
    Route::delete('/effacer-email/{id_email}', ['as' => 'effacer_email', 'uses' => 'admin\newsletterController@effacer_email']);


//****************************//
//UTILISATEURS
//*****************************//
//code merdique
//tu as editer regiterUsers.php de laravel lui meme pour ne pas rediriger su home et ne pas connecté l'utilisateur apres son enregistrement
    Route::get('/profil', ['as' => 'profil', 'uses' => 'auth\UpdateProfileController@profil'])->middleware(['auth', 'password.confirm']);
    Route::post('/modifier-profil', ['as' => 'modifier_profil', 'uses' => 'auth\UpdateProfileController@modifier_profil'])->middleware(['auth', 'password.confirm']);
    Route::get('/gerer-utilisateur', ['as' => 'gestion_utilisateur', 'uses' => 'auth\UpdateProfileController@gestion_utilisateur'])->middleware(['auth', 'password.confirm']);
    Route::put('/modifier-type-utilisateur/{id_utilisateur}', ['as' => 'modifier_type_utilisateur', 'uses' => 'auth\UpdateProfileController@modifier_type_utilisateur']);
    Route::delete('/effacer-utilisateur/{id_utilisateur}', ['as' => 'effacer_utilisateur', 'uses' => 'auth\UpdateProfileController@effacer_utilisateur']);


//****************************//
//GESTION REGION ET PAYS
//*****************************//
//GET
    Route::get('/gestion-region', ['as' => 'pays_region', 'uses' => 'pays_regionController@pays_region']);
//POST
    Route::post('/ajouter-pays', ['as' => 'ajouter_pays', 'uses' => 'pays_regionController@ajouter_pays']);
    Route::post('/modifier-pays/{id_pays}', ['as' => 'modifier_pays', 'uses' => 'pays_regionController@modifier_pays']);
    Route::delete('/effacer-pays/{id_pays}', ['as' => 'effacer_pays', 'uses' => 'pays_regionController@effacer_pays']);

    Route::post('/ajouter-region', ['as' => 'ajouter_region', 'uses' => 'pays_regionController@ajouter_region']);
    Route::post('/modifier-region/{id_region}', ['as' => 'modifier_region', 'uses' => 'pays_regionController@modifier_region']);
    Route::delete('/effacer-region/{id_region}', ['as' => 'effacer_region', 'uses' => 'pays_regionController@effacer_region']);


//DELETE


//****************************//
//GESTION DES PROJET
//*****************************//
//*************GET****************
    Route::get('/gestion-projets', ['as' => 'gestion_projet', 'uses' => 'projetController@projet']);
    Route::get('/editer-projet/{id_versionning}', ['as' => 'editer_projet', 'uses' => 'projetController@editer_projet']);

//********post***********
    Route::post('/ajouter-projet', ['as' => 'ajouter_projet', 'uses' => 'projetController@ajouter_projet']);
    Route::post('/modifier-projet/{id_versionning}', ['as' => 'modifier_projet', 'uses' => 'projetController@modifier_projet']);
    Route::delete('/effacer-projet/{id_versionning}', ['as' => 'effacer_projet', 'uses' => 'projetController@effacer_projet']);


//****************************//
//GESTION DES SERVICES
//*****************************//
//*************GET****************

    Route::get('/gestion-services', ['as' => 'gestion_service', 'uses' => 'serviceController@service']);
    Route::get('/editer-service/{id_verionning}', ['as' => 'editer_service', 'uses' => 'serviceController@editer_service']);

//********post***********
    Route::post('/ajouter-service', ['as' => 'ajouter_service', 'uses' => 'serviceController@ajouter_service']);
    Route::post('/modifier-service/{id_versionning}', ['as' => 'modifier_service', 'uses' => 'serviceController@modifier_service']);
    Route::delete('/effacer-service/{id_versionning}', ['as' => 'effacer_service', 'uses' => 'serviceController@effacer_service']);


//****************************//
//GESTION DES FILIERES
//*****************************//
//*************GET****************

    Route::get('/gestion-filieres', ['as' => 'gestion_filiere', 'uses' => 'filiereController@filiere']);
    Route::get('/editer-filiere/{id_service}', ['as' => 'editer_filiere', 'uses' => 'filiereController@editer_filiere']);

//********post***********
    Route::post('/ajouter-filiere', ['as' => 'ajouter_filiere', 'uses' => 'filiereController@ajouter_filiere']);
    Route::post('/modifier-filiere/{id_versionning}', ['as' => 'modifier_filiere', 'uses' => 'filiereController@modifier_filiere']);
    Route::delete('/effacer-filiere/{id_versionning}', ['as' => 'effacer_filiere', 'uses' => 'filiereController@effacer_filiere']);


//****************************//
//GESTION DES PARTENAIRES
//*****************************//
//*************GET****************
    Route::get('/gestion-partenaires', ['as' => 'gestion_partenaire', 'uses' => 'partenaireController@partenaire']);
    Route::get('/editer-partenaire/{id_versionning}', ['as' => 'editer_partenaire', 'uses' => 'partenaireController@editer_partenaire']);

//********post***********
    Route::post('/ajouter-partenaire', ['as' => 'ajouter_partenaire', 'uses' => 'partenaireController@ajouter_partenaire']);
    Route::post('/modifier-partenaire/{id_partenaire}', ['as' => 'modifier_partenaire', 'uses' => 'partenaireController@modifier_partenaire']);
    Route::delete('/effacer-partenaire/{id_partenaire}', ['as' => 'effacer_partenaire', 'uses' => 'partenaireController@effacer_partenaire']);


//****************************////***************Entreprise*************////****************************//
//****************************////***************Entreprise*************////****************************//
//GESTION de l' Equipes
//*****************************//
//*************GET****************
    Route::get('/gestion-equipe', ['as' => 'gestion_equipe', 'uses' => 'equipeController@equipe']);
    //********post***********
    Route::post('/ajouter-equipe', ['as' => 'ajouter_a_equipe', 'uses' => 'equipeController@ajouter_a_equipe']);
    Route::put('/modifier-menbre-equipe/{id_versionning}', ['as' => 'modifier_menbre_equipe', 'uses' => 'equipeController@modifier_menbre_equipe']);
    Route::delete('/effacer-menbre-equipe/{id_versionning}', ['as' => 'effacer_menbre_equipe', 'uses' => 'equipeController@effacer_menbre_equipe']);


//*****************************//
//GESTION a parametre
//*****************************//
    Route::get('/gestion-parametre', ['as' => 'gestion_parametre', 'uses' => 'parametreController@parametre']);
    Route::post('/modifier-parametre', ['as' => 'modifier_parametre', 'uses' => 'parametreController@modifier_parametre']);
//*****************************//
//GESTION SLIDER
//*****************************//
    Route::get('/gestion-slider', ['as' => 'gestion_slider', 'uses' => 'sliderController@slider']);
    Route::get('/editer-slide/{id_slide}', ['as' => 'editer_slide', 'uses' => 'sliderController@editer_slide']);

    Route::post('/ajouter-au-slider', ['as' => 'ajouter_au_slider', 'uses' => 'sliderController@ajouter_au_slider']);
    Route::put('/modifier-slider/{id_slide}', ['as' => 'modifier_slide', 'uses' => 'sliderController@modifier_slide']);
    Route::delete('/effacer-un-slide/{id_slide}', ['as' => 'effacer_un_slide', 'uses' => 'sliderController@effacer_un_slide']);
//*****************************//
//GESTION STATISTIQUE
//*****************************//
    Route::get('/gestion-statistique', ['as' => 'gestion_statistique', 'uses' => 'statistiqueController@statistique']);
    Route::get('/editer-statistique/{id_statistique}', ['as' => 'editer_statistique', 'uses' => 'statistiqueController@editer_statistique']);

    Route::post('/ajouter-au-statistiques', ['as' => 'ajouter_au_statistique', 'uses' => 'statistiqueController@ajouter_au_statistique']);
    Route::put('/modifier-statistique/{id_slide}', ['as' => 'modifier_statistique', 'uses' => 'statistiqueController@modifier_statistique']);
    Route::delete('/effacer-une-statistique/{id_slide}', ['as' => 'effacer_une_statistique', 'uses' => 'statistiqueController@effacer_une_statistique']);

//*****************************//
//GESTION BLOG
//*****************************//
    Route::get('/blog', ['as' => 'gestion_blog', 'uses' => 'blogController@blog']);
    Route::get('/editer-article/{id_versionning}', ['as' => 'editer_article', 'uses' => 'blogController@editer_article']);

    Route::post('/ajouter-au-blog', ['as' => 'ajouter_au_blog', 'uses' => 'blogController@ajouter_au_blog']);
    Route::put('/modifier-article/{id_slide}', ['as' => 'modifier_article', 'uses' => 'blogController@modifier_article']);
    Route::delete('/effacer-un-article/{id_slide}', ['as' => 'effacer_un_article', 'uses' => 'blogController@effacer_un_article']);

//*****************************//
//GESTION a propos
//*****************************//
    Route::get('/gestion-apropos', ['as' => 'gestion_apropos', 'uses' => 'aproposController@apropos']);
    Route::post('/modifier-apropos', ['as' => 'modifier_apropos', 'uses' => 'aproposController@modifier_apropos']);

});
