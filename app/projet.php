<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class projet extends Model
{
    protected $table = 'projets';
    protected $guarded = [];

    public function region(){
        return $this->belongsTo(region::class);
    }
}
