<?php

namespace App;
use App\pays;

use Illuminate\Database\Eloquent\Model;

class region extends Model
{
    protected $table = 'regions';
    protected $fillable = ['nom_region','pays_id'];

    public function pays(){
        return $this->belongsTo(pays::class);
    }

    public function projet(){
        return $this->hasMany(projet::class);
    }
}
