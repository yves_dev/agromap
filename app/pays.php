<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pays extends Model
{
    protected $fillable = ['nom_pays'];

    public function region(){
        return $this->hasMany(region::class);
    }
}

