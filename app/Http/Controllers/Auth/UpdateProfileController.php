<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UpdateProfileController extends Controller
{


    public function profil(Guard $auth)
    {
        return view('admin/user/profil');
    }

    public function modifier_profil(Request $request){
        $donneesFormulaire = $request->all();
        $id_utilisateur = $donneesFormulaire['id_utilisateur'];
        $le_user = User::find($id_utilisateur);

        $nom = $donneesFormulaire['nom'];
        $email = $donneesFormulaire['email'];
        $nouveau_mdp =  Hash::make($donneesFormulaire['nouveau_mdp']);

            $le_user->name = $nom;
            $le_user->email = $email;

            if($nouveau_mdp !=''){
                $le_user->password = $nouveau_mdp;
            }

            if($le_user->save()){
                $notif = '<div class="alert alert-success text-center"> Les modification ont été effectuée </div>';
               return redirect(route('profil'))->with(['notification'=>$notif]);
            }else{
                $notif = '<div class="alert alert-danger text-center"> Quelque chose s\'est mal passé... </div>';
                return redirect(route('profil'))->with(['notification'=>$notif]);
            }

    }

    public function gestion_utilisateur(){
        if(Auth::user()->type != 'admin'){
            return redirect(route('home'));
        }else{
            $liste_utilisateur = User::where('id','<>',auth()->id())->get();
//            dd($liste_utilisateur);
            return view('admin/user/gerer_utilisateur',compact('liste_utilisateur'));
        }

    }

    public function effacer_utilisateur($id){
        if(Auth::user()->type != 'admin'){
            return redirect(route('home'));
        }else {
            $notif = '<div class="alert alert-success text-center"> La suppression a été effectuée </div>';
            $user = User::find($id);
            if ($user != null) {
                if (!$user->delete()) {
                    $notif = '<div class="alert alert-danger text-center"> Quelque chose s\'est mal passé... </div>';
                }
            }
            return redirect(route('gestion_utilisateur'))->with(['notification' => $notif]);
        }

    }

    public function modifier_type_utilisateur(Request $request,$id){
        if(Auth::user()->type != 'admin'){
            return redirect(route('home'));
        }else {
            $notif = '<div class="alert alert-success text-center"> La Modification a été effectuée </div>';
            $user = User::find($id);
            if ($user != null) {
                $df = $request->all();
                $user->type = $df['type'];
                $user->creer_par = Auth::user()->id;
                if (!$user->save()) {
                    $notif = '<div class="alert alert-danger text-center"> Quelque chose s\'est mal passé... </div>';
                }
            }
            return redirect(route('gestion_utilisateur'))->with(['notification' => $notif]);
        }

    }

}
