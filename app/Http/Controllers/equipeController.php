<?php

namespace App\Http\Controllers;

use App\equipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class equipeController extends Controller
{

    public function equipe(){
        $equipe = equipe::all()->groupBy('id_versionning');
//        dump($les_services[0]['id_versionning']);
//        die();
        return view('admin/equipe',compact('equipe'));
    }

    public function ajouter_a_equipe(Request $request){
        $id_versionning = date('dhms');//utiliser pour savoir que les deux equipe sont en fait differente version du meme equipe
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/equipes/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('photo')){
            $photo = $request->file('photo');
            $extension = $photo->getClientOriginalExtension();
            $time = date('dhms');
            $nom_photo = $time . '-'. $photo->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $photo->move($chemin_destination,$nom_photo);
                $nom_photo = $destination.$nom_photo;
            }
        }else{
            $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Photo obligatoire / Photo is required</div>";
            return redirect(route('gestion_equipe'))->withInput($request->input())->with(['notification'=>$notification]);
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $le_equipe_fr = new equipe();
        $le_equipe_fr->nom_personne = $donnees_formulaire['nom_complet'];
        $le_equipe_fr->fonction = $donnees_formulaire['fonction_fr'];
        $le_equipe_fr->description = $donnees_formulaire['description_fr'];
        $le_equipe_fr->url_photo = $nom_photo;

        $le_equipe_fr->langue = 'fr';
        $le_equipe_fr->id_versionning = $id_versionning;

        if($le_equipe_fr->save()){
//                Enregistrer version anglaise
            $le_equipe_en = new equipe();
            $le_equipe_en->nom_personne = $donnees_formulaire['nom_complet'];
            $le_equipe_en->fonction = $donnees_formulaire['fonction_en'];
            $le_equipe_en->description = $donnees_formulaire['description_en'];
            $le_equipe_en->url_photo = $nom_photo;

            $le_equipe_en->langue = 'en';
            $le_equipe_en->id_versionning = $id_versionning;
            if($le_equipe_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Menbre enregistré avec succes <br/> Menber Recorded succesfully
                            </div>";
                return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
    }

//    **********************************************************************************************************************************

    public function modifier_menbre_equipe(Request $request,$id_versionning){
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/equipes/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('photo')){
            $photo = $request->file('photo');
            $extension = $photo->getClientOriginalExtension();
            $time = date('dhms');
            $nom_photo = $time . '-'. $photo->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $photo->move($chemin_destination,$nom_photo);
                $nom_photo = $destination.$nom_photo;
            }
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $id_version_fr = $donnees_formulaire['id_version_fr'];
        $le_equipe_fr = equipe::find($id_version_fr);
        $le_equipe_fr->nom_personne = $donnees_formulaire['nom_complet'];
        $le_equipe_fr->fonction = $donnees_formulaire['fonction_fr'];
        $le_equipe_fr->description = $donnees_formulaire['description_fr'];

        if($request->hasFile('photo')){
            $ancienne_photo = $le_equipe_fr['photo'];
            File::delete(public_path($ancienne_photo));
            $le_equipe_fr->url_photo = $nom_photo;
        }

        $le_equipe_fr->langue = 'fr';
        $le_equipe_fr->id_versionning = $id_versionning;

        if($le_equipe_fr->save()){
//                Enregistrer version anglaise
            $id_version_en = $donnees_formulaire['id_version_en'];
            $le_equipe_en = equipe::find($id_version_en);
            $le_equipe_en->nom_personne = $donnees_formulaire['nom_complet'];
            $le_equipe_en->fonction = $donnees_formulaire['fonction_en'];
            $le_equipe_en->description = $donnees_formulaire['description_en'];

            if($request->hasFile('photo')){
                $ancienne_photo = $le_equipe_en['photo'];
                File::delete(public_path($ancienne_photo));
                $le_equipe_en->url_photo = $nom_photo;
            }

            $le_equipe_en->langue = 'en';
            $le_equipe_en->id_versionning = $id_versionning;
            if($le_equipe_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Menbre enregistré avec succes <br/> Menber Recorded succesfully
                            </div>";
                return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
    }

    public function effacer_menbre_equipe($id_versionning){
        $le_menbre = equipe::all()->where('id_versionning','=',$id_versionning);
        $probleme= false;
        $couleur ='success';
        foreach ($le_menbre as $equipe){
            if(!$equipe->delete()){
                $probleme=true;
                $couleur ='danger';
            }
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Seppression du menbre réussie / Menber Deleted Successfully";
        $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> $message </div>";
        return redirect(route('gestion_equipe'))->with(['notification'=>$notification]);
    }
}
