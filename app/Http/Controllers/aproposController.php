<?php

namespace App\Http\Controllers;

use App\apropos;
use Illuminate\Http\Request;

class aproposController extends Controller
{

    public function apropos(){
        $tri = apropos::all()->where('id_versionning','=',1)->groupBy('id_versionning');
        $apropos = $tri['1'];
//        dump($les_services[0]['id_versionning']);
//        die();
        return view('admin/apropos/index',compact('apropos'));
    }

    public function modifier_apropos(Request $request){
        $id_versionning =1;
        $donnees_formulaire = $request->all();

        $id_version_fr = $donnees_formulaire['id_version_fr'];
        $id_version_en = $donnees_formulaire['id_version_en'];

        $apropos_fr = apropos::find($id_version_fr);
        $apropos_fr->nom_entreprise = $donnees_formulaire['nom_entreprise_fr'];
        $apropos_fr->texte_nous = $donnees_formulaire['nous_fr'];
        $apropos_fr->texte_vision = $donnees_formulaire['vision_fr'];
        if($apropos_fr->save()){

            $apropos_en = apropos::find($id_version_en);
            $apropos_en->nom_entreprise = $donnees_formulaire['nom_entreprise_en'];
            $apropos_en->texte_nous = $donnees_formulaire['nous_en'];
            $apropos_en->texte_vision = $donnees_formulaire['vision_en'];
            if($apropos_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Modifiaction effectuée avec succes <br/> Updated succesfully
                            </div>";
                return redirect(route('gestion_apropos'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_apropos'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec Modification <br/> Updated Failed 
                            </div>";
            return redirect(route('gestion_apropos'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_apropos'))->with(['notification'=>$notification]);


    }
}
