<?php

namespace App\Http\Controllers;

use App\pays;
use App\region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class pays_regionController extends Controller
{


//****************************
//****GESTION REGION ET PAYS
//******************************
    public function pays_region(){
        $les_pays = pays::all();
        $les_regions = region::get();
        return view('admin/pays_region',compact('les_pays','les_regions'));
    }

    public function ajouter_pays(Request $request){
        $donnee_formulaire = $request->only('nom_pays');
        $le_pays = pays::firstOrCreate($donnee_formulaire);
        $notification ="";
        if($le_pays->wasRecentlyCreated === true){
            $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> Enregistrement du pays reussi <br/> Record succesfully</div>";
        }else{
            $notification = "<div class='alert-danger text-center' style='font-size: 18px; font-weight: bold'>Le pays existe déja <br/> Country already exist</div>";
        };
        Session::flash('notification',$notification);
        return redirect(route('pays_region'));
    }

    //**************************************************************EN DELETE**************************************************************
    public function effacer_pays($id_pays){
        $le_pays = pays::find($id_pays);
        $probleme= false;
        if(!$le_pays->delete()){
            $probleme=true;
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Suppression du pays réussie / Delete Successfully";
        $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
        return redirect(route('gestion_pays_region'))->with(['notification'=>$notification]);
    }
    
    public function modifier_pays(Request $request,$id_pays){
        $donnee_formulaire = $request->only('nom_pays');
        $le_pays = pays::find($id_pays);
        $le_pays->nom_pays = $donnee_formulaire['nom_pays'];
        $notification ="";
        if($le_pays->save()){
            $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> Modifiaction reussie <br/> Updated succesfully</div>";
        }else{
            $notification = "<div class='alert-danger text-center' style='font-size: 18px; font-weight: bold'>Echech modification <br/> Updated failed</div>";
        };
        Session::flash('notification',$notification);
        return redirect(route('pays_region'));
    }




    public function ajouter_region(Request $request){
        $donnee_formulaire = $request->only('nom_region','pays_id');
        $la_region = region::firstOrCreate($donnee_formulaire);
        $notification ="";
        if($la_region->wasRecentlyCreated === true){
            $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Enregistrement de la region reussi <br/> Region Record succesfully
                            </div>";
        }else{
            $notification = "<div class='alert-danger text-center' style='font-size: 18px; font-weight: bold'>
                                La region existe déja <br/> Region already exist
                            </div>";
        };
        Session::flash('notification',$notification);
        return redirect(route('pays_region'));
    }
    //**************************************************************EN DELETE**************************************************************
    public function effacer_region($id_region){
        $la_region = region::find($id_region);
        $probleme= false;
        $notification='';
        try{
            if($la_region != null){
                if(!$la_region->delete()){
                    $probleme=true;
                }
                $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Suppression du region réussie / Deleted Successfully";
                $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
            }
        }catch (\Exception $e){}
        return redirect(route('pays_region'))->with(['notification'=>$notification]);
    }

    public function modifier_region(Request $request,$id_region){
        $donnee_formulaire = $request->only('nom_region');
        $le_region = region::find($id_region);
        $le_region->nom_region = $donnee_formulaire['nom_region'];
        $notification ="";
        if($le_region->save()){
            $notification = "<div class='alert-success text-center' style='font-size: 18px; font-weight: bold'> Modifiaction reussie <br/> Updated succesfully</div>";
        }else{
            $notification = "<div class='alert-danger text-center' style='font-size: 18px; font-weight: bold'>Echech modification <br/> Updated failed</div>";
        };
        Session::flash('notification',$notification);
        return redirect(route('pays_region'));
    }







}
