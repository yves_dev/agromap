<?php

namespace App\Http\Controllers;

use App\service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class serviceController extends Controller
{
    public function service(){
        $les_services = service::all()->groupBy('id_versionning');
//        dump($les_services[0]['id_versionning']);
//        die();
        return view('admin/service/service',compact('les_services'));
    }

    public function editer_service($id_versionning){

        $resutat_tri_service = service::all()->where('id_versionning','=',$id_versionning)->groupBy('id_versionning');
        $le_service = $resutat_tri_service[$id_versionning];
//        die();
        return view('admin/service/editer_service',compact('le_service'));
    }

    //**************************************************************EN DELETE**************************************************************
    public function effacer_service($id_versionning){
        $les_services = service::all()->where('id_versionning','=',$id_versionning);
        $probleme= false;
        foreach ($les_services as $service){
            if(!$service->delete()){
                $probleme=true;
            }
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Seppression du service réussie / Delete Successfully";
        $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
        return redirect(route('gestion_service'))->with(['notification'=>$notification]);
    }

    //**************************************************************EN POST**************************************************************
    public function ajouter_service(Request $request){
//        dd($request->all());
        $id_versionning = date('dhms');//utiliser pour savoir que les deux service sont en fait differente version du meme service
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/services/';
        $chemin_destination = public_path($destination);

        $nom_image_section1 = $nom_image_section2 =$nom_image_section3 ='';
        if($request->hasFile('image_section1')){
            $image_section1 = $request->file('image_section1');
            $extension = $image_section1->getClientOriginalExtension();
            $time = date('dhms');
            $nom_image_section1 = $time . '-'. $image_section1->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $image_section1->move($chemin_destination,$nom_image_section1);
                $nom_image_section1 = $destination.$nom_image_section1;
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                La banniere est obligatoire <br/> Banner picture is required
                            </div>";
            return redirect(route('gestion_service'))->with(['notification'=>$notification]);
        }
        if($request->hasFile('image_section2')){
            $image_section2 = $request->file('image_section2');
            $extension = $image_section2->getClientOriginalExtension();
            $time = date('dhms');
            $nom_image_section2 = $time . '-'. $image_section2->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $image_section2->move($chemin_destination,$nom_image_section2);
                $nom_image_section2 = $destination.$nom_image_section2;
            }
        }
        if($request->hasFile('image_section3')){
            $image_section3 = $request->file('image_section3');
            $extension = $image_section3->getClientOriginalExtension();
            $time = date('dhms');
            $nom_image_section3 = $time . '-'. $image_section3->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $image_section3->move($chemin_destination,$nom_image_section3);
                $nom_image_section3 = $destination.$nom_image_section3;
            }
        }


        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $le_service_fr = new service();
        $le_service_fr->nom = $donnees_formulaire['nom_service_fr'];
        $le_service_fr->description = $donnees_formulaire['description_fr'];
        $le_service_fr->text_section1 = $donnees_formulaire['section_fr_1'];
        $le_service_fr->text_section2 = $donnees_formulaire['section_fr_2'];
        $le_service_fr->text_section3 = $donnees_formulaire['section_fr_3'];

        $le_service_fr->image_section1 = $nom_image_section1;

        if($request->hasFile('image_section2')){
            $le_service_fr->image_section2 = $nom_image_section2;
        }

        if($request->hasFile('image_section3')){
            $le_service_fr->image_section3 = $nom_image_section3;
        }

        $le_service_fr->langue = 'fr';
        $le_service_fr->id_versionning = $id_versionning;
        if($le_service_fr->save()){
//                Enregistrer version anglaise
            $le_service_en = new service();
            $le_service_en->nom = $donnees_formulaire['nom_service_en'];
            $le_service_en->description = $donnees_formulaire['description_en'];
            $le_service_en->text_section1 = $donnees_formulaire['section_en_1'];
            $le_service_en->text_section2 = $donnees_formulaire['section_en_2'];
            $le_service_en->text_section3 = $donnees_formulaire['section_en_3'];

            $le_service_en->image_section1 = $nom_image_section1;

            if($request->hasFile('image_section2')){
                $le_service_en->image_section2 = $nom_image_section2;
            }

            if($request->hasFile('image_section3')){
                $le_service_en->image_section3 = $nom_image_section3;
            }

            $le_service_en->langue = 'en';
            $le_service_en->id_versionning = $id_versionning;
            if($le_service_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Service enregistré avec succes <br/> Service Recorded succesfully
                            </div>";
                return redirect(route('gestion_service'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_service'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_service'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_service'))->with(['notification'=>$notification]);
    }

    public function modifier_service(Request $request,$id_versionning){
//        dd($request->all());
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/services/';
        $chemin_destination = public_path($destination);

        $nom_image_section1 = $nom_image_section2 =$nom_image_section3 ='';
        if($request->hasFile('image_section1')){
            $image_section1 = $request->file('image_section1');
            $extension = $image_section1->getClientOriginalExtension();
            $time = date('dhms');
            $nom_image_section1 = $time . '-'. $image_section1->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $image_section1->move($chemin_destination,$nom_image_section1);
                $nom_image_section1 = $destination.$nom_image_section1;
            }
        }

        if($request->hasFile('image_section2')){
            $image_section2 = $request->file('image_section2');
            $extension = $image_section2->getClientOriginalExtension();
            $time = date('dhms');
            $nom_image_section2 = $time . '-'. $image_section2->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $image_section2->move($chemin_destination,$nom_image_section2);
                $nom_image_section2 = $destination.$nom_image_section2;
            }
        }
        if($request->hasFile('image_section3')){
            $image_section3 = $request->file('image_section3');
            $extension = $image_section3->getClientOriginalExtension();
            $time = date('dhms');
            $nom_image_section3 = $time . '-'. $image_section3->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $image_section3->move($chemin_destination,$nom_image_section3);
                $nom_image_section3 = $destination.$nom_image_section3;
            }
        }


        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $id_version_fr = $donnees_formulaire['id_version_fr'];
        $le_service_fr = service::find($id_version_fr);
        $le_service_fr->nom = $donnees_formulaire['nom_service_fr'];
        $le_service_fr->description = $donnees_formulaire['description_fr'];
        $le_service_fr->text_section1 = $donnees_formulaire['section_fr_1'];
        $le_service_fr->text_section2 = $donnees_formulaire['section_fr_2'];
        $le_service_fr->text_section3 = $donnees_formulaire['section_fr_3'];


        if($request->hasFile('image_section1')){
            $ancienne_image_section = $le_service_fr['image_section1'];
            File::delete(public_path($ancienne_image_section));
            $le_service_fr->image_section1 = $nom_image_section1;
        }

        if($request->hasFile('image_section2')){
            $ancienne_image_section = $le_service_fr['image_section2'];
            File::delete(public_path($ancienne_image_section));
            $le_service_fr->image_section2 = $nom_image_section2;
        }else if($request->has('retirer_image_section2')){
            $le_service_fr->image_section2 = null;
        }

        if($request->hasFile('image_section3')){
            $ancienne_image_section = $le_service_fr['image_section3'];
            File::delete(public_path($ancienne_image_section));
            $le_service_fr->image_section3 = $nom_image_section3;
        }else if($request->has('retirer_image_section3')){
            $le_service_fr->image_section3 = null;
        }

        $le_service_fr->langue = 'fr';
        $le_service_fr->id_versionning = $id_versionning;
        if($le_service_fr->save()){
//                Enregistrer version anglaise

            $id_version_en = $donnees_formulaire['id_version_en'];
            $le_service_en = service::find($id_version_en);
            $le_service_en->nom = $donnees_formulaire['nom_service_en'];
            $le_service_en->description = $donnees_formulaire['description_en'];
            $le_service_en->text_section1 = $donnees_formulaire['section_en_1'];
            $le_service_en->text_section2 = $donnees_formulaire['section_en_2'];
            $le_service_en->text_section3 = $donnees_formulaire['section_en_3'];

            if($request->hasFile('image_section1')){
                $le_service_en->image_section1 = $nom_image_section1;
            }

            if($request->hasFile('image_section2')){
                $le_service_en->image_section2 = $nom_image_section2;
            }elseif($request->has('retirer_image_section2')){
                File::delete(public_path($le_service_en['image_section2']));
                $le_service_en->image_section2 = null;
            }

            if($request->hasFile('image_section3')){
                $le_service_en->image_section3 = $nom_image_section3;
            }else if($request->has('retirer_image_section3')){
                File::delete(public_path($le_service_en['image_section3']));
                $le_service_en->image_section3 = null;
            }

            $le_service_en->langue = 'en';
            $le_service_en->id_versionning = $id_versionning;
            if($le_service_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Modification reussie <br/> Updated succesfully
                            </div>";
                return redirect(route('editer_service',$id_versionning))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('editer_service',$id_versionning))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec dModification <br/> Updated Failed 
                            </div>";
            return redirect(route('editer_service',$id_versionning))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_service'))->with(['notification'=>$notification]);
    }

}



