<?php

namespace App\Http\Controllers;

use App\blog;
use App\filiere;
use App\model\front\newsletter;
use App\projet;
use App\service;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Guard $auth)
    {


        $projet = projet::all(); $projet = sizeof($projet)/2;//on divise par 2 car on a chaque projet en version fr et en
        $nb_article = blog::all(); $nb_article = sizeof($nb_article)/2;;
        $services = service::all(); $services = sizeof($services)/2;;
        $filieres = filiere::all(); $filieres = sizeof($filieres)/2;
        $nb_email = newsletter::all(); $nb_email = sizeof($nb_email);

        return view('admin/home',compact('projet','nb_article','services','filieres','nb_email'));
    }

}
