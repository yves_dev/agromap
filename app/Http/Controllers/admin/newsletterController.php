<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\model\front\newsletter;
use App\parametre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class newsletterController extends Controller
{
    public function newsletter(){
        $liste_email = newsletter::all();
        $parametre = parametre::first();

        return view('admin/newsletter',compact('liste_email','parametre'));
    }

    public function envoyer_mail(Request $request){
//        dd($request->all());

        $donneeformulaire = $request->all();
        $nom = $donneeformulaire['nom_entreprise'];
        $email_reponse = $donneeformulaire['email_reponse'];
        $sujet = $donneeformulaire['sujet'];
        $message_a_envoyer = $donneeformulaire['message'];

        $liste_email = [];
        $liste_email[] = $email_reponse;
        $emails = \App\model\admin\newsletter::select('email')->get();
        foreach ($emails as $email){
            $liste_email[] = $email['email'];
        }
//        dd($liste_email);

        $echec_mail = '';
        $data = array(
          "name"=>$nom,
          "email"=>$email_reponse,
          "subject"=>$sujet,
          "content"=>$message_a_envoyer
        );
        Mail::send([], [], function ($message) use ($nom,$email_reponse,$sujet,$message_a_envoyer,$liste_email) {
            $message->to($liste_email)
                ->from($email_reponse,$nom)
                ->replyTo($email_reponse)
                ->subject($sujet)
                ->setBody("<html>$message_a_envoyer</html>", 'text/html'); // for HTML rich messages
        });

        if( count(Mail::failures()) > 0 ) {

            foreach(Mail::failures as $email_address) {
                $echec_mail .= "$email_address <br />";
            }

            $message = "<div class='alert-danger' style='padding: 10px;margin-bottom: 5px'> 
                            Quelque chose s'est mal passé...<br/>
                            Echec d'envoi des mail aux adresses suivantes : <br/>
                            $echec_mail;
                        </div>";
        }else {
            $message = "<div class='alert-success' style='padding: 10px;margin-bottom: 5px'> 
                            Votre e-mail a été bien été envoyé à <b>$email_reponse</b> et à tous vos contacts
                    </div>";
        }
        return redirect(route('newsletter'))->with(['notification'=>$message]);
    }

    public function ajouter_email(Request $request){
        $donneeformulaire = $request->only('email');
//        $email = $df['email'];

        \App\model\admin\newsletter::firstOrCreate($donneeformulaire);
        $reponse = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'>Enregistrement reussie</div>";

        return redirect(route('newsletter'))->with(['notification'=>$reponse]);
    }

    public function effacer_email($id_email){
        $email = \App\model\admin\newsletter::find($id_email);
        $reponse = "Echec de Suppression";

        if($email->delete()){
            $reponse = "Suppression reussie";
        }

        return redirect(route('newsletter'))->with(['notification'=>$reponse]);
    }
}
