<?php

namespace App\Http\Controllers;

use App\statistique;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class statistiqueController extends Controller
{
    public function statistique(){
        $les_statistiques = statistique::all()->groupBy('id_versionning');
//        dd($le_statistique);
        return view("admin/statistique/statistique",compact('les_statistiques'));
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function editer_statistique($id_versionning){
        $resutat_tri_statistique = statistique::all()->where('id_versionning','=',$id_versionning)->groupBy('id_versionning');
//        dump($le_filiere[$id_versionning]);//tu comprendra en regardant le resutat du dump
        $la_statistique = $resutat_tri_statistique[$id_versionning];
        return view('admin/statistique/editer_statistique',compact('la_statistique'));
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function effacer_une_statistique($id_versionning){
        $la_statistique = statistique::all()->where('id_versionning','=',$id_versionning);
        $probleme= false;
        foreach ($la_statistique as $item_statistique){
            $ancienne_banniere = $item_statistique['url_image'];
            if(!$item_statistique->delete()){
                $probleme=true;
            }else{
                File::delete(public_path($ancienne_banniere));
            }
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Seppression réussie / Delete Successfully";
        $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
        return redirect(route('gestion_statistique'))->with(['notification'=>$notification]);
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function ajouter_au_statistique(Request $request){
        $id_versionning = date('dhms');//utiliser pour savoir que les deux statistique sont en fait differente version du meme statistique
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/statistique/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('banniere')){
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time .'.'.$extension;
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $banniere->move($chemin_destination,$nom_banniere);
                $nom_banniere = $destination.$nom_banniere;
            }
        }else{
            $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Bannière obligatoire / Banner is required</div>";
            return redirect(route('gestion_statistique'))->withInput($request->input())->with(['notification'=>$notification]);
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $statistique_fr = new statistique();
        $statistique_fr->titre = $donnees_formulaire['titre_fr'];
        $statistique_fr->description = $donnees_formulaire['description_fr'];
        $statistique_fr->url_image = $nom_banniere;

        $statistique_fr->langue = 'fr';
        $statistique_fr->id_versionning = $id_versionning;

        if($statistique_fr->save()){
//                Enregistrer version anglaise
            $le_statistique_en = new statistique();
            $le_statistique_en->titre = $donnees_formulaire['titre_en'];
            $le_statistique_en->description = $donnees_formulaire['description_fr'];
            $le_statistique_en->url_image= $nom_banniere;

            $le_statistique_en->langue = 'en';
            $le_statistique_en->id_versionning = $id_versionning;
            if($le_statistique_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                statistique enregistré avec succes <br/> statistique Recorded succesfully
                            </div>";
                return redirect(route('gestion_statistique'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_statistique'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_statistique'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_statistique'))->with(['notification'=>$notification]);
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function modifier_statistique(Request $request,$id_versionning){
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/statistique/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('banniere')){
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time .'.'.$extension;
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $banniere->move($chemin_destination,$nom_banniere);
                $nom_banniere = $destination.$nom_banniere;
            }
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $id_version_fr = $donnees_formulaire['id_version_fr'];
        $statistique_fr = statistique::find($id_version_fr);
        $statistique_fr->titre = $donnees_formulaire['titre_fr'];
        $statistique_fr->description = $donnees_formulaire['description_fr'];

        if ($request->hasFile('banniere')) {
            $ancienne_banniere = $statistique_fr['url_image'];
            File::delete(public_path($ancienne_banniere));
            $statistique_fr->url_image = $nom_banniere;
        }

        $statistique_fr->langue = 'fr';
        $statistique_fr->id_versionning = $id_versionning;

        if($statistique_fr->save()){
//                Enregistrer version anglaise
            $id_version_en = $donnees_formulaire['id_version_en'];
            $le_statistique_en = statistique::find($id_version_en);
            $le_statistique_en->titre = $donnees_formulaire['titre_en'];
            $le_statistique_en->description = $donnees_formulaire['description_fr'];

            if ($request->hasFile('banniere')) {
                $le_statistique_en->url_image = $nom_banniere;
            }

            $le_statistique_en->langue = 'en';
            $le_statistique_en->id_versionning = $id_versionning;
            if($le_statistique_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Modifier avec succes <br/> updated succesfully
                            </div>";
                return redirect(route('editer_statistique',$id_versionning))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('editer_statistique',$id_versionning))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec modification <br/>  Failed to update
                            </div>";
            return redirect(route('editer_statistique',$id_versionning))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('editer_statistique',$id_versionning))->with(['notification'=>$notification]);
    }
}
