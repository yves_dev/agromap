<?php

namespace App\Http\Controllers;

use App\Partenaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class partenaireController extends Controller
{

//****************************
//****GESTION Partenaire
//******************************
    public function partenaire(){
        $les_partenaires = Partenaire::all();
        return view('admin/partenaire/index',compact('les_partenaires'));
    }

    public function effacer_partenaire($id_partenaire){
        $le_partenaire = partenaire::find($id_partenaire);
          $logo = $le_partenaire['logo'];
          File::delete(public_path($logo));

        $probleme= false;
        $notification='';
        try{
            if($le_partenaire != null){
                if(!$le_partenaire->delete()){
                    $probleme=true;
                }
                $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Suppression du partenaire réussie / Deleted Successfully";
                $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
            }
        }catch (\Exception $e){}
        return redirect(route('gestion_partenaire'))->with(['notification'=>$notification]);
    }
    public function ajouter_partenaire(Request $request){
        $id_versionning = date('dhms');//utiliser pour savoir que les deux partenaire sont en fait differente version du meme partenaire
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/partenaires/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('logo')){
            $logo = $request->file('logo');
            $extension = $logo->getClientOriginalExtension();
            $time = date('dhms');
            $nom_logo = $time . '-'. $logo->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $logo->move($chemin_destination,$nom_logo);
                $nom_logo = $destination.$nom_logo;
            }else{
                $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Le logo doit etre une image / Brand must be picture file</div>";
                return redirect(route('gestion_partenaire'))->withInput($request->input())->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Logo obligatoire / Brand is required</div>";
            return redirect(route('gestion_partenaire'))->withInput($request->input())->with(['notification'=>$notification]);
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $le_partenaire = new partenaire();
        $le_partenaire->nom_partenaire = $donnees_formulaire['nom_partenaire'];
        $le_partenaire->logo = $nom_logo;
        if($le_partenaire->save()){
            $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Enregistrement réussi <br/> Recorded successfully 
                            </div>";
            return redirect(route('gestion_partenaire'))->with(['notification'=>$notification]);
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_partenaire'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_partenaire'))->with(['notification'=>$notification]);
    }



    public function modifier_partenaire(Request $request,$id_partenaire){
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/partenaires/';
        $chemin_destination = public_path($destination);
        $nom_logo='';

        if($request->hasFile('logo')){
            $logo = $request->file('logo');
            $extension = $logo->getClientOriginalExtension();
            $time = date('dhms');
            $nom_logo = $time . '-'. $logo->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $logo->move($chemin_destination,$nom_logo);
                $nom_logo = $destination.$nom_logo;
            }else{
                $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Le logo doit etre une image / Brand must be picture file</div>";
                return redirect(route('gestion_partenaire'))->withInput($request->input())->with(['notification'=>$notification]);
            }
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $le_partenaire = partenaire::find($id_partenaire);
        $le_partenaire->nom_partenaire = $donnees_formulaire['nom_partenaire'];
        if($request->hasFile('logo')){
            $ancien_logo = $le_partenaire['logo'];
            File::delete(public_path($ancien_logo));
            $le_partenaire->logo = $nom_logo;
        }
        if($le_partenaire->save()){
            $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                 Modifiaction reussie <br/> Updated succesfully
                            </div>";
            return redirect(route('gestion_partenaire'))->with(['notification'=>$notification]);
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                  Echec de la modifiaction <br/> Updated Failed 
                            </div>";
            return redirect(route('gestion_partenaire'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_partenaire'))->with(['notification'=>$notification]);
    }

}
