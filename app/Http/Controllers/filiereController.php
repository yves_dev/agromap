<?php

namespace App\Http\Controllers;

use App\filiere;
use Illuminate\Http\Request;

class filiereController extends Controller
{
    public function filiere(){
        $les_filieres = filiere::all()->groupBy('id_versionning');
//        dump($les_filieres[0]['id_versionning']);
//        die();
        return view('admin/filieres/filiere',compact('les_filieres'));
    }

    public function editer_filiere($id_versionning){
        $resutat_tri_filiere = filiere::all()->where('id_versionning','=',$id_versionning)->groupBy('id_versionning');
//        dump($le_filiere[$id_versionning]);//tu comprendra en regardant le resutat du dump
        $la_filiere = $resutat_tri_filiere[$id_versionning];
        return view('admin/filieres/editer_filiere',compact('la_filiere'));
    }
    
    //**************************************************************EN DELETE**************************************************************
    public function effacer_filiere($id_versionning){
        $les_filieres = filiere::all()->where('id_versionning','=',$id_versionning);
        $probleme= false;
        foreach ($les_filieres as $filiere){
            if(!$filiere->delete()){
                $probleme=true;
            }
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Seppression du filiere réussie / Delete Successfully";
        $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
        return redirect(route('gestion_filiere'))->with(['notification'=>$notification]);
    }


    //**************************************************************EN POST**************************************************************
    public function ajouter_filiere(Request $request){
        $id_versionning = date('dhms');//utiliser pour savoir que les deux filiere sont en fait differente version du meme filiere
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/filieres/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('banniere')){
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time . '-'. $banniere->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $banniere->move($chemin_destination,$nom_banniere);
                $nom_banniere = $destination.$nom_banniere;
            }
        }else{
            $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Bannière obligatoire / Banner is required</div>";
            return redirect(route('gestion_filiere'))->withInput($request->input())->with(['notification'=>$notification]);
        }

        $nom_icone =null;
        if($request->hasFile('icone')){
            $icone = $request->file('icone');
            $extension = $icone->getClientOriginalExtension();
            $time = date('dhms');
            $nom_icone = $time . '-'. $icone->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $icone->move($chemin_destination,$nom_icone);
                $nom_icone = $destination.$nom_icone;
            }
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $le_filiere_fr = new filiere();
        $le_filiere_fr->nom = $donnees_formulaire['nom_fr'];
        $le_filiere_fr->description = $donnees_formulaire['description_fr'];
        $le_filiere_fr->banniere = $nom_banniere;
        $le_filiere_fr->icone = $nom_icone;

        $le_filiere_fr->langue = 'fr';
        $le_filiere_fr->id_versionning = $id_versionning;

        if($le_filiere_fr->save()){
//                Enregistrer version anglaise
            $le_filiere_en = new filiere();
            $le_filiere_en->nom = $donnees_formulaire['nom_en'];
            $le_filiere_en->description = $donnees_formulaire['description_en'];
            $le_filiere_en->banniere = $nom_banniere;
            $le_filiere_en->icone = $nom_icone;

            $le_filiere_en->langue = 'en';
            $le_filiere_en->id_versionning = $id_versionning;
            if($le_filiere_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                filiere enregistrer avec succes <br/> Sector Recorded succesfully
                            </div>";
                return redirect(route('gestion_filiere'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_filiere'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_filiere'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_filiere'))->with(['notification'=>$notification]);
    }


//***************************************
//**********Editer une filiere***********
//**************************************
    public function modifier_filiere(Request $request,$id_versionning)
    {


        $id_versionning = date('dhms');//utiliser pour savoir que les deux filiere sont en fait differente version du meme filiere
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/filieres/';
        $chemin_destination = public_path($destination);

        if ($request->hasFile('banniere')) {
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time . '-' . $banniere->getClientOriginalName();
            if (in_array($extension, ['jpg', 'JPG', 'png', 'PNG', 'jpeg', 'JPEG'])) {
                $banniere->move($chemin_destination, $nom_banniere);
                $nom_banniere = $destination . $nom_banniere;
            }
        }

        $nom_icone = null;
        if ($request->hasFile('icone')) {
            $icone = $request->file('icone');
            $extension = $icone->getClientOriginalExtension();
            $time = date('dhms');
            $nom_icone = $time . '-' . $icone->getClientOriginalName();
            if (in_array($extension, ['jpg', 'JPG', 'png', 'PNG', 'jpeg', 'JPEG'])) {
                $icone->move($chemin_destination, $nom_icone);
                $nom_icone = $destination . $nom_icone;
            }
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $id_filiere_version_fr = $donnees_formulaire['id_version_fr'];
        $le_filiere_fr = filiere::find($id_filiere_version_fr);
        $le_filiere_fr->nom = $donnees_formulaire['nom_fr'];
        $le_filiere_fr->description = $donnees_formulaire['description_fr'];
        if ($request->hasFile('banniere')) {
            $le_filiere_fr->banniere = $nom_banniere;
        }
        if ($request->hasFile('icone')) {
            $le_filiere_fr->icone = $nom_icone;
        }

        $le_filiere_fr->langue = 'fr';
        $le_filiere_fr->id_versionning = $id_versionning;

        if ($le_filiere_fr->save()) {
//                Enregistrer version anglaise
            $id_filiere_version_en = $donnees_formulaire['id_version_en'];
            $le_filiere_en = filiere::find($id_filiere_version_en);
            $le_filiere_en->nom = $donnees_formulaire['nom_en'];
            $le_filiere_en->description = $donnees_formulaire['description_en'];
            if ($request->hasFile('banniere')) {
                $le_filiere_en->banniere = $nom_banniere;
            }
            if ($request->hasFile('icone')) {
                $le_filiere_en->icone = $nom_icone;
            }

            $le_filiere_en->langue = 'en';
            $le_filiere_en->id_versionning = $id_versionning;
            if ($le_filiere_en->save()) {
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Modifiactions enregistrées<br/> Updated succesfully
                            </div>";
                return redirect(route('editer_filiere',$id_versionning))->with(['notification' => $notification]);
            } else {
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('editer_filiere',$id_versionning))->with(['notification' => $notification]);
            }
        } else {
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec Modification <br/> Failed to Update
                            </div>";
            return redirect(route('editer_filiere',$id_versionning))->with(['notification' => $notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('editer_filiere',$id_versionning))->with(['notification' => $notification]);
    }
}
