<?php

namespace App\Http\Controllers\front;

use App\apropos;
use App\blog;
use App\equipe;
use App\filiere;
use App\Http\Controllers\Controller;
use App\Http\Middleware\SetLangue;
use App\model\front\newsletter;
use App\parametre;
use App\Partenaire;
use App\pays;
use App\projet;
use App\region;
use App\service;
use App\slider;
use App\statistique;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class frontController extends Controller
{

    private $langue;
    public function __construct(Request $request)
    {
        $this->middleware('langue');
        $langue_actuelle = $request->langue;
        $this->langue = $langue_actuelle;
    }

    public function index(Request $request){
        $titre = "Acceuil";
        $parametre = parametre::orderby('id','DESC')->first();
        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $regions = region::orderby('id','DESC')->take(3)->get();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(8)->get()->where('langue','=',$this->langue);
        $articles = blog::orderby('id','DESC')->take(6)->get()->where('langue','=',$this->langue);
        $statistiques = statistique::orderby('id','DESC')->get()->where('langue','=',$this->langue);
        $partenaires = Partenaire::orderby('id','DESC')->get();


        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }

        $slider = slider::get()->where('langue','=',$this->langue);
        $texte_slide_a_gauche = true;
//        dd($projet);
        return view('front/index',compact('parametre','projets','les_pays_ou_il_ya_des_projet','regions','services','filieres','slider','texte_slide_a_gauche','statistiques','partenaires','articles','titre'));
    }

    public function souscrire_newsletter(Request $request){
        $df = $request->all();
        try{
            newsletter::firstOrCreate($df);
            return "<div style='padding: 10px;font-size: 18px;background-color: #fff;color: #1f2839;margin-bottom: 15px'>
                                Sousription reussie, vous recevrez nos actualités et projets </div>";
        }catch(\Exception $e){
            return '<div style=\'padding: 10px;font-size: 18px;background-color: #fff;color: #1f2839;margin-bottom: 15px\'>
                                Quelque chose s\'est mal passé</div>';
        }
    }


    public function blog($lang){
        $titre = 'Agronews';
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $articles = blog::orderby('id','DESC')->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);


        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }


        return view('front/blog/blog',compact('parametre','articles','les_pays_ou_il_ya_des_projet','filieres','services','titre'));
    }

    public function lire_article($langue,$id){
        $titre = 'Agronews';
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);

        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }

        $un_article = blog::find($id);
//        $les_articles = $resutat_tri_article[$id_versionning];
        //fr
//        $un_article = $les_articles[0];

        $suggestion_articles = blog::orderby('id','DESC')->take(8)->get()->where('langue','=',$this->langue)->where('id','!=',$un_article['id']);
        //en
//        $un_article = blog::orderby('id','DESC')->get()->where('id_versionning','=',$id_versionning)->where('langue','=',$this->langue);
//        dd($un_article);

        return view('front/blog/un_article',compact('id','parametre','un_article','suggestion_articles','les_pays_ou_il_ya_des_projet','filieres','services','titre'));
    }

    public function a_propos($lang){
        $titre = 'A propos';
//*****************FOR MENU**********************************
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);

        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }
//*****************FOR MENU**********************************

        $a_propos = apropos::orderby('id','DESC')->where('langue','=',$this->langue)->first();
        $equipe = equipe::orderby('id','DESC')->get()->where('langue','=',$this->langue);
//        $equipe =$tri_equipe[0];
//        dd($equipe);
        return view('front/a_propos',compact('a_propos','equipe','parametre','services','filieres','les_pays_ou_il_ya_des_projet','titre'));
    }

//*************************************************************************************//
//*********************************************LE SERVICE**********************************//
//*************************************************************************************//
    public function service($lang){
        $titre = 'Services';
//*****************FOR MENU**********************************
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);

        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }
//*****************FOR MENU**********************************

        $tous_les_services = service::orderby('id','DESC')->where('langue','=',$this->langue)->get();
        return view('front/services/services',compact('parametre','services','filieres','les_pays_ou_il_ya_des_projet','tous_les_services','titre'));
    }

    public function un_service($langue,$id){
        $titre = 'Service';
//*****************FOR MENU**********************************
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);

        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }
//*****************FOR MENU**********************************

        $le_service = service::find($id);
//        dd($le_service);
        return view('front/services/un_service',compact('id','parametre','services','filieres','les_pays_ou_il_ya_des_projet','le_service','titre'));
    }


//*************************************************************************************//
//*********************************************FILIERES**********************************//
//*************************************************************************************//
    public function filieres($lang){
        $titre = 'Filieres';
//*****************FOR MENU**********************************
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);

        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }

//*****************FOR MENU**********************************

        $toutes_les_filieres = filiere::orderby('id','DESC')->where('langue','=',$this->langue)->get();
        return view('front/filieres',compact('parametre','services','filieres','les_pays_ou_il_ya_des_projet','toutes_les_filieres','titre'));
    }



//*************************************************************************************//
//*********************************************PROJETS**********************************//
//*************************************************************************************//

    public function projets($lang){
        $titre = 'Projets';
//*****************FOR MENU**********************************
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);

        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }
//*****************FOR MENU**********************************

//        dd($les_pays);
        return view('front/projets/projets',compact('parametre','services','filieres','les_pays_ou_il_ya_des_projet','titre'));
    }


public function projets_pays($lang,$id_pays){
//        dd($id_pays);
        $titre = 'Projets';
        $le_pays = pays::find($id_pays);
        $les_regions_de_pays = $le_pays->region;
        $les_projets_groupe_par_region = [];
        foreach ($les_regions_de_pays as $item_region){
                $les_projets_groupe_par_region[] = $item_region->projet;
        }

//*****************FOR MENU**********************************
    $parametre = parametre::orderby('id','DESC')->first();
    $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
    $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);


    $les_pays = pays::with('region')->get();
    $les_pays_ou_il_ya_des_projet = [];
    foreach($les_pays as $item_pays){
        $a_des_projet = false;
        $liste_regions = $item_pays->region;
        if(sizeof($liste_regions) > 0){
            foreach($liste_regions as $item_region){
                if(sizeof($item_region->projet) > 0){
                    $a_des_projet =true;
                }
            }
        }
        if($a_des_projet){
            $les_pays_ou_il_ya_des_projet[] =$item_pays;
            $a_des_projet = false;
        }
    }
//*****************FOR MENU**********************************
//    $id = $id_pays;
    return view('front/projets/projets_pays',compact('id_pays','parametre','services','filieres','les_pays_ou_il_ya_des_projet','les_projets_groupe_par_region','titre'));
}

    public function un_projet($lang,$id){
        $titre = 'Projet';
//*****************FOR MENU**********************************
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);


        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }
//*****************FOR MENU**********************************

        $le_projet = projet::find($id);
        return view('front/projets/un_projet',compact('id','parametre','services','filieres','les_pays_ou_il_ya_des_projet','le_projet','titre'));
    }


    public function contact($lang){
        $titre = 'Contact';
//*****************FOR MENU**********************************
        $parametre = parametre::orderby('id','DESC')->first();
        $services = service::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
        $filieres = filiere::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);
//        $projets = projet::orderby('id','DESC')->take(10)->get()->where('langue','=',$this->langue);

        $les_pays = pays::with('region')->get();
        $les_pays_ou_il_ya_des_projet = [];
        foreach($les_pays as $item_pays){
            $a_des_projet = false;
            $liste_regions = $item_pays->region;
            if(sizeof($liste_regions) > 0){
                foreach($liste_regions as $item_region){
                    if(sizeof($item_region->projet) > 0){
                        $a_des_projet =true;
                    }
                }
            }
            if($a_des_projet){
                $les_pays_ou_il_ya_des_projet[] =$item_pays;
                $a_des_projet = false;
            }
        }
//*****************FOR MENU**********************************

        return view('front/contact',compact('parametre','services','filieres','les_pays_ou_il_ya_des_projet','titre'));
    }
}
