<?php

namespace App\Http\Controllers;

use App\slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class sliderController extends Controller
{
    public function slider(){
        $le_slider = slider::all()->groupBy('id_versionning');
//        dd($le_slider);
        return view("admin/slider/slider",compact('le_slider'));
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function editer_slide($id_versionning){
        $resutat_tri_slide = slider::all()->where('id_versionning','=',$id_versionning)->groupBy('id_versionning');
//        dump($le_filiere[$id_versionning]);//tu comprendra en regardant le resutat du dump
        $le_slide = $resutat_tri_slide[$id_versionning];
        return view('admin/slider/editer_slide',compact('le_slide'));
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function effacer_un_slide($id_versionning){
        $le_slide = slider::all()->where('id_versionning','=',$id_versionning);
        $probleme= false;
        foreach ($le_slide as $item_slide){
            $ancienne_banniere = $item_slide['url_image'];
            if(!$item_slide->delete()){
                $probleme=true;
            }else{
                File::delete(public_path($ancienne_banniere));
            }
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Seppression réussie / Delete Successfully";
        $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
        return redirect(route('gestion_slider'))->with(['notification'=>$notification]);
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function ajouter_au_slider(Request $request){
        $id_versionning = date('dhms');//utiliser pour savoir que les deux slider sont en fait differente version du meme slider
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/slider/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('banniere')){
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time .'.'.$extension;
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $banniere->move($chemin_destination,$nom_banniere);
                $nom_banniere = $destination.$nom_banniere;
            }
        }else{
            $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Bannière obligatoire / Banner is required</div>";
            return redirect(route('gestion_slider'))->withInput($request->input())->with(['notification'=>$notification]);
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $le_slider_fr = new slider();
        $le_slider_fr->titre = $donnees_formulaire['titre_fr'];
        $le_slider_fr->description = $donnees_formulaire['description_fr'];
        $le_slider_fr->url_image = $nom_banniere;
        $le_slider_fr->text_bouton = $donnees_formulaire['texte_bouton_fr'];
        $le_slider_fr->url_destination_bouton = $donnees_formulaire['url_destination_fr'];

        $le_slider_fr->langue = 'fr';
        $le_slider_fr->id_versionning = $id_versionning;

        if($le_slider_fr->save()){
//                Enregistrer version anglaise
            $le_slider_en = new slider();
            $le_slider_en->titre = $donnees_formulaire['titre_en'];
            $le_slider_en->description = $donnees_formulaire['description_en'];
            $le_slider_en->url_image= $nom_banniere;
            $le_slider_en->text_bouton = $donnees_formulaire['texte_bouton_en'];
            $le_slider_en->url_destination_bouton = $donnees_formulaire['url_destination_en'];

            $le_slider_en->langue = 'en';
            $le_slider_en->id_versionning = $id_versionning;
            if($le_slider_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Slide enregistré avec succes <br/> Slide Recorded succesfully
                            </div>";
                return redirect(route('gestion_slider'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_slider'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_slider'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_slider'))->with(['notification'=>$notification]);
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function modifier_slide(Request $request,$id_versionning){
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/slider/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('banniere')){
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time .'.'.$extension;
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $banniere->move($chemin_destination,$nom_banniere);
                $nom_banniere = $destination.$nom_banniere;
            }
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $id_version_fr = $donnees_formulaire['id_version_fr'];
        $le_slider_fr = slider::find($id_version_fr);
        $le_slider_fr->titre = $donnees_formulaire['titre_fr'];
        $le_slider_fr->description = $donnees_formulaire['description_fr'];
        $le_slider_fr->text_bouton = $donnees_formulaire['texte_bouton_fr'];
        $le_slider_fr->url_destination_bouton = $donnees_formulaire['url_destination_fr'];

        if ($request->hasFile('banniere')) {
            $ancienne_banniere = $le_slider_fr['url_image'];
            File::delete(public_path($ancienne_banniere));
            $le_slider_fr->url_image = $nom_banniere;
        }

        $le_slider_fr->langue = 'fr';
        $le_slider_fr->id_versionning = $id_versionning;

        if($le_slider_fr->save()){
//                Enregistrer version anglaise
            $id_version_en = $donnees_formulaire['id_version_en'];
            $le_slider_en = slider::find($id_version_en);
            $le_slider_en->titre = $donnees_formulaire['titre_en'];
            $le_slider_en->description = $donnees_formulaire['description_en'];
            $le_slider_en->text_bouton = $donnees_formulaire['texte_bouton_en'];
            $le_slider_en->url_destination_bouton = $donnees_formulaire['url_destination_en'];

            if ($request->hasFile('banniere')) {
                $le_slider_en->url_image = $nom_banniere;
            }

            $le_slider_en->langue = 'en';
            $le_slider_en->id_versionning = $id_versionning;
            if($le_slider_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Modifier avec succes <br/> Slide updated succesfully
                            </div>";
                return redirect(route('editer_slide',$id_versionning))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('editer_slide',$id_versionning))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec modification <br/>  Failed to update
                            </div>";
            return redirect(route('editer_slide',$id_versionning))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('editer_slide',$id_versionning))->with(['notification'=>$notification]);
    }
}
