<?php

namespace App\Http\Controllers;

use App\pays;
use App\projet;
use App\region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class projetController extends Controller
{
//**************************************************************EN GET**************************************************************
//*******************************
//**********index***********
//*******************************
    public function projet(){
        $les_pays = pays::all();
        $les_regions = region::all()->sortBy('pays_id');
        $les_projets = projet::all()->groupBy('id_versionning');
//        var_dump($les_projets);
//        die();
        return view('admin/projets/projet',compact('les_regions','les_projets','les_pays'));
    }
    public function editer_projet($id_versionning){
        $les_pays = pays::all();
        $les_regions = region::all()->sortBy('pays_id');
        $resutat_tri_projet = projet::all()->where('id_versionning','=',$id_versionning)->groupBy('id_versionning');
//        dump($le_projet[$id_versionning]);//tu comprendra en regardant le resutat du dump
        $le_projet = $resutat_tri_projet[$id_versionning];
        return view('admin/projets/editer_projet',compact('le_projet','les_regions','les_pays'));
    }

//**************************************************************EN DELETE**************************************************************
    public function effacer_projet($id_versionning){
        $les_projets = projet::all()->where('id_versionning','=',$id_versionning);
        $probleme= false;
        foreach ($les_projets as $projet){
            if(!$projet->delete()){
                $probleme=true;
            }
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Seppression du projet réussie / Delete Successfully";
        $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
        return redirect(route('gestion_projet'))->with(['notification'=>$notification]);
    }


//**************************************************************EN POST**************************************************************
//***************************************
//**********ajouter une projet***********
//**************************************
    public function ajouter_projet(Request $request){
        $id_versionning = date('dhms');//utiliser pour savoir que les deux projet sont en fait differente version du meme projet
        //****Gestion des fichiers**********
                $destination = '/uploaded_image/projets/';
                $chemin_destination = public_path($destination);
                $notification ='';
                if($request->hasFile('image_illustration')){
                    $image_illustration = $request->file('image_illustration');
                    $extension = $image_illustration->getClientOriginalExtension();
                    $time = date('dhms');
                    $nom_image_illustration = $time . '-'. $image_illustration->getClientOriginalName();
                    if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                        $image_illustration->move($chemin_destination,$nom_image_illustration);
                        $nom_image_illustration = $destination.$nom_image_illustration;
                    }
                    else{
                        echo 'pas une image';
                    }
                }else{
                    $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Image d'illustration obligatoire </div>";
//                    return redirect(route('gestion_projet'))->withInput($request->input())->with(['notification'=>$notification]);
                }

                $liste_urls_gallery_image='';
                if($request->hasFile('gallery_image')){
                    $gallery_image = $request->file('gallery_image');
                    $nombre_image = count($gallery_image);
                    echo $nombre_image;
                    for($i=0; $i<$nombre_image ; $i++){
                        $image = $gallery_image[$i];
                        $extension = $image->getClientOriginalExtension();
                        if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                            $time2 = date('dhms');
                            $nom_img =  $time2. '-' .$image->getClientOriginalName();
                            $image->move($chemin_destination,$nom_img);
                            $liste_urls_gallery_image.=$destination.$nom_img.'#';
                        }
                        else{
                            echo 'pas une image';
                        }
                    }

                }
        //****Gestion des donnees**********
                $donnees_formulaire = $request->all();

//************************GESTION INTERNE DES REGION************************/
                if($request->has('est_une_nouvelle_region')){

                    $nom_region = $donnees_formulaire['nom_region'];
                    $id_pays =  $donnees_formulaire['pays_id'];
                    var_dump($nom_region);

                    if($id_pays == '-1'){
                        $notification .= "<div class='alert alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Vous n'avez pays choisit de pays pour la nouvelle region
                            </div>";
                        return redirect(route('gestion_projet'))->with(['notification'=>$notification]);
                    }else{
                        $nvle_region = region::create(["nom_region"=>$nom_region,"pays_id"=>$id_pays]);
                        $id_region = $nvle_region->id;
//                        die();
                    }
                }else{
                    $la_region = region::select()->where('nom_region','=',$donnees_formulaire['nom_region'])->first();
                    if($la_region !=null){
                        $id_region = $la_region->id;
                    }else{
                        $notification .= "<div class='alert alert-danger text-center' style='font-size: 18px; font-weight: bold'> 
                                Region inconnue, si c'est une nouvelle region cochez la case
                            </div>";
                        return redirect(route('gestion_projet'))->with(['notification'=>$notification]);
                    }
                }
//************************GESTION INTERNE DES REGION************************/


//                Enregistrer version francaise
                $le_projet_fr = new projet();
                $le_projet_fr->nom = $donnees_formulaire['nom_fr'];
                $le_projet_fr->etat = $donnees_formulaire['etat_fr'];
                $le_projet_fr->resume = $donnees_formulaire['resume_fr'];
                $le_projet_fr->methode = $donnees_formulaire['methode_fr'];
                $le_projet_fr->objectif = $donnees_formulaire['objectif_fr'];
                $le_projet_fr->resultats_attendus = $donnees_formulaire['resultat_fr'];
                $le_projet_fr->partenaires = $donnees_formulaire['partenaire_fr'];
                $le_projet_fr->text_divers = $donnees_formulaire['divers_fr'];


                $le_projet_fr->periode_travail = $donnees_formulaire['periode_travail'];
                $le_projet_fr->image_illustration =$nom_image_illustration;
                $le_projet_fr->liste_urls_gallerie_image = $liste_urls_gallery_image;
                $le_projet_fr->hectar = $donnees_formulaire['hectar'];
                $le_projet_fr->arbre = $donnees_formulaire['arbre'];
                $le_projet_fr->carbonne = $donnees_formulaire['carbonne'];
                $le_projet_fr->region_id = $id_region;
                $le_projet_fr->youtube_video = $donnees_formulaire['lien_video'];
                $le_projet_fr->langue = 'fr';
                $le_projet_fr->id_versionning = $id_versionning;

               if($le_projet_fr->save()){
//                Enregistrer version anglaise
                   $le_projet_en = new projet();
                   $le_projet_en->nom = $donnees_formulaire['nom_en'];
                   $le_projet_en->etat = $donnees_formulaire['etat_en'];
                   $le_projet_en->resume = $donnees_formulaire['resume_en'];
                   $le_projet_en->methode = $donnees_formulaire['methode_en'];
                   $le_projet_en->objectif = $donnees_formulaire['objectif_en'];
                   $le_projet_en->resultats_attendus = $donnees_formulaire['resultats_en'];
                   $le_projet_en->partenaires = $donnees_formulaire['partenaire_en'];
                   $le_projet_en->region_id = $id_region;
                   $le_projet_en->youtube_video = $donnees_formulaire['lien_video'];
                   $le_projet_en->text_divers = $donnees_formulaire['divers_en'];


                   $le_projet_en->periode_travail = $donnees_formulaire['periode_travail'];
                   $le_projet_en->image_illustration = $nom_image_illustration;
                   $le_projet_en->	liste_urls_gallerie_image = $liste_urls_gallery_image;
                   $le_projet_en->hectar = $donnees_formulaire['hectar'];
                   $le_projet_en->arbre = $donnees_formulaire['arbre'];
                   $le_projet_en->carbonne = $donnees_formulaire['carbonne'];
                   $le_projet_en->langue = 'en';
                   $le_projet_en->id_versionning = $id_versionning;
                   if($le_projet_en->save()){
                       $notification .= "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Projet enregistrer avec succes <br/> Project Recorded succesfully
                            </div>";
                       return redirect(route('gestion_projet'))->with(['notification'=>$notification]);
                   }else{
                       $notification .= "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                       return redirect(route('gestion_projet'))->with(['notification'=>$notification]);
                   }
               }else{
                   $notification .= "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
                   return redirect(route('gestion_projet'))->with(['notification'=>$notification]);
               }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_projet'))->with(['notification'=>$notification]);
    }


//***************************************
//**********Editer une projet***********
//**************************************
    public function modifier_projet(Request $request,$id_versionning){


        //****Gestion des fichiers**********
        $destination = '/uploaded_image/projets/';
        $chemin_destination = public_path($destination);
        $nom_image_illustration = '';
        if($request->hasFile('image_illustration')){
            $image_illustration = $request->file('image_illustration');
            $extension = $image_illustration->getClientOriginalExtension();
            $time = date('dhms');
            $nom_image_illustration = $time . '-'. $image_illustration->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $image_illustration->move($chemin_destination,$nom_image_illustration);
            }
            else{
                echo 'pas une image';
            }
        }
//        else{
//            $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Image d'illustration obligatoire </div>";
//            return redirect(route('gestion_projet'))->withInput($request->input())->with(['notification'=>$notification]);
//        }

        $liste_urls_gallery_image='';
        if($request->hasFile('gallery_image')){
            $gallery_image = $request->file('gallery_image');
            $nombre_image = count($gallery_image);
            echo $nombre_image;
            for($i=0; $i<$nombre_image ; $i++){
                $image = $gallery_image[$i];
                $extension = $image->getClientOriginalExtension();
                if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                    $time2 = date('dhms');
                    $nom_img =  $time2. '-' .$image->getClientOriginalName();
                    $image->move($chemin_destination,$nom_img);
                    echo '***'. $nom_img.'*****';
                    $liste_urls_gallery_image.=$destination.$nom_img.'#';
                }
                else{
                    echo 'pas une image';
                }
            }
        }
        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

        //-----------------gestion de la nouvelle gallerie--------------
        $gallerie_actuelle = explode('#',$donnees_formulaire['liste_img_gallery_actuelle']);
        $nouvelle_gallerie=$donnees_formulaire['liste_img_gallery_actuelle'];
        if($request->has('a_retirer_de_la_gallerie')){
            foreach ($donnees_formulaire['a_retirer_de_la_gallerie'] as $item_a_retirer){
                echo $item_a_retirer;
                if(array_search($item_a_retirer,$gallerie_actuelle)!==false){
                    $cle = array_search($item_a_retirer,$gallerie_actuelle);
                    unset($gallerie_actuelle[$cle]);

                    try {
                        File::delete(public_path($item_a_retirer));
                    }catch (\Exception $e){}
                }
            }
        }
        $gallerie_actuelle_to_string = implode('#',$gallerie_actuelle);
        $nouvelle_gallerie = $gallerie_actuelle_to_string.$liste_urls_gallery_image;
//        echo 'nouvelle gallerie = '.$nouvelle_gallerie;
//        die();


//************************GESTION INTERNE DES REGION************************/
        if($request->has('est_une_nouvelle_region')){

            $nom_region = $donnees_formulaire['nom_region'];
            $id_pays =  $donnees_formulaire['pays_id'];
            var_dump($nom_region);

            if($id_pays == '-1'){
                $notification = "<div class='alert alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Vous n'avez pays choisit de pays pour la nouvelle region
                            </div>";
                return redirect(route('editer_projet'))->with(['notification'=>$notification]);
            }else{
                $nvle_region = region::create(["nom_region"=>$nom_region,"pays_id"=>$id_pays]);
                $id_region = $nvle_region->id;
//                        die();
            }
        }else{
            $la_region = region::select()->where('nom_region','=',$donnees_formulaire['nom_region'])->first();
            if($la_region !=null){
                $id_region = $la_region->id;
            }else{
                $notification = "<div class='alert alert-danger text-center' style='font-size: 18px; font-weight: bold'> 
                                Region inconnue, si c'est une nouvelle region cochez la case
                            </div>";
                return redirect(route('editer_projet'))->with(['notification'=>$notification]);
            }
        }
//************************GESTION INTERNE DES REGION************************/

//                Enregistrer version francaise
        $id_projet_version_fr = $donnees_formulaire['id_version_fr'];
        $le_projet_fr = projet::find($id_projet_version_fr);
        $le_projet_fr->nom = $donnees_formulaire['nom_fr'];
        $le_projet_fr->etat = $donnees_formulaire['etat_fr'];
        $le_projet_fr->resume = $donnees_formulaire['resume_fr'];
        $le_projet_fr->methode = $donnees_formulaire['methode_fr'];
        $le_projet_fr->objectif = $donnees_formulaire['objectif_fr'];
        $le_projet_fr->resultats_attendus = $donnees_formulaire['resultat_fr'];
        $le_projet_fr->partenaires = $donnees_formulaire['partenaire_fr'];
        $le_projet_fr->region_id = $id_region;
        $le_projet_fr->youtube_video = $donnees_formulaire['lien_video'];
        $le_projet_fr->periode_travail = $donnees_formulaire['periode_travail'];
        $le_projet_fr->hectar = $donnees_formulaire['hectar'];
        $le_projet_fr->arbre = $donnees_formulaire['arbre'];
        $le_projet_fr->carbonne = $donnees_formulaire['carbonne'];
        $le_projet_fr->text_divers = $donnees_formulaire['divers_fr'];
        if($request->hasFile('image_illustration')){
            $le_projet_fr->image_illustration = $destination.$nom_image_illustration;
        }
        $le_projet_fr->liste_urls_gallerie_image = $nouvelle_gallerie;


        if($le_projet_fr->save()){
//                Enregistrer version anglaise
            $id_projet_version_en = $donnees_formulaire['id_version_en'];
            $le_projet_en = projet::find($id_projet_version_en);
            $le_projet_en->nom = $donnees_formulaire['nom_en'];
            $le_projet_en->etat = $donnees_formulaire['etat_en'];
            $le_projet_en->resume = $donnees_formulaire['resume_en'];
            $le_projet_en->methode = $donnees_formulaire['methode_en'];
            $le_projet_en->objectif = $donnees_formulaire['objectif_en'];
            $le_projet_en->resultats_attendus = $donnees_formulaire['resultats_en'];
            $le_projet_en->partenaires = $donnees_formulaire['partenaire_en'];
            $le_projet_en->region_id = $id_region;
            $le_projet_en->youtube_video = $donnees_formulaire['lien_video'];
            $le_projet_en->periode_travail = $donnees_formulaire['periode_travail'];
            $le_projet_en->hectar = $donnees_formulaire['hectar'];
            $le_projet_en->arbre = $donnees_formulaire['arbre'];
            $le_projet_en->carbonne = $donnees_formulaire['carbonne'];
            $le_projet_en->text_divers = $donnees_formulaire['divers_en'];
            if($request->hasFile('image_illustration')){
                $le_projet_en->image_illustration = $destination.$nom_image_illustration;
            }
            $le_projet_en->liste_urls_gallerie_image = $nouvelle_gallerie;
            if($le_projet_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Modifiaction reussie <br/> Updated succesfully
                            </div>";
                return redirect(route('editer_projet',[$id_versionning]))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('editer_projet',[$id_versionning]))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec de la modifiaction <br/> Updated Failed 
                            </div>";
            return redirect(route('editer_projet',[$id_versionning]))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('editer_projet',[$id_versionning]))->with(['notification'=>$notification]);
    }
}
