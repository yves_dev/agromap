<?php

namespace App\Http\Controllers;

use App\parametre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class parametreController extends Controller
{
    public function parametre(){
        $les_parametres = parametre::firstOrFail();
        return view('admin/parametre',compact('les_parametres'));
    }

    public function modifier_parametre(Request $request){
        $donnees_formulaire =$request->all();
        $les_parametres = parametre::firstOrFail();
        $destination = '/uploaded_image/parametres/';
        $chemin_destination = public_path($destination);


        if($request->hasFile('logo_entreprise')){
            $logo = $request->file('logo_entreprise');
            $extension = $logo->getClientOriginalExtension();
            $time = date('dhms');
            $nom_logo = $time . '-'. $logo->getClientOriginalName();
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $logo->move($chemin_destination,$nom_logo);
                $nom_logo = $destination.$nom_logo;

                $ancien_logo = $les_parametres['url_logo_entreprise'];
                File::delete(public_path($ancien_logo));
                $les_parametres->url_logo_entreprise = $nom_logo;
            }else{
                $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Le logo doit etre une image / Brand must be picture file</div>";
                return redirect(route('gestion_parametre'))->withInput($request->input())->with(['notification'=>$notification]);
            }
        }

        if($request->hasFile('fav_icone_entreprise')){
            $fav_icone = $request->file('fav_icone_entreprise');
            $extension = $fav_icone->getClientOriginalExtension();
            
            $time = date('dhms');
            $nom_fav_icone = $time . '-'. $fav_icone->getClientOriginalName();
            
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $fav_icone->move($chemin_destination,$nom_fav_icone);
                $nom_fav_icone = $destination.$nom_fav_icone;

                $ancien_fav_icone = $les_parametres['url_fav_icone_entreprise'];
                File::delete(public_path($ancien_fav_icone));
                $les_parametres->url_fav_icone = $nom_fav_icone;
            }else{
                $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Le logo doit etre une image / Brand must be picture file</div>";
                return redirect(route('gestion_parametre'))->withInput($request->input())->with(['notification'=>$notification]);
            }
        }

        $les_parametres->nom_entreprise = $donnees_formulaire['nom_entreprise'];
        $les_parametres->description = $donnees_formulaire['description_entreprise_fr'] . "###" . $donnees_formulaire['description_entreprise_en'];
        $les_parametres->email = $donnees_formulaire['email_entreprise'];
        $les_parametres->telephones = $donnees_formulaire['telephones_entreprise'];
        $les_parametres->adresse = $donnees_formulaire['adresse_entreprise'];
        $les_parametres->horraires = $donnees_formulaire['horraires_entreprise'];
        $les_parametres->lien_facebook = $donnees_formulaire['lien_facebook'];
        $les_parametres->lien_twitter = $donnees_formulaire['lien_twitter'];
        $les_parametres->lien_youtube = $donnees_formulaire['lien_youtube'];
        $les_parametres->lien_linkedin = $donnees_formulaire['lien_linkedin'];

        if($les_parametres->save()){
            $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                 Modification reussie <br/> Updated succesfully
                            </div>";
            return redirect(route('gestion_parametre'))->with(['notification'=>$notification]);
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                  Echec de la modification <br/> Updated Failed 
                            </div>";
            return redirect(route('gestion_parametre'))->with(['notification'=>$notification]);
        }
    }
}
