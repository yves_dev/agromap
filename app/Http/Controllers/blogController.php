<?php

namespace App\Http\Controllers;

use App\blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class blogController extends Controller
{
    public function blog(){
        $les_articles = blog::all()->groupBy('id_versionning');
//        dd($les_articles);
        return view('admin/blog/blog',compact('les_articles'));
    }


//**********************************************************************************************************************
//**********************************************************************************************************************
    public function editer_article($id_versionning){
        $resutat_tri_article = blog::all()->where('id_versionning','=',$id_versionning)->groupBy('id_versionning');
//        dump($le_filiere[$id_versionning]);//tu comprendra en regardant le resutat du dump
        $article = $resutat_tri_article[$id_versionning];
        return view('admin/blog/editer_article',compact('article'));
    }

//**********************************************************************************************************************
//**********************************************************************************************************************
    public function effacer_un_article($id_versionning){
        $articles = blog::all()->where('id_versionning','=',$id_versionning);
        $probleme= false;
        foreach ($articles as $item_article){
            $ancienne_banniere = $item_article['url_image'];
            if(!$item_article->delete()){
                $probleme=true;
            }else{
                File::delete(public_path($ancienne_banniere));
            }
        }
        $message = $probleme ? "Echec de la Suppression / Delete Failed" : "Seppression réussie / Delete Successfully";
        $notification = "<div class='alert-success text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> $message </div>";
        return redirect(route('gestion_blog'))->with(['notification'=>$notification]);
    }


//**********************************************************************************************************************
//**********************************************************************************************************************
    public function ajouter_au_blog(Request $request){
        $id_versionning = date('dhms');//utiliser pour savoir que les deux blog sont en fait differente version du meme blog
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/blog/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('banniere')){
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time .'.'.$extension;
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $banniere->move($chemin_destination,$nom_banniere);
                $nom_banniere = $destination.$nom_banniere;
            }
        }else{
            $notification = "<div class='alert-danger text-center' style='padding: 5px;font-size: 16px;font-weight: bold'> *Bannière obligatoire / Banner is required</div>";
            return redirect(route('gestion_blog'))->withInput($request->input())->with(['notification'=>$notification]);
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $le_blog_fr = new blog();
        $le_blog_fr->titre = $donnees_formulaire['titre_fr'];
        $le_blog_fr->resume = $donnees_formulaire['resume_fr'];
        $le_blog_fr->article = $donnees_formulaire['article_version_fr'];

        $le_blog_fr->video = $donnees_formulaire['video'];
        $le_blog_fr->banniere = $nom_banniere;
        $le_blog_fr->langue = 'fr';
        $le_blog_fr->id_versionning = $id_versionning;

        if($le_blog_fr->save()){
//                Enregistrer version anglaise
            $le_blog_en = new blog();
            $le_blog_en->titre = $donnees_formulaire['titre_en'];
            $le_blog_en->resume = $donnees_formulaire['resume_en'];
            $le_blog_en->article = $donnees_formulaire['article_version_en'];

            $le_blog_en->video = $donnees_formulaire['video'];
            $le_blog_en->banniere= $nom_banniere;
            $le_blog_en->langue = 'en';
            $le_blog_en->id_versionning = $id_versionning;
            if($le_blog_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Article enregistré avec succes <br/> Post Recorded succesfully
                            </div>";
                return redirect(route('gestion_blog'))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('gestion_blog'))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec d'enregistrement <br/> Record Failed 
                            </div>";
            return redirect(route('gestion_blog'))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('gestion_blog'))->with(['notification'=>$notification]);
    }




//**********************************************************************************************************************
//**********************************************************************************************************************
    public function modifier_article(Request $request,$id_versionning){
        //****Gestion des fichiers**********
        $destination = '/uploaded_image/blog/';
        $chemin_destination = public_path($destination);

        if($request->hasFile('banniere')){
            $banniere = $request->file('banniere');
            $extension = $banniere->getClientOriginalExtension();
            $time = date('dhms');
            $nom_banniere = $time .'.'.$extension;
            if(in_array($extension,['jpg','JPG','png','PNG','jpeg','JPEG'])){
                $banniere->move($chemin_destination,$nom_banniere);
                $nom_banniere = $destination.$nom_banniere;
            }
        }

        //****Gestion des donnees**********
        $donnees_formulaire = $request->all();

//                Enregistrer version francaise
        $id_version_fr = $donnees_formulaire['id_version_fr'];
        $article_fr = blog::find($id_version_fr);
        $article_fr->titre = $donnees_formulaire['titre_fr'];
        $article_fr->resume = $donnees_formulaire['resume_fr'];
        $article_fr->article = $donnees_formulaire['article_version_fr'];

        if ($request->hasFile('banniere')) {
            $ancienne_banniere = $article_fr['banniere'];
            File::delete(public_path($ancienne_banniere));
            $article_fr->banniere = $nom_banniere;
        }

        $article_fr->video = $donnees_formulaire['video'];
        $article_fr->langue = 'fr';
        $article_fr->id_versionning = $id_versionning;

        if($article_fr->save()){
//                Enregistrer version anglaise
            $id_version_en = $donnees_formulaire['id_version_en'];
            $article_en = blog::find($id_version_en);
            $article_en->titre = $donnees_formulaire['titre_en'];
            $article_en->resume = $donnees_formulaire['resume_en'];
            $article_en->article = $donnees_formulaire['article_version_en'];

            if ($request->hasFile('banniere')) {
                $ancienne_banniere = $article_en['banniere'];
                File::delete(public_path($ancienne_banniere));
                $article_en->banniere = $nom_banniere;
            }


            $article_en->video = $donnees_formulaire['video'];
            $article_en->langue = 'en';
            $article_en->id_versionning = $id_versionning;
            if($article_en->save()){
                $notification = "<div class='alert-success text-center ' style='font-size: 18px; font-weight: bold'> 
                                Modifier avec succes <br/> article updated succesfully
                            </div>";
                return redirect(route('editer_article',$id_versionning))->with(['notification'=>$notification]);
            }else{
                $notification = "<div class='alert-warning text-center ' style='font-size: 18px; font-weight: bold'> 
                                Version anglaise non sauvergardée <br/> Failed to record English version
                            </div>";
                return redirect(route('editer_article',$id_versionning))->with(['notification'=>$notification]);
            }
        }else{
            $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Echec modification <br/>  Failed to update
                            </div>";
            return redirect(route('editer_article',$id_versionning))->with(['notification'=>$notification]);
        }

        $notification = "<div class='alert-danger text-center ' style='font-size: 18px; font-weight: bold'> 
                                Quelque chose s'est mal passé <br/> Something went wrong
                            </div>";
        return redirect(route('editer_article',$id_versionning))->with(['notification'=>$notification]);
    }
}
