<?php

namespace App\model\front;

use Illuminate\Database\Eloquent\Model;

class newsletter extends Model
{
    protected $table = 'newsletter';
    protected $fillable = ['email'];
}
