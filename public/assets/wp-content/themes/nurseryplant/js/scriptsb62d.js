jQuery(document).ready(function( $ ) {
	"use strict";
	
	$(window).load(function() {
        $('.cssload-container').delay(300).fadeOut('slow');
        $('body').delay(300).css({
            'overflow': 'visible'
        });
    })
	
	$('p').each(function() {
        var $this = jQuery(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0) {
            $this.remove();
        }
    });
	
	var url = $('.slim-wrap').data('image');
	var homelink = $('.slim-wrap').data('homelink');
	$('.slim-wrap ul.menu-items').slimmenu({
        resizeWidth: '991',
        collapserTitle: '',
        easingEffect:'easeInOutQuint',
        animSpeed:'medium',
        indentChildren: true,
        childrenIndenter: '&raquo;'
    });
	
	$( '.panel-row-style' ).each( function () {
		var overlay = $(this).data('overlay');
		var opacity = $(this).data('opacity');
		if(overlay != '' && opacity != ''){
		$(this).prepend( '<div class="overlay-row" style="background: rgba(' +overlay+ ', ' +opacity+ ');">' );
		}
	});
	
	$('.ts-button').each(function () {
		var hoverback = $(this).data('hoverback');
		var currentbg = $(this).data('currentbg');
		var currentbor = $(this).data('currentbor');
		var currentcolor = $(this).data('currentcolor');
		var hovercolor = $(this).data('hovercolor');
		$(this).on('mouseenter',function() {
			$(this).css({'background': hoverback, 'border-color': hoverback, 'color': hovercolor});
			$(this).find('i').css({'color': hovercolor});
		});
		$(this).on('mouseleave',function() {
			$(this).css({'background': currentbg, 'border-color': currentbor, 'color': currentcolor});
			$(this).find('i').css({'color': currentcolor});
		});
	});
	
	$('.site-colors ul > li > a').each(function () {
		var backgroundcolor = $(this).data('backgroundcolor');
		$(this).css({'background': backgroundcolor});
	});
	
	$('.element').each(function () {
		var text1 = $(this).data('text1');
		var text2 = $(this).data('text2');
		var text3 = $(this).data('text3');
		var text4 = $(this).data('text4');
	$(function(){
	  $(".element").typed({
		strings: [text1, text2, text3, text4],
		typeSpeed: 300
	  });
	});
	});
	
	if( $("a[rel^='prettyPhoto'], a[data-rel^='prettyPhoto']").length != 0 ) { 
	
	 $("a[rel^='prettyPhoto'], a[data-rel^='prettyPhoto']").prettyPhoto({hook: 'data-rel', social_tools: false, deeplinking: false});
	
	}
	
	$(".content").fitVids();
	
	var authorContainer = $('body');
    // This will handle stretching the cells.
	
    $('.stretch-content').each(function(){
		
        var $$ = $(this);

        var onResize = function(){

            $$.css({
                'margin-left' : 0,
                'margin-right' : 0,
                'padding-left' : 0,
                'padding-right' : 0
            });

            var leftSpace = $$.offset().left - authorContainer.offset().left;
            var rightSpace = authorContainer.outerWidth() - leftSpace - $$.parent().outerWidth();

            $$.css({
                'margin-left' : -leftSpace,
                'margin-right' : -rightSpace-30,
                'padding-left' : $$.data('stretch-type') === 'full' ? leftSpace : 0,
                'padding-right' : $$.data('stretch-type') === 'full' ? rightSpace : 0
            });
        };

        $(window).resize( onResize );
        onResize();

        $$.css({
            'border-left' : 0,
            'border-right' : 0
        });
    });
	
	// Counter
	$(".count-number").appear(function(){
		$('.count-number').each(function(){
			var datacount = $(this).attr('data-count');
			$(this).find('.counter').delay(6000).countTo({
				from: 10,
				to: datacount,
				speed: 3000,
				refreshInterval: 50,
			});
		});
	});
	
	// Countdown
	$('.daycounter').each(function(){
		var counter_id = $(this).attr('id');
		var counter_type = $(this).data('counter');
		var year = $(this).data('year');
		var month = $(this).data('month');
		var date = $(this).data('date');
		var countDay = new Date();
		countDay = new Date(year, month - 1, date);
		
		if( counter_type == "down" ) {
			$("#"+counter_id).countdown({
				labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Mins', 'Secs'],
				labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Min', 'Sec'],
				until: countDay
			});
		} else if( counter_type == "up" ) {
			$("#"+counter_id).countdown({
				labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Mins', 'Secs'],
				labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Min', 'Sec'],
				since: countDay
			});
		}
		
	});
	
	/* ==============================================
    BACK TOP
    =============================================== */
	
	// Scroll top top
	var offset = 300,
		offset_opacity = 1200,
		scroll_top_duration = 700,
		$back_to_top = $('.dmtop');

	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('dmtop dmtop-show') : $back_to_top.removeClass('dmtop-show cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
	
	$('.ts-slider-center').slick({
	  centerMode: true,
	  centerPadding: '70px',
	  speed: 500,
	  slidesToShow: 5,
	  autoplaySpeed: 2000,
  	  cssEase: 'linear',
	  autoplay: true,
	  nextArrow     : '<button type="button" class="slick-next slick-nav"><i class="fa fa-angle-right"></i></button>',
	  prevArrow     : '<button type="button" class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></button>',
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: false,
			centerMode: true,
			centerPadding: '0',
			slidesToShow: 3
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			arrows: false,
			centerMode: true,
			centerPadding: '0',
			slidesToShow: 1
		  }
		}
	  ]
	});	

});