@extends('partials.base')

@section('style_complementaire')
    <link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>

    <style>
        .conteneur_de_texte_riche{
            word-wrap: break-word;
            max-width: 300px;
            padding: 10px;
        }
        @media screen and (max-width: 300px){
            .conteneur_de_texte_riche{
                max-width: 200px;
            }
        }
        #souscrire{
            color: yellow;
        }

        #footer_horraire{
            display: block;
        }
        #footer_newsletter{
            display: none;
        }

    </style>
@endsection

@section('contenu')

    {{--    /***********************************************************--}}
    {{--                    DEBUT SLIDER--}}
    {{--    /***********************************************************--}}
    <section class="section main-container">
{{--        <div class="homepage-content">--}}
{{--            <div class="container-fluid">--}}
                <div id="pl-22" class="panel-layout">
                    <div id="pg-22-0" class="panel-grid panel-has-style">
                        <div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-22-0"
                             data-stretch-type="full-stretched">
                            <div id="pgc-22-0-0" class="panel-grid-cell">
                                <div id="panel-22-0-0-0"
                                     class="so-panel widget widget_themestall themestall panel-first-child panel-last-child"
                                     data-index="0">
                                    <div class="textwidget">
                                        <div class="home-banner" style="max-height:100vh; overflow-y: hidden">
                                            <div class="owl-carousel owl-banner-slider" data-animatein=""
                                                 data-animateout=""
                                                 data-items="1" data-margin=""
                                                 data-loop="true"
                                                 data-merge="true"
                                                 data-nav="false"
                                                 data-dots="false"
                                                 data-stagepadding=""
                                                 data-mobile="1"
                                                 data-tablet="1"
                                                 data-desktopsmall="1"
                                                 data-desktop="1"
                                                 data-autoplay="true"
                                                 data-delay="6000"
                                                 data-autoheight="900" data-navigation="false" data-rtl="false">

                                                {{--                                /***********************************************************--}}
                                                {{--                                    CAROUSEL ITEMS--}}
                                                {{--                                /***********************************************************--}}
                                                @foreach($slider as $item_slider)
                                                    {{--                                    DEBUT ITEM--}}
                                                    <div class="item">
                                                        <img src="{{$item_slider['url_image']}}" alt=""
                                                             class="img-responsive" width="100%" height="100%">
                                                        <div class="bg-overlay"></div>
                                                        <div class="container slider-content vtop text-left">
                                                            <div class="row hidden-xs">
                                                                <div class="col-md-6 {{ $texte_slide_a_gauche ? "" :"col-md-offset-6"  }} ">
                                                                    <h3 class="main-heading animated"
                                                                        data-animate="fadeInUp"
                                                                        data-animation-delay="1200">
                                                                        {{$item_slider['titre']}}
                                                                    </h3>
                                                                    <p class="animated des-content"
                                                                       data-animate="fadeInUp"
                                                                       data-animation-delay="1700">
                                                                        {{$item_slider['description']}}
                                                                    </p>
                                                                    <br/>
                                                                    @if($item_slider['text_bouton'] != null)
                                                                        @if($item_slider['url_destination_bouton'] != null)
                                                                        <p class="animated" data-animate="fadeInUp" data-animation-delay="2300">
                                                                            <a href="{{$item_slider['url_destination_bouton']}}"
                                                                               class="btn hero-btn"> {{$item_slider['text_bouton']}} </a>
                                                                        </p>
                                                                        @endif
                                                                    @endif
                                                                    {{--                                                    <p class="animated" data-animate="fadeInUp" data-animation-delay="2300">--}}
                                                                    {{--                                                        <a href="../shop/index.html" class="btn hero-btn">Buy Plants</a>--}}
                                                                    {{--                                                    </p>--}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>{{--FIN ITEM--}}
                                                    <?php if ($texte_slide_a_gauche == true) {
                                                        $texte_slide_a_gauche = false;
                                                    } else {
                                                        $texte_slide_a_gauche = true;
                                                    } ?>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{--    /***********************************************************--}}
                    {{--                    FIN SLIDER--
                                                    DEBUT STATISTIQUE--}}
                    {{--    /***********************************************************--}}
                    <div id="pg-22-1" class="panel-grid panel-has-style container">
                        <div class="panel-row-style panel-row-style-for-22-1">
                            <div id="pgc-22-1-0" class="panel-grid-cell">
                                <div id="panel-22-1-0-0"
                                     class="so-panel widget widget_siteorigin-panels-builder panel-first-child panel-last-child"
                                     data-index="1">
                                    <div class="panel-widget-style panel-widget-style-for-22-1-0-0"
                                         style="background-color: #1f2839">
                                        <div class="textwidget">
                                            <div class="row">
                                                @foreach($statistiques as $item_statistique)
                                                    <div class="col-sm-3 stat-wrap text-center">
                                                        <img src="{{$item_statistique['url_image']}}" style="max-width : 64px"/>
                                                        <p class="count-number"
                                                           data-count="{{$item_statistique['description']}}"><span
                                                                    class="counter">{{$item_statistique['description']}}</span>
                                                        </p>
                                                        <small>
                                                            {{$item_statistique['titre']}}
                                                        </small>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{-- ************************************************************************************************************
                            *******************************FIN SECTION STATISTIQUE
                                                        *****SECTION QUI SOMMES NOUS************************************
                    ************************************************************************************************************ --}}
                    <div id="pg-22-3" class="panel-grid panel-has-style container" style="margin-top: 25px; padding: 5px;">

                        <div class="textwidget">
                            <div class="section-big-title text-center title-subtitle"
                                 style="margin-bottom: 0px;">
                                <h3 style="font-size: 42px; color: #6bb42f;">{{__("qui_sommes_nous")}}</h3>
                                {{--                                <p style="color: #1f2839;">This is an example page. It’s different from a blog post because it will</p>--}}
                                <div class="divider-title text-center" style="background: #6bb42f">
                                    <span class="span-1" style="background: #6bb42f"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 15px;margin-bottom: 15px">
                            <div class="col-md-8">
                                <div class="row">
{{--                                    <div class="col-md-6">--}}
                                        <div class="vmiddle hidden-sm hidden-xs" style="width: 50%;margin-left: 5%;text-align: center">
                                            <h1 style="font-size: 4.5em;font-weight:1000;text-transform: uppercase;text-align: center">
                                                {{$parametre['nom_entreprise']}}
                                            </h1>
                                            <a href="{{route('a_propos',app()->getLocale())}}" class="btn hero-btn"> {{__("btn_voir_plus")}} </a>
                                            <br/>
                                        </div>
{{--                                    </div>--}}
                                    <div class="col-md-offset-6 col-md-6" id="divplanteImage">
                                        <img src="/uploaded_image/apropos/static/plante.png" id="planteImage" style="max-width: 100%;height: auto;"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="descriptionEntreprise" style="position: relative;min-height: 300px;" >
                                <?php
                                $desc = explode("###",$parametre['description']);
                                $description_fr = $description_en ="";
                                if (isset($desc[0])){ $description_fr  = $desc[0]; }
                                if (isset($desc[1])){ $description_en  = $desc[1]; }

                                ?>

                                    <div class="vmiddle" style="padding: 5px;left: 10px;right: 10px" >


                                        <h1 class="hidden-sm hidden-md hidden-lg text-center" style="font-size: 2em;font-weight:900"> {{$parametre['nom_entreprise']}} </h1>
                                        <br/>

                                        @if(app()->getLocale() =='fr')
                                            {{$description_fr}}
                                        @elseif(app()->getLocale() == 'en')
                                            {{$description_en}}
                                        @endif

                                        <div class="col-xs-12 hidden-md hidden-lg text-center">
                                            <br/>
                                            <a href="{{route('a_propos',app()->getLocale())}}" class="btn hero-btn"> {{__("btn_voir_plus")}} </a>
                                        </div>
                                    </div>

{{--                                FOR MOBILE--}}


                            </div>
                        </div>
                    </div>
                    {{-- ************************************************************************************************************
                            ************************************FIN SECTION QUI SOMMES NOUS*****
                                                                         DEBUT SECTION SERVICE*******************************
                    ************************************************************************************************************ --}}

                    <div id="pg-22-2" class="panel-grid panel-has-style container">

                        <div id="panel-22-3-0-0" class="so-panel widget widget_themestall themestall panel-first-child"
                             data-index="4">
                            <div class="textwidget">
                                <div class="section-big-title text-center title-subtitle" style="margin-bottom: 0px;">
                                    <h3 style="font-size: 42px; color: #6bb42f;">{{__("nos_services")}}</h3>
                                    {{--                                <p style="color: #1f2839;">This is an example page. It’s different from a blog post because it will</p>--}}
                                    <div class="divider-title text-center" style="background: #6bb42f">
                                        <span class="span-1" style="background: #6bb42f"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="{{--panel-row-style panel-row-style-for-22-2--}} row">
                            @foreach($services as $item_service)
                            <div {{--id="pgc-22-2-0"--}} class="col-md-6" style="height: 370px">
                                <div id="panel-22-2-0-0"
                                     class="so-panel widget widget_themestall themestall panel-first-child panel-last-child"
                                     data-index="2"
                                style="margin:5px 0px ">
                                    <div class="textwidget">
                                        <div class="container-invis woocommerce product-carousel-lists">

                                            <div class="list-wrapper products clearfix">

                                                <div class="owl-hotels owl-carousel owl-theme owl-custom"
                                                     data-column="1" data-rtl="false">
                                                    {{--                ITEM SERVICE************************************--}}
                                                    {{--                    @foreach($services as $item_service)--}}
                                                    <div class="owl-item" style="width:100%">
                                                        <div class="list-item" style="width:100%">
                                                            <a href="{{route('un_service',[app()->getLocale(),$item_service['id']])}}" title="">
                                                                <div class="entry" style="width: 100%;background-color: #6bb42f;background-repeat: no-repeat;height:300px !important;
                                                                                            background-image: url('{{$item_service['image_section1']}}');
                                                                                            background-size:cover;background-position: center ">
{{--                                                                    <img src="{{$item_service['image_section1']}}" style="height:300px !important;" alt="new1">--}}
                                                                </div>{{-- end entry --}}
                                                            </a>
                                                            <div class="product-magni-desc">
                                                                <h4><a href="{{route('un_service',[app()->getLocale(),$item_service['id']])}}" title="">{{$item_service['nom']}}</a></h4>
                                                                <div class="short-description-product">
                                                                    {{Str::limit($item_service['description'],170)}}
                                                                </div>
                                                            </div>

                                                            {{-- end list-desc --}}
                                                        </div>{{-- end list-item --}}
                                                    </div>{{-- end col --}}
                                                    {{--                    @endforeach--}}
                                                    {{--                ITEM SERVICE************************************--}}

                                                </div>{{-- end col --}}

                                                </div>{{-- end row --}}
                                                <div class="product-section-title clearfix">
                                                    <h3>{{$item_service['nom']}}
                                                    </h3>
                                                </div>{{-- end section-title --}}
                                            </div>{{-- end list-wrapper --}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        </div>

                    {{-- ************************************************************************************************************
                            ************************************FIN DEBUT SECTION SERVICE*****
                                                                         DEBUT SECTION FILIERES*******************************
                    ************************************************************************************************************ --}}
                        <?php
                            $filiere_to_array = [];
                            foreach($filieres as $item_filiere){
                                $filiere_to_array[] = $item_filiere;
                            }
                        //    dd($filiere_to_array);
                        ?>

                    <div id="pg-22-8" class="panel-grid panel-has-style container" style="padding-top: 30px">
                        <div class="textwidget">
                            <div class="section-big-title text-center title-subtitle" style="margin-bottom: 0px;">
                                <h3 style="font-size: 42px; color: #6bb42f;">{{__("nos_filieres")}}</h3>
                                <div class="divider-title text-center" style="background: #6bb42f">
                                    <span class="span-1" style="background: #6bb42f"></span>
                                </div>
                            </div>
                        </div>

                        <div class="ts-posts-default-loop-style2 row" style="margin-top:30px;margin-bottom: 60px" >
                            @isset($filiere_to_array[0])
                            <div class="col-md-6 col-sm-12 nth-child wow fadeIn"
                                 data-wow-duration="1s" data-wow-delay="0.2s"
                                 style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;height: 200px">
                                <div class="blog-wrapper clearfix row">
                                    <div class="post-media col-xs-4">
                                        <div class="entry has-thumb">
                                            <img src="{{$filiere_to_array[0]['banniere']}}">
                                        </div>
                                    </div><!-- end media -->
                                    <div class="blog-title col-xs-8">
                                        <h4><a href="{{route('filieres',[app()->getLocale()])}}#{{$filiere_to_array[0]['nom']}}#{{$filiere_to_array[0]['id']}}" title="">{{$filiere_to_array[0]['nom']}}</a></h4>
                                        <p>
                                            {{Str::limit($filiere_to_array[0]['description'],170)}}
                                        </p>
                                    </div><!-- end blog-title -->
                                </div><!-- end clearfix -->
                            </div>
                            @endisset
                            @isset($filiere_to_array[1])
                            <div class="col-md-6 col-sm-12 nth-child wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"
                                 style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-name: fadeIn;height: 200px">
                                <div class="blog-wrapper clearfix row">
                                    <div class="post-media col-xs-4">
                                        <div class="entry has-thumb">
                                            <img src="{{$filiere_to_array[1]['banniere']}}">
                                        </div>
                                    </div><!-- end media -->
                                    <div class="blog-title col-xs-8">
                                        <h4><a href="{{route('filieres',[app()->getLocale()])}}#{{$filiere_to_array[1]['nom']}}#{{$filiere_to_array[1]['id']}}" title="">{{$filiere_to_array[1]['nom']}}</a></h4>

                                        <p>
                                            {{Str::limit($filiere_to_array[1]['description'],170)}}
                                        </p>
                                    </div><!-- end blog-title -->
                                </div><!-- end clearfix -->
                            </div>
                            @endisset
                            @isset($filiere_to_array[2])
                            <div class="col-md-6 col-sm-12 nth-child wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s"
                                 style="visibility: visible; animation-duration: 1s; animation-delay: 0.6s; animation-name: fadeIn;height: 200px">
                                <div class="blog-wrapper clearfix row">
                                    <div class="post-media col-xs-4">
                                        <div class="entry has-thumb">
                                            <img src="{{$filiere_to_array[2]['banniere']}}">
                                        </div>
                                    </div><!-- end media -->
                                    <div class="blog-title col-xs-8">
                                        <h4><a href="{{route('filieres',[app()->getLocale()])}}#{{$filiere_to_array[2]['nom']}}{{$filiere_to_array[2]['id']}}" title="">{{$filiere_to_array[2]['nom']}}</a></h4>

                                        <p>
                                            {{Str::limit($filiere_to_array[2]['description'],170)}}
                                        </p>
                                    </div><!-- end blog-title -->
                                </div><!-- end clearfix -->
                            </div>
                            @endisset
                            @isset($filiere_to_array[3])
                            <div class="col-md-6 col-sm-12 nth-child wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s"
                                 style="visibility: visible; animation-duration: 1s; animation-delay: 0.8s; animation-name: fadeIn;height: 200px">
                                <div class="blog-wrapper clearfix row">
                                    <div class="post-media col-xs-4">
                                        <div class="entry has-thumb">
                                            <img src="{{$filiere_to_array[3]['banniere']}}">
                                        </div>
                                    </div><!-- end media -->
                                    <div class="blog-title col-xs-8">
                                        <h4><a href="{{route('filieres',[app()->getLocale()])}}#{{$filiere_to_array[3]['nom']}}{{$filiere_to_array[3]['id']}}" title="">{{$filiere_to_array[3]['nom']}}</a></h4>

                                        <p>
                                            {{Str::limit($filiere_to_array[3]['description'],170)}}
                                        </p>
                                    </div><!-- end blog-title -->
                                </div><!-- end clearfix -->
                            </div>
                            @endisset
                        </div>
                    </div>
                {{-- ************************************************************************************************************
                            ************************************FIN DEBUT SECTION FILIERES*****
                                                                         DEBUT SECTION PROJETS*****************************************--}}
                    {{--                    *********************************************************************************************************--}}
                    <div id="pg-22-7" class="panel-grid panel-has-style">
                        <div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-22-7"
                             data-stretch-type="full" data-overlay="107,180,47" data-opacity="0.8">
                            <div id="pgc-22-7-0" class="panel-grid-cell">
                                <div id="panel-22-7-0-0"
                                     class="so-panel widget widget_themestall themestall panel-first-child panel-last-child"
                                     data-index="10">
                                    <div class="textwidget">
                                        <div class="section-big-title text-center title-subtitle"
                                             style="margin-bottom: 45px;">
                                            <h3 style="font-size: 42px; color: #fff;">{{__("nos_projets")}}</h3>
                                            <div class="divider-title text-center" style="background: #fff">
                                                <span class="span-1" style="background: #fff"></span>
                                            </div>
                                        </div>
                                        {{--                    **************************************SECTION CARTE*******************************************************************--}}
                                        <div class="container-fluid" id="div_carte">
{{--                                            <div class="recouvrir_carte"></div>--}}
                                            <iframe src="https://www.google.com/maps/d/u/0/embed?mid=18k0aQZesopK2g0zHPLJIXxqY7qa97n8L" width="100%" height="480"></iframe>
                                        </div>
                                        {{--                    **************************************FIN SECTION CARTE /
                                                                                DEBUT # DERNIER PROJETS*******************************************************************--}}
                                        <div class="container">
                                            <div class="row">
                                                <?php $nombre_de_projet_afficher = 0; ?>
                                                @foreach($projets as $item_projet)
                                                    <div class="col-md-6" style="padding:7px;height: 450px !important;margin-bottom: 35px">
                                                        <div class="card" style="background-color: #fff;height: 450px">
                                                            <div style="height: 200px;
                                                                    background-image: url('{{$item_projet['image_illustration']}}');
                                                                    background-size: 100%;
                                                                    background-color: #fff"></div>
{{--                                                            <img class="card-img-top" width="100%" style="height: 200px" src="{{$item_projet['image_illustration']}}" alt="Card image">--}}
                                                            <div class="card-body text-center" style="background-color: #fff;padding:5px;height: 250px;overflow-y: auto">
                                                                <h3 class="card-title text-success" style="color:#1f2839">{{Str::limit($item_projet['nom'],115)}}</h3>
                                                                <br/>
                                                                <p class="card-text" style="color:#1f2839"> {{$item_projet->region->pays->nom_pays}} - {{$item_projet->region->nom_region}}</p>

                                                                <p class="text-center">
                                                                    <a href="{{route('un_projet',[app()->getLocale(),$item_projet['id']])}}" class="btn btn-primary" style="background-color:#1f2839">{{__("btn_voir_plus")}}</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                        $nombre_de_projet_afficher++;
                                                        if($nombre_de_projet_afficher ==4):?>
                                                            @break
                                                        <?php endif;?>
                                                @endforeach
                                            </div>
                                            <div>
                                                <p class="text-center" style="padding:8px">
                                                    <a href="{{route('projets',app()->getLocale())}}" class="btn btn-primary" style="background-color: #fff;color:#1f2839">{{__("btn_voir_tous")}}</a>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

        {{--                    *********************************************************************************************************--}}
        {{--                    *************************************************SECTION NEWS LETTERS*****************************************--}}
        {{--                    *********************************************************************************************************--}}
                    <div id="pg-22-5" class="panel-grid panel-has-style">
                        <div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-22-5"
                             data-stretch-type="full" style="background-color:#fff;">
                            <div id="pgc-22-5-0" class="panel-grid-cell" style="padding-left:8px ">
                                <div id="panel-22-5-0-0"
                                     class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-first-child panel-last-child"
                                     data-index="7">
                                    <div class="textwidget">
                                        <blockquote>
                                            <p>
                                                <strong>
                                                    <span style="color: #1f2839;">
                                                       {{__("texte_souscrivez")}}
                                                    </span>
                                                </strong>
                                            </p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                            <div id="pgc-22-5-1" class="panel-grid-cell panel-grid-cell-empty"></div>
                            <div id="pgc-22-5-2" class="panel-grid-cell">
                                <div id="panel-22-5-2-0"
                                     class="so-panel widget widget_newsletterwidgetminimal panel-first-child panel-last-child"
                                     data-index="8">
{{--                                        <span class="widget-title" style="font-size: 18px;font-weight: bold;color:#1f2839;text-align: center">Newsletter</span>--}}

                                    <div class="tnp tnp-widget-minimal">
                                        <div class="row">
                                            <div class="row">
                                                <div id="reponse_recue"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-offset-1 col-xs-10" style="padding: 5px">
                                                    <input class="form-control" style="color:#1f2839 !important;border:none;border-bottom: 1px solid #1f2839;border-radius: 0px" type="email" required name="email" id="email_a_enregistrer" placeholder="Email">
                                                </div>
                                                <div class="col-xs-12 text-center">
                                                    <button class="btn tnp-submit"
                                                            type="button" id="souscrire"
                                                            style="margin-left: 0px;background-color:#1f2839 !important;border-color:#1f2839!important;">
                                                        {{__("btn_souscrire")}}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

        {{--                    *********************************************************************************************************--}}
        {{--                    *************************************************SECTION PARTENAIRE*****************************************--}}
        {{--                    *********************************************************************************************************--}}
                    <div id="pg-22-13" class="panel-grid panel-has-style">
                        <div class="videobackground siteorigin-panels-stretch panel-row-style panel-row-style-for-22-13"
                             data-stretch-type="full">
                            <div id="pgc-22-13-0" class="panel-grid-cell">
                                <div id="panel-22-13-0-0"
                                     class="so-panel widget widget_themestall themestall panel-first-child panel-last-child"
                                     data-index="19">
                                    <div class="textwidget">
                                        <div class="youtubevideo" data-videoid="TwIEOkZ-e7I">
                                            <div class="video-overlay"
                                                 style="background:-webkit-linear-gradient(left, #6bb42f 0%, #6bb42f 46%, rgba(107,180,47, 0.2) 99%,
                                                  rgba(255,255,255,0) 100%);background:linear-gradient(to right, #6bb42f 0%, #6bb42f 46%, rgba(107,180,47, 0.2) 99%,
                                                   rgba(255,255,255,0) 100%);"></div>

                                            <div class="row text-center">
                                                <div class="col-md-6">
                                                    <div class="videobg text-left">

                                                        <div class="textwidget">
                                                            <div class="section-big-title text-center title-subtitle"
                                                                 style="margin-bottom: 0px;">
                                                                <h3 style="font-size: 42px; color: #fff;">{{__("nos_partenaires")}}</h3>
                                                                <div class="divider-title text-center" style="background: #fff">
                                                                    <span class="span-1" style="background: #fff"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="container-fluid">
                                                            <div class="row slick">
                                                                @foreach($partenaires as $item_partenaire)
                                                                    <div class="col-xs-12"
                                                                         style="height:300px;background-color: #fff;
                                                                                 background-image: url('{{$item_partenaire['logo']}}');
                                                                                 background-size: 50%;background-repeat: no-repeat;background-position: center">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
{{--                                                        <button id="playvideo" class="btn btn-home">Play / Stop</button>--}}
{{--                                                        <button id="opensound" class="btn btn-home">Sound / Mute--}}
                                                        </button>
                                                    </div>
                                                </div>{{-- end col --}}
                                            </div>{{-- end row --}}
                                        </div>{{-- end section --}}</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--                    *********************************************************************************************************--}}
                    {{--                    *************************************************FIN SECTION DU PARTENAIRE*****************************************--}}
                    {{--                    *********************************************************************************************************--}}


{{--                    *********************************************************************************************************--}}
{{--                    *************************************************SECTION DU BLOG*****************************************--}}
{{--                    *********************************************************************************************************--}}
                    <div id="pg-22-15" class="panel-grid panel-has-style container">
                        <div class="panel-row-style panel-row-style-for-22-15">
                            <div id="pgc-22-15-0" class="panel-grid-cell">
                                <div id="panel-22-15-0-0"
                                     class="so-panel widget widget_siteorigin-panels-builder panel-first-child panel-last-child"
                                     data-index="22">
                                    <div class="panel-widget-style panel-widget-style-for-22-15-0-0">
                                        <div id="pl-w590f7a7b23d1e" class="panel-layout">
                                            <div id="pg-w590f7a7b23d1e-0" class="panel-grid panel-no-style">
                                                <div id="pgc-w590f7a7b23d1e-0-0" class="panel-grid-cell">
                                                    <div id="panel-w590f7a7b23d1e-0-0-0"
                                                         class="so-panel widget widget_themestall themestall panel-first-child panel-last-child"
                                                         data-index="0">
                                                        <div class="textwidget">
                                                            <div class="ts-posts-default-loop row justify-content-between">

                {{--                    *************************************************SECTION SECTION DES DERNIERES ARTICLE DU BLOG*****************************************--}}
                                                                @foreach($articles as $item_article)
                                                                <div class="col-md-4 col-sm-12">
                                                                    <div class="blog-wrapper clearfix">
                                                                        <div class="post-media">
                                                                            <div class="entry has-thumb"
                                                                                 style="height: 200px;
                                                                                         background-image: url({{$item_article['banniere']}});
                                                                                         background-size: 100%;
                                                                                         background-color: #fff;
                                                                                         background-position: center" >
{{--                                                                                <img src="{{$item_article['banniere']}}"--}}
{{--                                                                                     alt="blog22" width="100%" height="100%"/>--}}
                                                                                <div class="date-container">
                                                                                    <a href="{{route('lire_article',[app()->getLocale(),$item_article['id']])}}">{{date('d M Y', strtotime($item_article['updated_at']))}} </a>
                                                                                </div>{{-- end magnifier --}}
                                                                            </div>
                                                                        </div>{{-- end media --}}
                                                                        <div class="blog-title conteneur_de_texte_riche">
                                                                            <h4>
                                                                                <a href="{{route('lire_article',[app()->getLocale(),$item_article['id']])}}"
                                                                                   title="">{{Str::limit($item_article['titre'],40)}}</a></h4>
                                                                                      <p>{{Str::limit($item_article['resume'],100)}}</p>
                                                                                      <a class="read-more btn hero-btn"
                                                                                         href="{{route('lire_article',[app()->getLocale(),$item_article['id']])}}">{{__("btn_voir_plus")}}</a>
                                                                            </p>
                                                                        </div>{{-- end blog-title --}}
                                                                    </div>{{-- end clearfix --}}
                                                                </div>
                                                                @endforeach

        {{--                    *************************************************FIN SECTION DES DERNIERES ARTICLE DU BLOG*****************************************--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="pgc-w590f7a7b23d1e-0-1" class="panel-grid-cell" style="border-radius: 5px;background-color: #6bb42f">
                                                    <div id="panel-w590f7a7b23d1e-0-1-0"
                                                         class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-first-child panel-last-child"
                                                         data-index="1">
                                                        <div class="panel-widget-style panel-widget-style-for-w590f7a7b23d1e-0-1-0 vmiddle">
                                                            <div class="textwidget"><h2
                                                                        class="ts-heading ts-heading-style-2 text-center"
                                                                        style="margin-top:0px; margin-bottom:20px;
                                                                        font-size: 28px; text-transform: uppercase; color: #ffffff; letter-spacing: 0px; ">
                                                                    <span class="ts-heading-inner">{{__("actualite")}}</span>
                                                                </h2>
                                                                <p>
                                                                    <h5 style="color: #ffffff;text-align: center">{{__("texte_actualite")}}</h5>
                                                                    <h3 class="text-center">
                                                                        <a href="{{route('acceuil_blog',app()->getLocale())}}" class="btn" style="background-color:#fff ;color:#6bb42f">{{__("btn_consulter")}}</a>
                                                                    </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--                    *********************************************************************************************************--}}
                    {{--                    *************************************************SECTION DU BLOG*****************************************--}}
                    {{--                    *********************************************************************************************************--}}
                </div>
{{--            </div>--}}
{{--        </div>--}}

    </section>

@endsection

@section('script_complementaire')

<script type="text/javascript" src="/slick/slick.min.js"></script>
    <script>

        function preventDefault(e) {
            e.preventDefault();
        }
        $(document).ready(function(){
            $('.slick').slick({
                infinite: true,
                speed:100,
                autoplaySpeed:500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay:true
            });

            $('#planteImage').hide();

                $('iframe').css('pointer-events', 'none');
            $('#div_carte').click(function (e) {
                    $('iframe').css('pointer-events', 'auto');
            }).mouseleave(function(e) {
                $('iframe').css('pointer-events', 'none');
            });
        });
        //voir si l'element est visible avant de lancé l'animation

        var descriptionEntreprise = document.querySelector('#descriptionEntreprise');
        var nbFois = 0;
        var observer = new IntersectionObserver(function (element){
            if(nbFois <2){
                if(element[0].isIntersecting === true){
                    // alert('');
                    $('#planteImage').hide();
                    $('#planteImage').show(1000);
                    nbFois++;
                }else{
                    $('#planteImage').hide();
                }
            }
        },{threshold:[1]});
        observer.observe(descriptionEntreprise);
    </script>
@endsection