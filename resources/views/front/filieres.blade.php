@extends('partials.base')

@section('style_complementaire')
    <style>
        .w-full {
            width: 100%;
        }

        .relative {
            position: relative;
        }

        .w-full {
            width: 100%;
        }
        .relative {
            position: relative;
        }

        .ag-fill .body-fill .fill-bg {
            left: 50%;
            transform: translateX(-50%);
            top: -25%;
        }

        .ag-fill .body-fill .fill-bg img:nth-child(2) {
            left: 50%;
            transform: translateX(-50%) translateY(-30%);
            top: 50%;
            position: absolute;
        }

         .conteneur_de_texte_riche{
             word-wrap: break-word;
             max-width: 500px;
             padding: 10px;
         }
        @media screen and (max-width: 1000px){
            .conteneur_de_texte_riche{
                max-width: 700px;
            }
        }
    </style>
@endsection

@section('contenu')

<section id="parallax_container" class="section lb page-title1 little-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                <h3>{{__("nos_filieres")}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<section>
    <div class="container" style="padding-bottom: 100px;margin-top: 10px">

        <div class="row hidden-md hidden-lg">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                    <div class="title-banner-bred " style="background-color: #1f2839">
                        <div class="title-subtitle-content">
                            <h3 style="color: #fff">{{__("nos_filieres")}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php $offset='';$i=0; ?>
            @foreach($filieres as $item_filiere)
                <?php
                $i++;
                if($i%2==0){
                    $offset='col-md-offset-1';
                }else{
                    $offset='';
                }
            ?>
                <div id="{{$item_filiere['nom']}}{{$item_filiere['id']}}" class="{{$offset}} col-md-5" style="height: 400px;margin-top:100px;">
                <div class="relative ag-fill" style="border-color:#209e2e;box-shadow: 5px 5px 5px 11px rgba(42,71,18,0.9)">

                    <div class="justify-center" style="padding-top: 10%;padding-bottom: 2em">
                        <br/>
                        <p class="text-center">
                            @isset($item_filiere['icone'])
                                <img src="{{$item_filiere['icone']}}" class="mx-auto pb-3" style="width:60px; margin-top:-1em;">
                            @else
                                <img src="{{$item_filiere['banniere']}}" class="mx-auto pb-3" style="width:60px; margin-top:-1em;">
                            @endisset
                        </p>
                        <h1 style="border: width: 100%;font-weight: bold;text-align: center;color:#209e2e;background-color:#fff;padding: 10px">
                            <span style="border: 2px solid #209e2e;padding: 10px">
                                {{$item_filiere['nom']}}
                            </span>
                        </h1>
                    </div>
                    <div class="body-fill w-full" style="background-color: #fff;padding-bottom: 25px">
{{--                        FLOTTE--}}
                        <div style="padding: 1em;text-align: center;height: 200px;overflow-y: auto" class="conteneur_de_texte_riche" >
                            <p style="font-size: 18px;color: #6bb42f">
                                {{$item_filiere['description']}}
                            </p>
                        </div>
{{--                        FLOTTE--}}

                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection