@extends('partials.base')

@section('style_complementaire')
    <style>
        .conteneur_de_texte_riche{
            word-wrap: break-word;
            max-width: 800px;
            padding: 10px;
        }
        @media screen and (min-width: 600px) and (max-width: 800px){
            .conteneur_de_texte_riche{
                max-width: 600px;
            }
        }
        @media screen and (max-width: 600px){
            .conteneur_de_texte_riche{
                max-width: 350px;
            }
        }
        @media screen and (max-width: 350px){
            .conteneur_de_texte_riche{
                max-width: 200px;
            }
        }
    </style>
@endsection

@section('contenu')
    <section class="section lb page-title1 little-pad" style="background: url('{{$un_article['banniere']}}');">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                <h3>AgroNews</h3>
                                <p>{{__("texte_souscrivez")}}</p>
                            </div>
                            <div class="breadcrumb-content">
                                <ul class="breadcrumb">
                                    <li><a href="#footer" class="btn" style="background-color:#1f2839;">Newsletter</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section main-container">

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-md hidden-lg" style="background-color: #1f2839;margin-top: 10px">
                            <div class="title-subtitle-content">
                                <h3 style="color:#fff;">AgroNews</h3>
                                <p style="color:#fff;">{{__("texte_souscrivez")}}</p>
                            </div>
                            <div class="breadcrumb-content">
                                <ul class="breadcrumb">
                                    <li><a href="#footer" class="btn" style="background-color:#fff;">Newsletter</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
{{--********************************SECTION ARTICLE********************************--}}
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="content">


                        <div id="post-630"
                             class="post-630 post type-post status-publish format-standard has-post-thumbnail
                             hentry category-nursery tag-nursery tag-plant tag-sale tag-shop">
                            <div class="blog-item">
                                <div class="post-media">
                                    <div class="entry has-thumb">

                                        {{--***************VIDEO YOUTUBE*************--}}

                                        @if($un_article['video'] != null)
                                            <div class="col-xs-12 text-center" style="padding-top:15px;margin-bottom: 15px">
                                                {!! $un_article['video']!!}
                                                <br/>
                                                <a href="{{$parametre['lien_youtube']}}"><h5><u>Voir les autres actualités de projets sous formats video</u></h5></a>
                                            </div>
                                        @else

                                            <div class="date-container">
                                                {{date('d M Y',strtotime($un_article['updated_at']))}}
                                            </div><!-- end magnifier -->
                                            <img src="{{$un_article['banniere']}}" alt="blog3" style="width:calc(90%); max-height: 400px">

                                        @endif

                                        {{--***************VIDEO YOUTUBE*****************--}}

                                    </div>
                                </div><!-- end media -->
                                <p>

                                    {{--                                PARTAGER LE POST--}}
                                    <br/><br/>
                                <ul class="list-inline social">
                                    <li class="facebook">
                                        <!--fb-->
                                        <a target="_blank" href="http://www.facebook.com/share.php?u={{Request::url()}}&amp;title={{$un_article['titre']}}"
                                           title="Share this post on Facebook!"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="twitter">
                                        <!--twitter-->
                                        <a target="_blank"
                                           href="http://twitter.com/home?status={{$un_article['titre']}}:{{Request::url()}}"
                                           title="Share this post on Twitter!"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="google-plus">
                                        <!--g+-->
                                        <a target="_blank" href="https://plus.google.com/share?url={{Request::url()}}"
                                           title="Share this post on Google Plus!"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li class="linkedin">
                                        <!--linkedin-->
                                        <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::url()}}&amp;title={{$un_article['titre']}};source=LinkedIn"
                                           title="Share this post on Linkedin!"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <li class="email-share">
                                        <!--Email-->
                                        <a title="Share by email" href="mailto:?subject=Check this post - {{$un_article['titre']}} &body={{Request::url()}}&title='{{$un_article['titre']}}' email"="">
                                        <i class="fa fa-envelope"></i>
                                        </a>
                                    </li>
                                </ul>
                                {{--                                PARTAGER LE POST--}}
                                </p>
                                <div class="blog-meta">
                                    <br/>
                                    <h1 style="padding: 25px">{{$un_article['titre']}}</h1>

                                    <div class="container-fluid">
                                        <div class="row">

                                            <div class="col-xs-12 conteneur_de_texte_riche" >
                                                {!! $un_article['article']!!}
                                            </div>
                                        </div>
                                    </div>
                                    <p><span id="more-630"></span></p>

                                    <div class="post-views post-630 entry-meta">
                                        <span class="post-views-icon dashicons dashicons-chart-bar"></span>
                                        <span class="post-views-label">Post Views: </span>
                                        <span class="post-views-count">647</span>
                                    </div>
                                </div><!-- end blog-meta -->

                                <hr>

{{--                                PARTAGER LE POST--}}
                                <ul class="list-inline social">
                                    <li class="facebook">
                                        <!--fb-->
                                        <a target="_blank" href="http://www.facebook.com/share.php?u={{Request::url()}}&amp;title={{$un_article['titre']}}"
                                           title="Share this post on Facebook!"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="twitter">
                                        <!--twitter-->
                                        <a target="_blank"
                                           href="http://twitter.com/home?status={{$un_article['titre']}}:{{Request::url()}}"
                                           title="Share this post on Twitter!"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="google-plus">
                                        <!--g+-->
                                        <a target="_blank" href="https://plus.google.com/share?url={{Request::url()}}"
                                           title="Share this post on Google Plus!"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li class="linkedin">
                                        <!--linkedin-->
                                        <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::url()}}&amp;title={{$un_article['titre']}};source=LinkedIn"
                                           title="Share this post on Linkedin!"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <li class="email-share">
                                        <!--Email-->
                                        <a title="Share by email" href="mailto:?subject=Check this post - {{$un_article['titre']}} &body={{Request::url()}}&title='{{$un_article['titre']}}' email"="">
                                        <i class="fa fa-envelope"></i>
                                        </a>
                                    </li>
                                </ul>
        {{--                                PARTAGER LE POST--}}

                            </div><!-- end blog-item -->
                        </div><!-- end content -->


                    </div>
                </div>

{{--********************************SECTION SIDE BAR********************************--}}
                <div id="sidebar" class="col-md-4 col-sm-12 col-xs-12">
                    <div class="sidebar">
                        <div class="widget-area">
                            <div id="recent-posts-3" class="widget clearfix widget_recent_entries">
                                <div class="widget-title"><h4>{{__("artcile_recent")}}</h4></div>
{{--                                <div class="recentposts recent">--}}
                                    <div class="container-fluid">
                                        <div class="row">

                                        {{--********************************SCETION SUGGESTION********************************--}}
                                        @foreach($suggestion_articles as $item_suggestion)
{{--                                        <li>--}}
                                            <div class="col-xs-12">
                                            <a href="{{route('lire_article',[app()->getLocale(),$item_suggestion['id']])}}">
                                                <div style="height: 220px; background:url('{{$item_suggestion['banniere']}}') 100% center no-repeat ;margin-bottom:0px; ">
                                                </div><!-- end entry -->
                                                <h4 style="color:#fff;background-color: #1f2839;text-align: center;padding: 5px;margin-top:0px">
                                                    {{Str::limit($item_suggestion['titre'],40)}}
                                                </h4>
                                            </a>
                                            </div><!-- end entry -->
                                        </li>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </div>
        <!-- Content Wrap -->

    </section>

@endsection