@extends('partials.base')
@section('style_complementaire')
    <style>
        .conteneur_de_texte_riche{
            word-wrap: break-word;
            max-width: 300px;
            padding: 10px;
        }
        @media screen and (max-width: 300px){
            .conteneur_de_texte_riche{
                max-width: 200px;
            }
        }
    </style>
@endsection
@section('contenu')
    <section class="section lb page-title1 little-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                <h3>AgroNews</h3>
                                <p>{{__("texte_souscrivez")}}</p>
                            </div>
                            <div class="breadcrumb-content">
                                <ul class="breadcrumb">
                                    <li><a href="#footer" class="btn" style="background-color:#1f2839;">Newsletter</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section main-container" style="padding-top: 15px">

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-md hidden-lg" style="background-color: #1f2839">
                            <div class="title-subtitle-content">
                                <h3 style="color: #fff">AgroNews</h3>
                                <p style="color:#fff" >{{__("texte_souscrivez")}}</p>
                            </div>
                            <div class="breadcrumb-content">
                                <ul class="breadcrumb">
                                    <li><a href="#footer" class="btn" style="background-color:#fff;">Newsletter</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                                <?php $offset='';$i=0; ?>
                             @foreach($articles as $item_article)
                                <?php
                                $i++;
                                if($i%2==0){
                                    $offset='col-md-offset-1';
                                }else{
                                    $offset='';
                                }
                                ?>
                                <div class="{{$offset}} col-md-5" style="box-shadow: 0 15px 5px -2px #1f2839;min-height: 300px;margin-bottom:15px;margin-top: 30px">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="post-media">
                                                <div class="entry has-thumb">
                                                    <a href="{{route('lire_article',[app()->getLocale(),$item_article['id']])}}" title="">
                                                    <div class="col-xs-12"
                                                         style="height: 250px;background: url('{{$item_article['banniere']}}')
                                                                 top no-repeat;background-size: cover">
                                                    </div>
                                                    </a>

                                                    <div class="date-container">
                                                        <a href="{{route('lire_article',[app()->getLocale(),$item_article['id']])}}" style="z-index: 50">
                                                            {{date('d M Y',strtotime($item_article['updated_at']))}}</a>
                                                    </div><!-- end magnifier -->

                                                </div>
                                            </div><!-- end media -->
                                        </div>
                                        <div class="col-xs-7">
                                            <div class="blog-meta conteneur_de_texte_riche">
                                                <h4><a href="{{route('lire_article',[app()->getLocale(),$item_article['id']])}}" title="">{{Str::limit($item_article['titre'],40)}}</a></h4>
                                                <p>{{Str::limit($item_article['resume'],170)}}</p>
                                                <p><a href="{{route('lire_article',[app()->getLocale(),$item_article['id']])}}" class="more-link">{{__("btn_voir_plus")}}</a></p>

                                            </div><!-- end blog-meta -->
                                        </div>
                                    </div>
                                </div><!-- end blog-item -->
                            @endforeach

                    </div>
                </div>
        <!-- Content Wrap -->

    </section>
@endsection
