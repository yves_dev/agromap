@extends('partials.base')

@section('style_complementaire')
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

    <style>
        .form-control{
            border-color: #ccc;
            color: #111;
            padding: 10px;
        }
        .form-control::placeholder,.form-control::-moz-placeholder,.form-control::-webkit-input-placeholder,.form-control::-ms-input-placeholder{
            color: #111;
        }
        #parallax_container {
            /* Set a specific height */
            background-image: url('/assets/wp-content/themes/nurseryplant/images/blog.jpg');

            /* Create the parallax scrolling effect */
            background-attachment: fixed  !important;;
            background-position: center  !important;;
            background-repeat: no-repeat !important;;
            background-size: cover  !important;;
        }

        .btn{
            background-color: #1f2839;
            color: #fff;
        }
        .btn:hover{
            background-color: yellow;
            color: #1f2839;
        }
        #contact_form .form-group{
            margin-top: 10px;
        }
    </style>
@endsection

@section('contenu')

    <section id="parallax_container" class="section lb page-title1 little-pad" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                <h3>{{__("contact")}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start main-content -->
    <div class="main-content">

        <div class="row">
            <div class="col-md-12">
                <div class="section-big-title clearfix text-center">
                    <div class="title-banner-bred hidden-md hidden-lg" style="background-color: #1f2839;padding: 5px">
                        <div class="title-subtitle-content">
                            <h3 style="color: #fff">{{__("contact")}}</h3sty>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section: inner-header -->

        <!-- Divider: Contact -->
        <section class="divider">
            <div class="container pt-0">
                <div class="row mb-60 bg-deep" style="background-color: #f1f1f1">
                    <div class="col-sm-12 col-md-4" style="padding: 5%;">
                        <div class="contact-info text-center pt-60 pb-60 border-right">
                            <i class="fa fa-phone font-36 mb-10 text-theme-colored" style="font-size: 2.57142857rem;margin-bottom: 10px"></i>
{{--                            <h4>Call Us</h4>--}}
                            <h6 class="text-gray">{{$parametre['telephones']}}</h6>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4" style="padding: 5%;border-left: 1px solid #ccc;border-right: 1px solid #ccc;">
                        <div class="contact-info text-center  pt-60 pb-60 border-right">
                            <i class="fa fa-map-marker font-36 mb-10 text-theme-colored" style="font-size: 2.57142857rem;margin-bottom: 10px"></i>
{{--                            <h4>Address</h4>--}}
                            <h6 class="text-gray">{{$parametre['adresse']}}</h6>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4" style="padding: 5%;">
                        <div class="contact-info text-center  pt-60 pb-60">
                            <i class="fa fa-envelope font-36 mb-10 text-theme-colored" style="font-size: 2.57142857rem;margin-bottom: 10px"></i>
{{--                            <h4>Email</h4>--}}
                            <h6 class="text-gray">{{$parametre['email']}}</h6>
                        </div>
                    </div>
                </div>


                <div class="row" style="margin-top: 25px;">

{{--                    DIV FORMULAIRE--}}
                    <div class="col-md-7">
{{--                        <span style="font-size:1.5em">Ecrivez-nous</span>--}}
                        <!-- Contact Form -->
                        <form id="contact_form" name="contact_form" action="" method="post" style="margin-top: 15px">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form_name">{{__("nom")}} <small>*</small></label>
                                        <input name="form_name" class="form-control" type="text" placeholder="John Travolta" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email <small>*</small></label>
                                        <input name="form_email" class="form-control required email" type="email" placeholder="email@gmail.com" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form_name">{{__("sujet")}} <small>*</small></label>
                                        <input name="form_subject" class="form-control required" type="text" placeholder="{{__("sujet")}} " required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form_phone">Telephone / Phone *</label>
                                        <input name="form_phone" class="form-control" type="number" placeholder="+225 78 76 04 58" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="form_name">Message *</label>
                                <textarea name="form_message" class="form-control required" rows="5" placeholder="message..." required></textarea>
                            </div>
                            <div class="form-group">
                                <input name="form_botcheck" class="form-control" type="hidden" value="" />
                                <button type="submit" class="btn btn-flat" data-loading-text="Please wait..." style="margin: 5px">{{__("envoyer")}} </button>
                                <button type="reset" class="col-xs-offset-1 btn btn-flat btn-theme-colored" style="margin: 5px">{{__("vider")}} </button>
                            </div>
                        </form>

                    </div>


                    {{--                    DIV LA CARTE DE LOCALISATION--}}
                    <div class="col-md-5">
                    {{--                        <span  style="margin:10px;margin-left:0px;padding-bottom: 15px;font-size:1.2em">Find Our Location / Notre siège</span>--}}
                    <!-- Google Map HTML Codes -->
                        <div>
                            <iframe class="w-full h-screen" style="width: 100%;min-height: 400px;"
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.194467393645!2d-3.989091785735894!3d5.387304136822532!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc19325b80f8ab7%3A0xd2e901d2194f1231!2sAGRO-MAP!5e0!3m2!1sfr!2sci!4v1578477221808!5m2!1sfr!2sci"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- end main-content -->
@endsection
