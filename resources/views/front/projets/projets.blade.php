@extends('partials.base')

@section('style_complementaire')
<style>
    #parallax_container {
        /* Set a specific height */
        background-image: url('/assets/wp-content/themes/nurseryplant/images/blog.jpg');
        height: 60vh;

        /* Create the parallax scrolling effect */
        background-attachment: fixed  !important;;
        background-position: center  !important;;
        background-repeat: no-repeat !important;;
        background-size: cover  !important;;
    }
    .btn_pays{
        background-color: #1f2839;
        color: #fff;
    }
    .btn_pays:hover{
        background-color: yellow;
        color: #1f2839;
    }
    </style>
@endsection

@section('contenu')
    <section class="section lb page-title1 little-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                 <h3>Liste des projets rangés par pays</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="parallax_container" class="section main-container" style="margin-top: 7%;padding-top: 15px;min-height:95vh ;
                background-image: url('/uploaded_image/projets/static/map.png');background-size: cover;;background-position: bottom;background-repeat: repeat">

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-md hidden-lg" style="background-color: #1f2839;padding: 5px">
                            <div class="title-subtitle-content">
                                <h3 style="color: #fff">Liste des projets rangés par pays</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <?php $offset='';$i=0; ?>
                @foreach($les_pays_ou_il_ya_des_projet as $item_pays)
                        <?php
                        $i++;
                        if($i%2==0){$offset='col-md-offset-1';}else{$offset='';}
                        ?>
                        <div class="blog-item col-md-4 col-sm-12 col-xs-12" style="padding: 0 !important;margin-bottom: 10px">
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="{{route('projets_pays',[app()->getLocale(),$item_pays['id']])}}"  title="">
                                        <div class="blog-meta" style="padding: 5px;">
                                            <h4 class="text-center">
                                                <img src="/uploaded_image/projets/static/pin_point.png" width="100px"/>
                                                <br/>
                                                <a class="btn btn_pays"
                                                    href="{{route('projets_pays',[app()->getLocale(),$item_pays['id']])}}"
                                                >
                                                    {{$item_pays['nom_pays']}}</a>
                                            </h4>
                                        </div><!-- end blog-meta -->
                                    </a>
                                </div>
                            </div>
                        </div><!-- end blog-item -->
                @endforeach
                    </div>
                </div>
        <!-- Content Wrap -->

    </section>
@endsection
