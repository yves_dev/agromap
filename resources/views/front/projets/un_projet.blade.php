@extends('partials.base')

@section('style_complementaire')
    <style>
        #side_info div{
            border-bottom:1px dotted #d3d7d9;
            padding-bottom: 5px;
        }
        pre{
            font-size: 16px;
            font-family: 'Times New Roman',Arial,sans-serif;
        }
    </style>
@endsection

@section('contenu')

    <section class="section lb page-title1 little-pad" style="background: url('{{$le_projet['image_illustration']}}') center;background-size: cover">
        <div class="container">
            <div class="row">
                <div class="col-md-12 hidden-sm hidden-xs">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred">
                            <div class="title-subtitle-content">
                                <h3 style="padding-left: 10px;padding-right: 10px">{{$le_projet['nom']}}</h3>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section main-container">

        <div class="container">

            {{--***************STATISTIQUE*****************--}}
            <div class="container-fluid" style="padding: 15px 5px">

                <div class="row">
                    <div class="section-big-title clearfix text-center hidden-md hidden-lg" style="background-color: #1f2839">
                        <div class="title-banner-bred" style="background-color: #1f2839">
                            <div class="title-subtitle-content">
                                <h3 style="padding-left: 10px;padding-right: 10px;color:#fff">{{$le_projet['nom']}}</h3>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-top: 15px">
                    @if($le_projet['hectar'] !=null)
                        <div class="col-md-4 text-center">
                            <img src="/uploaded_image/projets/static/ha.png" style='height:50px' /><br/>
                            <h4>{{__("zone_restauree")}}</h4>
                            <h3><b>{{$le_projet['hectar']}} Ha</b></h3>
                        </div>
                    @endif
                    @if($le_projet['arbre'] !=null)
                        <div class="col-md-4 text-center">

                            <img src="/uploaded_image/projets/static/arbre.png" style='height:50px' /><br/>
                            <h4>{{__("arbre")}}</h4>
                            <h3><b>{{$le_projet['arbre']}}</b></h3>
                        </div>
                    @endif
                    @if($le_projet['carbonne'] !=null)
                        <div class="col-md-4 text-center">

                            <img src="/uploaded_image/projets/static/co2.png" style='height:50px' /><br/>
                            <h4>{{__("carbonne")}} </h4>
                            <h3><b>{{$le_projet['carbonne']}} CO<sub>2</sub></b></h3>
                        </div>
                    @endif
                </div>
            </div>

            {{--***************FIN STATISTIQUE****
                                    VIDEO YOUTUBE*************--}}

            @if($le_projet['youtube_video'] != null)
                <div class="container text-center" style="padding-top:15px;margin-bottom: 15px">
                    {!! $le_projet['youtube_video']!!}
                    <br/>
                    <h5>
                        <a href="{{$parametre['lien_youtube']}}">
                            <u>{{__("voir_playlist")}}</u>
                        </a>
                    </h5>
                </div>
            @endif

            {{--***************VIDEO YOUTUBE****
                                    Description ecrite*************--}}

            <div class="container-fluid">
                <div class="row">
        {{--                //************ cadre resume et galerie--}}
                        <div class="col-md-8">
                            <div class="row">

                                {{--//************ resume--}}

                                <div class="col-xs-12 text-left" style="margin-bottom: 20px;padding-bottom:5px;background-color: #1f2839;color:#fff;
                                                        font-family: 'Times New Roman',Arial,sans-serif;font-size: 18px">
                                    <h1 style="color: #fff;text-align: left">{{__("resume")}}</h1>
                                    {{$le_projet['resume']}}
                                </div>

                                {{--//************ gallerie--}}
                                <div class="col-xs-12">

                                            <div id="pg-22-0" class="panel-grid panel-has-style" style="cursor: grab;">
                                                    <div id="pgc-22-0-0" class="panel-grid-cell">
                                                        <div id="panel-22-0-0-0"
                                                             class="so-panel widget widget_themestall themestall panel-first-child panel-last-child"
                                                             data-index="0">
                                                                <div class="home-banner" style="overflow-y: hidden">
                                                                    <div class="owl-carousel owl-banner-slider" data-animatein=""
                                                                         data-animateout=""
                                                                         data-items="1" data-margin=""
                                                                         data-loop="true"
                                                                         data-merge="true"
                                                                         data-nav="false"
                                                                         data-dots="false"
                                                                         data-stagepadding=""
                                                                         data-mobile="1"
                                                                         data-tablet="1"
                                                                         data-desktopsmall="1"
                                                                         data-desktop="1"
                                                                         data-autoplay="true"
                                                                         data-delay="6000"
                                                                         data-navigation="false" data-rtl="false">

                                                                        {{--                                /***********************************************************--}}
                                                                        {{--                                    CAROUSEL ITEMS--}}
                                                                        {{--                                /***********************************************************--}}
                                                                        <?php $gallerie = explode('#',$le_projet['liste_urls_gallerie_image']) ?>
                                                                        @foreach($gallerie as $item_image)
                                                                            @if($item_image!='')
                                                                                <div class="item">{{--DEBUT ITEM--}}
                                                                                    <img src="{{$item_image}}" alt="illustration"
                                                                                         class="img-responsive" width="100%">
                                                                                </div>{{--FIN ITEM--}}
                                                                            @endif
                                                                        @endforeach

                                                                    </div>
                                                                </div>
                                                        </div>
                                                    </div>
                                        </div>
                                </div>{{--//FIN SLIDER--}}

                            </div>
                            <div class="row">

                                @isset($le_projet['objectif'])
                                    <div >
                                        <h4>{{__("objectif")}}</h4>
                                        <pre>{{$le_projet['objectif']}}</pre>
                                    </div>
                                @endisset

                                @isset($le_projet['resultats_attendus'])
                                    <div>
                                        <h4>{{__("resultat_attendus")}}</h4>
                                        <pre>{{$le_projet['resultats_attendus']}}</pre>
                                    </div>
                                @endisset
                            </div>
                        </div>

        {{--                //************ cadre objectif,region partenaire...--}}
                        <div class="col-md-4" id="side_info">

                            <div >
                                <h4>Region</h4>
                                <pre>{{$le_projet->region->pays->nom_pays}} , {{$le_projet->region->nom_region}}</pre>
                            </div>

                            @isset($le_projet['etat'])
                            <div >
                                <h4>{{__("etat")}}</h4>
                                <pre>{{$le_projet['etat']}}</pre>
                            </div>
                            @endisset

                        @isset($le_projet['periode_travail'])
                            <div >
                                <h4>{{__("periode")}}</h4>
                                <pre>{{$le_projet['periode_travail']}}</pre>
                            </div>
                        @endisset

                            @isset($le_projet['partenaires'])
                            <div >
                                <h4>{{__("partenaires")}}</h4>
                                <pre>{{$le_projet['partenaires']}}</pre>
                            </div>
                            @endisset


                        @isset($le_projet['methode'])
                            <div >
                                <h4>{{__("methode")}}</h4>
                                <pre>{{$le_projet['methode']}}</pre>
                            </div>
                            @endisset


                        </div>
                </div>
            </div>
        </div>
        {{--            ///TEXTE DIVERS--}}
        <div class="container" style="padding-bottom: 15px;border-top:1px dotted #d3d7d9">
            {!! $le_projet['text_divers']!!}
        </div>
        <!-- Content Wrap -->

    </section>


@endsection