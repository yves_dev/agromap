@extends('partials.base')

@section('style_complementaire')
<style>
    #parallax_container {
        /* Set a specific height */
        background-image: url('/assets/wp-content/themes/nurseryplant/images/blog.jpg');
        height: 60vh;

        /* Create the parallax scrolling effect */
        background-attachment: fixed  !important;;
        background-position: center  !important;;
        background-repeat: no-repeat !important;;
        background-size: cover  !important;;
    }
    </style>
@endsection

@section('contenu')
    <section id="parallax_container" class="section lb page-title1 little-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                <h4> {{__("projet_pays")}} </h4>
                                <h2><b>{{strtoupper($les_projets_groupe_par_region[0][0]->region->pays->nom_pays)}}</b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section main-container" style="padding-top: 15px">

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-md hidden-lg" style="background-color: #1f2839">
                            <div class="title-subtitle-content">
                                <h4 style="color: #fff" > {{__("projet_pays")}} </h4>
                                <h2><b style="color: #fff">{{strtoupper($les_projets_groupe_par_region[0][0]->region->pays->nom_pays)}}</b></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <?php $offset='';$i=0; ?>
                @foreach($les_projets_groupe_par_region as $item_projets_region)
                    @foreach ($item_projets_region as $item_projet)
                        @if($item_projet['langue']==app()->getLocale())
                                <?php
                                $i++;
                                if($i%2==0){$offset='col-md-offset-1';}else{$offset='';}
                                ?>
                                <div class="blog-item {{$offset}} col-md-5 col-xs-12" style="padding: 0 !important;box-shadow: 15px 15px 15px 10px #1f2839;min-height: 450px; ">
                                    <div class="row">
                                        <div class=" col-xs-12">
                                            <a href="{{route('un_projet',[app()->getLocale(),$item_projet['id']])}}" title="">
                                            <div class="col-xs-12"
                                                 style="height: 200px;
                                                    background: url('{{$item_projet['image_illustration']}}')
                                                         top no-repeat;background-size: cover">
                                            </div>
                                            </a>
                                        </div>
                                        <div class="col-xs-12">
                                            <a href="{{route('un_projet',[app()->getLocale(),$item_projet['id']])}}" title="">
                                                <div class="blog-meta" style="padding: 5px;">
                                                    <h4 style="width: 100%;text-align: center"><a href="{{route('un_projet',[app()->getLocale(),$item_projet['id']])}}" title="">{{Str::limit($item_projet['nom'],115)}}</a></h4>
                                                    <h5 class="text-center">Region : {{$item_projet->region->nom_region}}</h5>
                                                    <p class="text-center">
                                                        <a href="{{route('un_projet',[app()->getLocale(),$item_projet['id']])}}" class="more-link"> {{__("btn_voir_plus")}} </a>
                                                    </p>

                                                </div><!-- end blog-meta -->
                                            </a>
                                        </div>
                                    </div>
                                </div><!-- end blog-item -->

                        @endif
                    @endforeach
                @endforeach
                    </div>
                </div>
        <!-- Content Wrap -->

    </section>
@endsection
