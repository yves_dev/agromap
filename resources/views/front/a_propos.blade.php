@extends('partials.base')


@section('style_perso')
    <style>

        .page-template-home-page .topbar.style_2 ul li a{
            color: #1f2839;
        }

        .page-template-home-page .topbar.style_2 ul li{
            color: #1f2839;
        }
    </style>
@endsection

@section('contenu')

    <section class="section lb page-title1 little-pad" style="background-image: url('/uploaded_image/apropos/static/plante.png');background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                <h1 style="font-weight: 900;font-size: 7em">
                                    <img src="/uploaded_image/apropos/static/arbre.png" />
                                    {{$parametre['nom_entreprise']}}
                                    <img src="/uploaded_image/apropos/static/arbre.png" />
                                </h1>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- ************************************************************************************************************
                                                      *****SECTION QUI SOMMES NOUS************************************
                  ************************************************************************************************************ --}}
    <div id="pg-22-3" class="panel-grid panel-has-style" style="margin-top: 25px; padding: 5px">

{{--        ************************************QUI SOMMES NOUS*****--}}
        <div class="panel-row-style panel-row-style-for-22-3">
            <div id="pgc-22-3-0" class="panel-grid-cell">
                <div id="panel-22-3-0-0"
                     class="so-panel widget widget_themestall themestall panel-first-child"
                     data-index="4">
                    <div class="textwidget">
                        <div class="section-big-title text-center title-subtitle"
                             style="margin-bottom: 0px;">
                            <h3 style="font-size: 42px; color: #6bb42f;">{{__("qui_sommes_nous")}}</h3>
                            {{--                                <p style="color: #1f2839;">This is an example page. It’s different from a blog post because it will</p>--}}
                            <div class="divider-title text-center" style="background: #6bb42f">
                                <span class="span-1" style="background: #6bb42f"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel-22-3-0-1"
                     class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-last-child"
                     data-index="5">
                    <div class="panel-widget-style panel-widget-style-for-22-3-0-1">
                        <div class="textwidget">
                            <p style="text-align: center;"><br/>{{$a_propos['texte_nous']}}</p>
                            <p>
                                {{--                                    <img class="aligncenter wp-image-611 size-full"--}}
                                {{--                                         src="assets/wp-content/uploads/2017/03/about12.jpg" alt="" width="940" height="509" />--}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

{{--        ************************************VISION*****--}}
        <div class="panel-row-style panel-row-style-for-22-3">
            <div id="pgc-22-3-0" class="panel-grid-cell">
                <div id="panel-22-3-0-0"
                     class="so-panel widget widget_themestall themestall panel-first-child"
                     data-index="4">
                    <div class="textwidget">
                        <div class="section-big-title text-center title-subtitle"
                             style="margin-bottom: 0px;">
                            <h3 style="font-size: 42px; color: #6bb42f;">{{__("notre_vision")}}</h3>
                            <div class="divider-title text-center" style="background: #6bb42f">
                                <span class="span-1" style="background: #6bb42f"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel-22-3-0-1"
                     class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-last-child"
                     data-index="5">
                    <div class="panel-widget-style panel-widget-style-for-22-3-0-1">
                        <div class="textwidget">
                            <p style="text-align: center;"><br/>{{$a_propos['texte_vision']}}</p>
                            <p>
                                {{--                                    <img class="aligncenter wp-image-611 size-full"--}}
                                {{--                                         src="assets/wp-content/uploads/2017/03/about12.jpg" alt="" width="940" height="509" />--}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

{{--        ************************************EQUIPE*****--}}

        <?php
            $nbreMenbre = sizeof($equipe);
            if($nbreMenbre):
        ?>
        <div id="panel-22-3-0-0"
             class="so-panel widget widget_themestall themestall panel-first-child"
             data-index="4">
            <div class="textwidget">
                <div class="section-big-title text-center title-subtitle"
                     style="margin-bottom: 0px;">
                    <h3 style="font-size: 42px; color: #6bb42f;">{{__("notre_equipe")}}</h3>
                    <div class="divider-title text-center" style="background: #6bb42f">
                        <span class="span-1" style="background: #6bb42f"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="margin: 20px 0px">
            <div class="row">
                @foreach($equipe as $item_equipe)
                    <div class="col-md-3" style="padding: 8px">
                        <div class="col-xs-12" style="box-shadow: 5px 5px 5px 5px #1f2839">
                            <h3 class="text-center">
                                <img src="{{$item_equipe['url_photo']}}"style="width: 250px;height:250px;border-radius: 5px" {{--class="img-circle"--}} />
                            </h3>
                            <h3 class="text-center">{{$item_equipe['nom_personne']}}</h3>
                            <h4 class="text-center">{{$item_equipe['fonction']}}</h4>
                            <p class="text-center">{{$item_equipe['description']}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <?php endif; ?>

    </div>
    {{-- ************************************************************************************************************
            ************************************FIN SECTION QUI SOMMES NOUS*****
    ************************************************************************************************************ --}}
@endsection