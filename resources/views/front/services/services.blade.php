@extends('partials.base')

@section('style_perso')
<style>
/*    #parallax_container {
            !* Set a specific height *!
            background-image: url('assets/wp-content/themes/nurseryplant/images/blog.jpg');
            height: 60vh;

            !* Create the parallax scrolling effect *!
            background-attachment: fixed  !important;;
            background-position: center  !important;;
            background-repeat: no-repeat !important;;
            background-size: cover  !important;;
        }*/
    </style>
@endsection

@section('contenu')
    <section id="parallax_container" class="section lb page-title1 little-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred hidden-xs hidden-sm">
                            <div class="title-subtitle-content">
                                <h3>{{__("nos_services")}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section main-container" style="padding-top: 15px">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center hidden-md hidden-lg">
                        <div class="title-banner-bred" style="background-color: #1f2839">
                            <div class="title-subtitle-content">
                                <h3 style="color: #fff" >{{__("nos_services")}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                                <?php $offset='';$i=0; ?>
                             @foreach($tous_les_services as $item_service)
                                 <?php
                                    $i++;
                                    if($i%2==0){
                                        $offset='col-md-offset-1';
                                    }else{
                                        $offset='';
                                    }
                                 ?>
                                <div class="blog-item {{$offset}} col-md-5" style="box-shadow: 0 15px 5px -2px #1f2839;min-height: 270px; ">
                                    <div class="row">
                                        <div class=" col-xs-5">
                                            <div class="post-media">
                                                <div class="entry has-thumb">
                                                    <a href="{{route('un_service',[app()->getLocale(),$item_service['id']])}}" title="">
                                                    <div class="col-xs-12"
                                                         style="height: 240px;
                                                            background: url('{{$item_service['image_section1']}}')
                                                                 top no-repeat;background-size: cover">
                                                    </div>
                                                    </a>
                                                </div>
                                            </div><!-- end media -->
                                        </div>
                                        <div class=" col-xs-7">
                                            <div class="blog-meta" style="padding: 5px;">
                                                <h4 style="width: 100%"><a href="{{route('un_service',[app()->getLocale(),$item_service['id']])}}" title="">{{$item_service['nom']}}</a></h4>

                                                <p>{{ \Illuminate\Support\Str::limit($item_service['description'],$limit = 200, $end = ' ...') }}</p>
                                                <p><a href="{{route('un_service',[app()->getLocale(),$item_service['id']])}}" class="more-link">{{__('btn_voir_plus')}}</a></p>

                                            </div><!-- end blog-meta -->
                                        </div>
                                    </div>
                                </div><!-- end blog-item -->
                            @endforeach

                    </div>
                </div>
        <!-- Content Wrap -->

    </section>
@endsection
