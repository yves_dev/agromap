@extends('partials.base')

@section('style_complementaire')
    <style>
        .conteneur_de_texte_riche{
            word-wrap: break-word;
            max-width: 1000px;
            padding: 10px;
        }
        @media screen and (min-width: 600px) and (max-width: 800px){
            .conteneur_de_texte_riche{
                max-width: 600px;
            }
        }
        @media screen and (max-width: 600px){
            .conteneur_de_texte_riche{
                max-width: 350px;
            }
        }
        @media screen and (max-width: 350px){
            .conteneur_de_texte_riche{
                max-width: 200px;
            }
        }
    </style>
@endsection
@section('contenu')


<!-- ******************************************
START SITE HERE
********************************************** -->
<div id="wrapper">




    <section class="section lb page-title1 little-pad" style="background-image: url('{{$le_service['image_section1']}}');background-size: cover;">
        <div class="container hidden-xs hidden-sm">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred">
                            <div class="title-subtitle-content">
                                <h3>{{$le_service['nom']}}</h3>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section main-container">

        <div class="container">


            <div class="row hidden-md hidden-lg">
                <div class="col-md-12">
                    <div class="section-big-title clearfix text-center">
                        <div class="title-banner-bred" style="background-color: #1f2839;margin-top: 10px">
                            <div class="title-subtitle-content">
                                <h3 style="color: #fff">{{$le_service['nom']}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="content">
                        <div class="content-page blog-item">
                            <div class="post-padding clearfix">
                                <br/><br/>
                                <div class="container">
                                    <div class="row conteneur_de_texte_riche">
                                        @if($le_service['image_section2'] != null)
                                            <h1></h1>
                                            <img src="{{$le_service['image_section2']}}" style="max-height:250px;width:50%;" class="alignright">
                                        @endif
                                        {!!$le_service['text_section1']!!}
                                    </div>
                                    <div class="row conteneur_de_texte_riche">
                                         @if($le_service['image_section3'] != null)
                                              <h1></h1>
                                              <img src="{{$le_service['image_section3']}}" style="max-height:250px;width:50%;" class="alignleft">
                                         @endif
                                        {!! $le_service['text_section2']!!}
                                    </div>
                                    <div class="row conteneur_de_texte_riche">
                                        {!! $le_service['text_section3']!!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

        </div>
        <!-- Content Wrap -->

    </section>


</div><!-- end wrapper -->

@endsection