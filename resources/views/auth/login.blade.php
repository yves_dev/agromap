@extends('layouts.app')

@section('content')

<link href="admin_assets/css/font-awesome.css" rel="stylesheet"><!-- Font-awesome-CSS -->
<link href="admin_assets/css/login.css" rel='stylesheet' type='text/css'/><!-- style.css -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">

<div class="main-agile" style="margin-top: 5%;background-color: #1f2839/*;background-color: #6aa42f*/">
    <div class="alert-close"> </div>
    <div class="content-wthree">
        <div class="circle-w3layouts"></div>
        <h2>Administation</h2>
        <form action="#" method="post">
            <div class="inputs-w3ls" style="padding: 5px">
                <i class="fa fa-user" aria-hidden="true"></i>
{{--                <input type="text" name="Username" placeholder="Username" required=""/>--}}
                <input {{--id="email" --}}type="text" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email"
                       required autocomplete="email" autofocus>
                @error('email') <span class="invalid-feedback" role="alert"> <strong style="color:#fff;">{{ $message }}</strong> @enderror
            </div>


            <div class="inputs-w3ls" style="padding: 5px">
                <i class="fa fa-key" aria-hidden="true"></i>
{{--                <input type="password" name="Password" placeholder="Password" required=""/>--}}
                <input id="password" type="password" placeholder="Mot de passe"
                       class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password') <span class="invalid-feedback" role="alert"> <strong style="color:#fff;">{{ $message }}</strong> @enderror

            </div>
{{--            <input type="submit" value="LOGIN">--}}
            {{csrf_field()}}
            <button type="submit" class="btn btn-primary">
                 Connexion  </button>
            <div class="wthree-text" style="padding: 7px">
                <ul>
                    <li>
{{--                        <a href="#">Forgot password?</a>--}}
                        @if (Route::has('password.request'))
                            <a  href="{{ route('password.request') }}">
                               Mot de passe oublié ?
                            </a>
                        @endif
                    </li>
                </ul>
            </div>
        </form>
    </div>
</div>

<div class="footer-w3l">
    <p class="agileinfo" style="color: #1f2839"> &copy; 2020 {{__('copyright')}} | {{ config('app.name', 'Agromap') }}</p>
</div>
@endsection
<script src="js/jquery.min.js"></script>

{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<style>
    .kld{
        color:#6aa42f;
        color:#1f2839;
    }

</style>
    <script>
        // alert('');

        $(document).ready(function(){
            // alert('');
            $('body').css("background-color","#1f2839");
        });
    </script>--}}
