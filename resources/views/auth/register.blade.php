@extends('layouts.app')

{{--code merdique
//tu as editer regiterUsers.php de laravel lui meme pour rediriger ici--}}
@section('content')
<div class="container-fluid" style="background-color: #1f2839;min-height: 90vh" >
    <div class="row justify-content-center">
        <div class="container">
            {!! Session::get('notification','') !!}
        </div>
        <div class="col-md-8" style="margin-top: 10px">
            <div class="card">
                <div class="card-header text-center">Nouvel utilisateur</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register',[app()->getLocale()]) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{--{{ __('Name') }}--}} Nom</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                       value="{{ old('name') }}" required autocomplete="name" autofocus
                                >

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{--{{ __('E-Mail Address') }}--}} Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email')
                                        is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{--{{ __('Password') }}--}} Mot de passe</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                       name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{--{{ __('Confirm Password') }}--}}Confirmer mot de passe</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">{{--{{ __('Confirm Password') }}--}}Type utilisateur</label>

                            <div class="col-md-6">
                                <select id="type" class="form-control col-form-label text-md-right" name="type" required>
                                    <option value >Chossissez le type</option>
                                    <option value="editeur" >Editeur</option>
                                    <option value="admin" >Administrateur</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
{{--                            <label for="creer_par" class="col-md-4 col-form-label text-md-right">--}}{{--{{ __('Confirm Password') }}--}}{{--Creer par</label>--}}

                            <div class="col-md-6">
                                <input id="creer_par" type="hidden" class="form-control" name="creer_par" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-warning">
                                    Enregistrer l'utilisateur
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection