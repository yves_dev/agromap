<!DOCTYPE html>
<html lang="fr" class="no-js" style="overflow-x:hidden;">

{{--**************************
*************DEPENDANCES********
**********************--}}
{{-- Mirrored from theme-stall.com/nurseryplant/demos/home/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Jul 2020 12:02:12 GMT --}}
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{$titre}} &#8211; {{$parametre['nom_entreprise']}}</title>


    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }

        .footer-social-link a,.footer-social-link a{
            color: #1f2839;
            background-color: #fff;
        }
        #coeur{
            animation: 3s battement infinite;
        }

        @keyframes battement {
            0%,40%,80%
            {
                transform: scale( .75 );
            }
            20%,60%,100%
            {
                transform: scale( 1 );
            }

        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css'  href='/assets/wp-includes/css/dist/block-library/style.minb62d.css?ver=5.1.6' type='text/css' media='all' />

    <link rel='stylesheet' id='post-views-counter-frontend-css'  href='/assets/wp-content/plugins/post-views-counter/css/frontend9e1e.css?ver=1.3.2'
          type='text/css' media='all' />
    <link rel='stylesheet' id='siteorigin-panels-front-css'  href='/assets/wp-content/plugins/siteorigin-panels/css/front-flex.min6af1.css?ver=2.11.0'
          type='text/css' media='all' />
    <style id='dashicons-inline-css' type='text/css'>
        [data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
    </style>


    <style id='yith-quick-view-inline-css' type='text/css'>

        #yith-quick-view-modal .yith-wcqv-main{background:#ffffff;}
        #yith-quick-view-close{color:#cdcdcd;}
        #yith-quick-view-close:hover{color:#ff0000;}
    </style>

    <link rel='stylesheet' id='bootstrap-css'  href='/assets/wp-content/themes/nurseryplant/css/bootstrap.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-select-css'  href='/assets/wp-content/themes/nurseryplant/css/bootstrap-select.css' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='/assets/wp-content/themes/nurseryplant/css/font-awesome.min.css' type='text/css' media='all' />
    <style id='font-awesome-inline-css' type='text/css'>
        [data-font="FontAwesome"]:before {
            font-family: 'FontAwesome' !important;
            content: attr(data-icon) !important;
            speak: none !important;
            font-weight: normal !important;
            font-variant: normal !important;
            text-transform: none !important;
            line-height: 1 !important;
            font-style: normal !important;
            -webkit-font-smoothing: antialiased !important;
            -moz-osx-font-smoothing: grayscale !important;
        }
    </style>
    <link rel='stylesheet' id='owl-carousel-css'  href='/assets/wp-content/themes/nurseryplant/css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' id='animate-css'  href='/assets/wp-content/themes/nurseryplant/css/animate.css' type='text/css' media='all' />
    <link rel='stylesheet' id='slickslider-css'  href='/assets/wp-content/themes/nurseryplant/css/slick.css' type='text/css' media='all' />
    <link rel='stylesheet' id='slimmenu-css'  href='/assets/wp-content/themes/nurseryplant/css/slimmenu.css' type='text/css' media='all' />
    <link rel='stylesheet' id='prettyPhoto-css'  href='/assets/wp-content/themes/nurseryplant/css/prettyPhoto.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nurseryplant-styles-css'  href='/assets/wp-content/themes/nurseryplant/css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nurseryplant-style-css'  href='/assets/wp-content/themes/nurseryplant/styleb62d.css?ver=5.1.6' type='text/css' media='all' />
    <link rel='stylesheet' id='nurseryplant-responsive-css'  href='/assets/wp-content/themes/nurseryplant/css/responsive.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nurseryplant-colors-css'  href='/assets/wp-content/themes/nurseryplant/css/colors.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nurseryplant-custom-banner-style-css'  href='/assets/wp-content/themes/nurseryplant/css/custom_style_bannerb62d.css?ver=5.1.6' type='text/css' media='all' />


    <style id='nurseryplant-custom-banner-style-inline-css' type='text/css'>
        .container {
            max-width: 1170px;
        }

        #sidebar {
        }

        .topbar.style_1 {
            background-color: #fed602;
        }

        .navbar, .navbar.navbar-default, .header-widget-area {
            background-color: #1f2839;
        }

        .navbar .navbar-nav > li.current-menu-item > a, .navbar .navbar-nav > li:hover > a, .dropdown-menu {
            background-color: #6bb42f;
        }

        .header.affix, .header.affix .navbar, .header.affix .navbar.navbar-default {
            background-color: #1f2839;
        }

        .cssload-loader:after, .team-member-link a, .featured-buttons .yith-wcwl-add-to-wishlist .add_to_wishlist, .yith-wcwl-wishlistaddedbrowse.show a, .yith-wcwl-wishlistexistsbrowse.show a, .product-loop .owl-custom .owl-nav [class*="owl-"], .btn, post-password-form input[type="submit"], .pricing-footer, .pricing-box h3, .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt[disabled]:disabled, .woocommerce #respond input#submit.alt[disabled]:disabled:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt[disabled]:disabled, .woocommerce a.button.alt[disabled]:disabled:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt[disabled]:disabled, .woocommerce button.button.alt[disabled]:disabled:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt[disabled]:disabled, .woocommerce input.button.alt[disabled]:disabled:hover, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce div.product .woocommerce-tabs ul.tabs li:hover, .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woocommerce #respond input#submit,
        .woocommerce a.button,.woocommerce button.button,.woocommerce input.button,.dmtop,.price-with-title.hover-des.price-with-title.hover-des .yith-wcwl-add-to-wishlist .add_to_wishlist:after,.price-with-title.hover-des.price-with-title.hover-des .yith-wcwl-wishlistexistsbrowse a:after,.price-with-title.hover-des.price-with-title.hover-des .yith-wcwl-wishlistexistsbrowse.show a:after,.price-with-title,.date-container,.product-section-title,.search-form .search-submit,.tnp-widget-minimal input.tnp-submit,.navbar-default .navbar-nav > .open > a,.navbar-default .navbar-nav > .open > a:hover,
        .navbar-default .navbar-nav > .open > a:focus,.navbar .navbar-nav > li > a:hover,.navbar .navbar-nav > li > a:focus,.navbar .navbar-nav > .active > a,.navbar .navbar-nav > .active > a:hover,
        .navbar .navbar-nav > .active > a:focus,.navbar .navbar-nav > li.current-menu-item > a,.navbar .navbar-nav > li:hover > a,.dropdown-menu,.project-loop.owl-theme .owl-nav [class*="owl-"]{ background-color: #6bb42f; } .team-member-name p,.product-featured h4,.product-featured h3,.recentposts li a,.wpcf7-form .btn.dropdown-toggle.btn-default,.form-control::-ms-input-placeholder,.form-control::-webkit-input-placeholder,.form-control::-moz-placeholder,.post-password-form input[type="password"],.form-control,.product-country,.woocommerce-message::before,.woocommerce div.product .woocommerce-tabs ul.tabs li a,.woocommerce-info::before,.panel-grid-cell .widget-title,.testi-content h4,.tnp-widget-minimal input.tnp-email,.navbar .navbar-nav > li.search-icon-header:hover > a,.navbar .navbar-nav > li.cart-header-menu:hover > a,a,.slim-wrap ul.menu-items li a:hover,.breadcrumb > li a{ color: #6bb42f; } .cssload-loader:before,.product-loop .owl-custom .owl-nav [class*="owl-"],.wpcf7-form .btn.dropdown-toggle.btn-default,.form-control:focus,.post-password-form input[type="password"],.form-control,.woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt[disabled]:disabled, .woocommerce #respond input#submit.alt[disabled]:disabled:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt[disabled]:disabled, .woocommerce a.button.alt[disabled]:disabled:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt[disabled]:disabled, .woocommerce button.button.alt[disabled]:disabled:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt[disabled]:disabled, .woocommerce input.button.alt[disabled]:disabled:hover,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,.woocommerce #respond input#submit,.woocommerce a.button,.woocommerce button.button,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               .woocommerce input.button,.search-form .search-submit,.tnp-widget-minimal input.tnp-submit,.project-loop.owl-theme .owl-nav [class*="owl-"]{ border-color: #6bb42f; } .woocommerce-info,.woocommerce-message{ border-top-color: #6bb42f; } .counter-section,#comingsoon .tnp-widget-minimal input.tnp-email,.navbar,.navbar.navbar-default,.header-widget-area-empty,.header-widget-area{ background-color: #1f2839; } .team-member-link a:hover, .featured-buttons .yith-wcwl-add-to-wishlist .add_to_wishlist:hover,.yith-wcwl-wishlistaddedbrowse.show a:hover,.yith-wcwl-wishlistexistsbrowse.show a:hover,.product-loop .owl-custom .owl-nav [class*="owl-"]:hover,.list-inline.social li a,.tagwidget a,.recentposts .text h6,.recentposts li a:hover, .bootstrap-select .btn.dropdown-toggle:hover, .bootstrap-select .btn.dropdown-toggle:focus,.btn:hover,.btn:focus,.open > .dropdown-toggle.btn-default:hover,.open > .dropdown-toggle.btn-default:focus,.open > .dropdown-toggle.btn-default,.post-password-form input[type="submit"]:hover,.pricing-box.dark,.pricing-box.dark .panel-default > .panel-heading,.blog-meta ul li a,.woocommerce #respond input#submit.disabled:hover, .woocommerce #respond input#submit:disabled:hover, .woocommerce #respond input#submit[disabled]:disabled:hover, .woocommerce a.button.disabled:hover, .woocommerce a.button:disabled:hover, .woocommerce a.button[disabled]:disabled:hover, .woocommerce button.button.disabled:hover, .woocommerce button.button:disabled:hover, .woocommerce button.button[disabled]:disabled:hover, .woocommerce input.button.disabled:hover, .woocommerce input.button:disabled:hover, .woocommerce input.button[disabled]:disabled:hover,.woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover,.woocommerce #respond input#submit.alt:hover,.woocommerce a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce input.button.alt:hover,.dmtop:hover,.dmtop:hover i,.copyrights li a,.copyrights p,.copyrights,.breadcrumb > li + li::before,.breadcrumb > .active,.breadcrumb,.short-description-product,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      .short-description-product p,.price-with-title.hover-des.price-with-title.hover-des .yith-wcwl-add-to-wishlist .add_to_wishlist:hover:after,.price-with-title.hover-des.price-with-title.hover-des .yith-wcwl-wishlistexistsbrowse a:hover:after,.price-with-title.hover-des.price-with-title.hover-des .yith-wcwl-wishlistexistsbrowse.show a:hover:after,.price-with-title.hover-des.price-with-title.hover-des .yith-wcwl-add-to-wishlist .add_to_wishlist:hover,.owl-custom .owl-nav [class*="owl-"]:hover,.tnp-widget-minimal input.tnp-submit:hover,.woocommerce #respond input#submit.disabled:hover,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      .woocommerce #respond input#submit:disabled:hover,.woocommerce #respond input#submit[disabled]:disabled:hover,.woocommerce a.button.disabled:hover,.woocommerce a.button:disabled:hover,.woocommerce a.button[disabled]:disabled:hover,.woocommerce button.button.disabled:hover,.woocommerce button.button:disabled:hover,.woocommerce button.button[disabled]:disabled:hover,.woocommerce input.button.disabled:hover,.woocommerce input.button:disabled:hover,.woocommerce input.button[disabled]:disabled:hover,.topbar ul li,.topbar ul li a,a:hover,a:focus,.lead,h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,body{ color: #1f2839; } .owl-carousel .owl-item .testibox img,blockquote{ border-color: #1f2839; } .product-featured h4{ border-bottom-color: #1f2839; }
    </style>
{{--    <link rel='stylesheet' id='newsletter-css'  href='/assets/wp-content/plugins/newsletter/stylef244.css?ver=6.7.4' type='text/css' media='all' />--}}

    <script type='text/javascript' src='/assets/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type='text/javascript' src='/assets/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>


    <style type="text/css" media="all"
           id="siteorigin-panels-layouts-head">
        /* Layout 22 */
        #pgc-22-0-0, #pgc-22-1-0, #pgc-22-3-0, #pgc-22-4-0, #pgc-22-6-0, #pgc-22-7-0, #pgc-22-8-0, #pgc-22-9-0, #pgc-22-12-0, #pgc-22-13-0, #pgc-22-14-0, #pgc-22-15-0 {
            width: 100%;
            width: calc(100% - (0 * 30px))
        }

        #pg-22-0, #pg-22-1, #pg-22-2, #pg-22-3, #pg-22-4, #pg-22-5, #pg-22-6, #pg-22-7, #pg-22-8, #pg-22-9, #pg-22-10, #pg-22-11, #pg-22-12, #pg-22-13, #pg-22-14, #pl-22 .so-panel:last-child {
            margin-bottom: 0px
        }

        #pgc-22-2-0, #pgc-22-2-1, #pgc-22-5-2 {
            width: 50%;
            width: calc(50% - (0.5 * 30px))
        }

        #pgc-22-5-0, #pgc-22-10-0 {
            width: 40%;
            width: calc(40% - (0.6 * 30px))
        }

        #pgc-22-5-1, #pgc-22-11-1 {
            width: 10%;
            width: calc(10% - (0.9 * 30px))
        }

        #pgc-22-10-1 {
            width: 60%;
            width: calc(60% - (0.4 * 30px))
        }

        #pgc-22-11-0 {
            width: 54.9611%;
            width: calc(54.9611% - (0.4503893214683 * 30px))
        }

        #pgc-22-11-2 {
            width: 35.0389%;
            width: calc(35.0389% - (0.6496106785317 * 30px))
        }

        #pl-22 .so-panel {
            margin-bottom: 30px
        }

        #pg-22-0.panel-no-style, #pg-22-0.panel-has-style > .panel-row-style, #pg-22-1.panel-no-style, #pg-22-1.panel-has-style > .panel-row-style, #pg-22-2.panel-no-style, #pg-22-2.panel-has-style > .panel-row-style, #pg-22-3.panel-no-style, #pg-22-3.panel-has-style > .panel-row-style, #pg-22-4.panel-no-style, #pg-22-4.panel-has-style > .panel-row-style, #pg-22-5.panel-no-style, #pg-22-5.panel-has-style > .panel-row-style, #pg-22-6.panel-no-style, #pg-22-6.panel-has-style > .panel-row-style, #pg-22-7.panel-no-style, #pg-22-7.panel-has-style > .panel-row-style, #pg-22-8.panel-no-style, #pg-22-8.panel-has-style > .panel-row-style, #pg-22-9.panel-no-style, #pg-22-9.panel-has-style > .panel-row-style, #pg-22-10.panel-no-style, #pg-22-10.panel-has-style > .panel-row-style, #pg-22-11.panel-no-style, #pg-22-11.panel-has-style > .panel-row-style, #pg-22-12.panel-no-style, #pg-22-12.panel-has-style > .panel-row-style, #pg-22-13.panel-no-style, #pg-22-13.panel-has-style > .panel-row-style, #pg-22-14.panel-no-style, #pg-22-14.panel-has-style > .panel-row-style {
            -webkit-align-items: flex-start;
            align-items: flex-start
        }

        #pg-22-1 > .panel-row-style {
            margin-top: -40px;
            z-index: 5
        }

        #panel-22-1-0-0 > .panel-widget-style {
            background-color: #ffffff;
            padding: 40px 40px 40px 40px;
            -moz-box-shadow: 0 0 5px #1f2839;
            -webkit-box-shadow: 0 0 5px #1f2839;
            box-shadow: 0 0 5px #1f2839;
            border-radius: 2px
        }

        #pg-22-2 > .panel-row-style, #pg-22-6 > .panel-row-style, #pg-22-9 > .panel-row-style, #pg-22-12 > .panel-row-style, #pg-22-14 > .panel-row-style {
            padding: 70px 0px 70px 0px
        }

        #pg-22-3 > .panel-row-style {
            padding: 0px 0px 55px 0px
        }

        #panel-22-3-0-1 > .panel-widget-style {
            padding: 0px 140px 0px 140px
        }

        #pg-22-4 > .panel-row-style, #pg-22-7 > .panel-row-style, #pg-22-10 > .panel-row-style {
            padding: 70px 0px 70px 0px
        }

        #pg-22-5 > .panel-row-style {
            background-color: #1f2839;
            background-image: url('/uploaded_image/newsletter/bgnewsletter.png');
            background-position: center center;
            background-size: cover;
            padding: 40px 0px 40px 0px
        }

        #pgc-22-5-0, #pgc-22-5-1, #pgc-22-11-0, #pgc-22-11-1 {
            align-self: auto
        }

        #pg-22-8 > .panel-row-style {
            padding: 70px 0px 0px 0px
        }

        #panel-22-10-0-0 > .panel-widget-style {
            padding: 0px 35px 0px 0px
        }

        #panel-22-10-1-0 > .panel-widget-style {
            padding: 40px 50px 40px 50px;
            background: rgba(255, 255, 255, 0.8)
        }


        #panel-22-11-2-0 > .panel-widget-style {
            padding: 35px 0px 0px 0px
        }

        #pg-22-15 > .panel-row-style {
            margin-bottom: -40px;
            z-index: 1
        }

        #panel-22-15-0-0 > .panel-widget-style {
            background-color: #ffffff;
            padding: 40px 40px 40px 40px;
            -moz-box-shadow: 0 0 5px #1f2839;
            -webkit-box-shadow: 0 0 5px #1f2839;
            box-shadow: 0 0 5px #1f2839;
            border-radius: 2px;
            z-index: 1
        }

        @media (max-width: 780px) {
            #pg-22-0.panel-no-style, #pg-22-0.panel-has-style > .panel-row-style, #pg-22-1.panel-no-style, #pg-22-1.panel-has-style > .panel-row-style, #pg-22-2.panel-no-style, #pg-22-2.panel-has-style > .panel-row-style, #pg-22-3.panel-no-style, #pg-22-3.panel-has-style > .panel-row-style, #pg-22-4.panel-no-style, #pg-22-4.panel-has-style > .panel-row-style, #pg-22-5.panel-no-style, #pg-22-5.panel-has-style > .panel-row-style, #pg-22-6.panel-no-style, #pg-22-6.panel-has-style > .panel-row-style, #pg-22-7.panel-no-style, #pg-22-7.panel-has-style > .panel-row-style, #pg-22-8.panel-no-style, #pg-22-8.panel-has-style > .panel-row-style, #pg-22-9.panel-no-style, #pg-22-9.panel-has-style > .panel-row-style, #pg-22-10.panel-no-style, #pg-22-10.panel-has-style > .panel-row-style, #pg-22-11.panel-no-style, #pg-22-11.panel-has-style > .panel-row-style, #pg-22-12.panel-no-style, #pg-22-12.panel-has-style > .panel-row-style, #pg-22-13.panel-no-style, #pg-22-13.panel-has-style > .panel-row-style, #pg-22-14.panel-no-style, #pg-22-14.panel-has-style > .panel-row-style, #pg-22-15.panel-no-style, #pg-22-15.panel-has-style > .panel-row-style {
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column
            }

            #pg-22-0 > .panel-grid-cell, #pg-22-0 > .panel-row-style > .panel-grid-cell, #pg-22-1 > .panel-grid-cell, #pg-22-1 > .panel-row-style > .panel-grid-cell, #pg-22-2 > .panel-grid-cell, #pg-22-2 > .panel-row-style > .panel-grid-cell, #pg-22-3 > .panel-grid-cell, #pg-22-3 > .panel-row-style > .panel-grid-cell, #pg-22-4 > .panel-grid-cell, #pg-22-4 > .panel-row-style > .panel-grid-cell, #pg-22-5 > .panel-grid-cell, #pg-22-5 > .panel-row-style > .panel-grid-cell, #pg-22-6 > .panel-grid-cell, #pg-22-6 > .panel-row-style > .panel-grid-cell, #pg-22-7 > .panel-grid-cell, #pg-22-7 > .panel-row-style > .panel-grid-cell, #pg-22-8 > .panel-grid-cell, #pg-22-8 > .panel-row-style > .panel-grid-cell, #pg-22-9 > .panel-grid-cell, #pg-22-9 > .panel-row-style > .panel-grid-cell, #pg-22-10 > .panel-grid-cell, #pg-22-10 > .panel-row-style > .panel-grid-cell, #pg-22-11 > .panel-grid-cell, #pg-22-11 > .panel-row-style > .panel-grid-cell, #pg-22-12 > .panel-grid-cell, #pg-22-12 > .panel-row-style > .panel-grid-cell, #pg-22-13 > .panel-grid-cell, #pg-22-13 > .panel-row-style > .panel-grid-cell, #pg-22-14 > .panel-grid-cell, #pg-22-14 > .panel-row-style > .panel-grid-cell, #pg-22-15 > .panel-grid-cell, #pg-22-15 > .panel-row-style > .panel-grid-cell {
                width: 100%;
                margin-right: 0
            }

            #pgc-22-2-0, #pgc-22-5-0, #pgc-22-5-1, #pgc-22-10-0, #pgc-22-11-0, #pgc-22-11-1 {
                margin-bottom: 30px
            }

            #pl-22 .panel-grid-cell {
                padding: 0
            }

            #pl-22 .panel-grid .panel-grid-cell-empty {
                display: none
            }

            #pl-22 .panel-grid .panel-grid-cell-mobile-last {
                margin-bottom: 0px
            }

            #panel-22-1-0-0 > .panel-widget-style, #panel-22-10-1-0 > .panel-widget-style, #panel-22-15-0-0 > .panel-widget-style {
                padding: 15px 15px 15px 15px
            }

            #pg-22-2 > .panel-row-style {
                padding: 30px 0px 30px 0px
            }

            #pg-22-3 > .panel-row-style {
                padding: 0px 0px 40px 0px
            }

            #panel-22-3-0-1 > .panel-widget-style, #panel-22-10-0-0 > .panel-widget-style {
                padding: 0px 0px 0px 0px
            }

            #pg-22-4 > .panel-row-style, #pg-22-7 > .panel-row-style, #pg-22-10 > .panel-row-style {
                padding: 40px 0px 40px 0px;
                background-attachment: scroll
            }

            #pg-22-5 > .panel-row-style, #pg-22-11 > .panel-row-style {
                padding: 20px 0px 20px 0px
            }

            #pg-22-6 > .panel-row-style, #pg-22-9 > .panel-row-style, #pg-22-12 > .panel-row-style, #pg-22-14 > .panel-row-style {
                padding: 40px 0px 40px 0px
            }

            #pg-22-8 > .panel-row-style {
                padding: 40px 0px 0px 0px
            }

            #panel-22-11-2-0 > .panel-widget-style {
                padding: 10px 0px 0px 0px
            }
        } </style>


{{--    ************************************************************
                    ***************ADJOUTER PAR MOI***************
************************************************************--}}
    <link rel="icon" href="{{$parametre['url_fav_icone']}}" sizes="32x32" />
    <style>
        .topbarSize {
            font-size : 20px;
        }
        html{
            scroll-behavior: smooth;
        }
        .page-title1{
            min-height:70vh;
            background-image: url('/assets/wp-content/themes/nurseryplant/images/blog.jpg');
            height: 60vh;

            /* Create the parallax scrolling effect */
            background-attachment: fixed  !important;
            background-position: center  !important;
            background-repeat: no-repeat !important;
            background-size: cover  !important;
        }
        .page-title1 > .container{
            position:absolute;
            top:50%;
            left:50%;
            transform: translateX(-50%);
        }
        #footer_horraire{
            display: none;
        }
        body{
            overflow-x:hidden;
        }
    </style>


    @yield('style_complementaire')
    @yield('style_perso')
</head>
{{-- Added by HTTrack --}}<meta http-equiv="content-type" content="text/html;charset=UTF-8" />{{-- /Added by HTTrack --}}
{{--**************************
*************HEADER********
**********************--}}

<body class="page-template page-template-page-templates page-template-home-page page-template-page-templateshome-page-php
            page page-id-22 siteorigin-panels siteorigin-panels-before-js wide layout-full" style="overflow-x:hidden;">

{{-- LOADER --}}
{{--<div class="cssload-container" id="loader">
    <div class="cssload-loader"></div>
    <button class="btn btn-dark" style="position: absolute;bottom: 0;right: 0" onclick="document.getElementById('loader').style.display='none'">{{__('desactiver_loader')}}</button>
</div>--}}
{{-- END LOADER --}}

{{-- ******************************************
START SITE HERE
********************************************** --}}
<div id="wrapper">

    <div class="header-tranparent">
        <div class="topbar style_2">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <nav class="topbar-menu">
                            <ul class="list-inline social">
                                @isset($parametre['lien_facebook'])
                                <li>
                                    <a href="{{ $parametre['lien_facebook'] }}" title="Facebook">
                                        <i class="fa fa-facebook topbarSize"></i>
                                    </a>
                                </li>
                                @endisset
                                @isset($parametre['lien_twitter'])
                                    <li>
                                    <a href="{{ $parametre['lien_twitter'] }}" title="instagram">
                                        <i class="fa fa-instagram topbarSize"></i>
                                    </a>
                                </li>
                                @endisset
                                @isset($parametre['lien_youtube'])
                                <li>
                                    <a href="{{ $parametre['lien_youtube'] }}" title="Youtube">
                                        <i class="fa fa-youtube topbarSize"></i>
                                    </a>
                                </li>
                                @endisset
                                @isset($parametre['lien_linkedin'])
                                <li>
                                    <a href="{{ $parametre['lien_linkedin'] }}" title="Linkedin">
                                        <i class="fa fa-linkedin topbarSize"></i>
                                    </a>
                                </li>
                                @endisset
                                @isset($parametre['email'])
                                <li>
                                    <a href="mailto:{{ $parametre['email'] }}" title="{{ $parametre['email'] }}">
                                        <i class="fa fa-envelope topbarSize"></i>
                                    </a>
                                </li>
                                @endisset

                            </ul>
                        </nav><!-- end menu -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12">

                    @isset($parametre['telephones'])
                        <nav class="topbar-menu">
                            <ul class="list-inline text-right"><li> {{ $parametre['telephones'] }} </li></ul>
                        </nav><!-- end menu -->
                    @endisset
                    </div><!-- end col -->
                    <div class="col-md-5 col-sm-12">

                    @isset($parametre['adresse'])
                        <nav class="topbar-menu">
                            <ul class="list-inline text-right"><li> {{ $parametre['adresse'] }} </li></ul>
                        </nav><!-- end menu -->
                    @endisset
                    </div><!-- end col -->
                    <div class="col-md-1">
                        @if(app()->getLocale() == 'fr')
                            <?php
                                $background_fr="background-color: #6aa42f;";
                                $background_en="background-color: #ccc;";
                            ?>
                        @else
                            <?php
                                $background_fr="background-color: #ccc;";
                                $background_en="background-color: #6aa42f;";
                            ?>
                        @endif

{{--                        //POUR LES PROJET ARTICLE ..--}}
                        @isset($id)
                            @if(app()->getLocale() == 'fr')
                                <?php
                                    $id_fr = $id;
                                    $id_en = $id+1;
                                ?>
                            @elseif(app()->getLocale() == 'en')
                                <?php
                                    $id_en = $id;
                                    $id_fr = $id-1;
                                ?>
                            @endif
                            <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(), ['fr',$id_fr])}}"
                                    style="{{$background_fr}}padding: 5px;color: #fff;font-weight: bold;font-size: 16px;margin:0">FR</a>

                            <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),['en',$id_en])}}"
                               style="padding: 5px;{{$background_en}}color: #fff;font-weight: bold;font-size: 16px">EN</a>

                        @else
{{--                            POUR LISTER LES PROJET DU PAYS ON NE DOIT PAYS CHANGER D'iD--}}
                            @isset($id_pays)
                                <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(), ['fr',$id_pays])}}"
                                   style="{{$background_fr}}padding: 5px;color: #fff;font-weight: bold;font-size: 16px;margin:0">FR</a>

                                <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),['en',$id_pays])}}"
                                   style="padding: 5px;{{$background_en}}color: #fff;font-weight: bold;font-size: 16px">EN</a>
                             @else
                                    <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(), 'fr')}}"
                                       style="padding: 5px;{{$background_fr}}color: #fff;font-weight: bold;font-size: 16px;margin:0">FR</a>

                                    <a href="{{route(\Illuminate\Support\Facades\Route::currentRouteName(),'en')}}"
                                       style="padding: 5px;{{$background_en}}color: #fff;font-weight: bold;font-size: 16px;margin:0">EN</a>
                            @endisset
                        @endisset
                    </div>

                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end topbar -->

        <div class="header-widget-area-empty style_2"></div>

        <header class="header header-sticky">
            <div class="container">
                <nav class="navbar navbar-default yamm">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="visible-sec navbar-brand" href="{{route('acceuil_visiteur',app()->getLocale())}}" title="{{ $parametre['nom_entreprise'] }}">
                                <img src="{{ $parametre['url_logo_entreprise'] }}" alt="{{ $parametre['nom_entreprise'] }}">
                                <span class="site-title">{{ $parametre['nom_entreprise'] }}</span>
                            </a>
                        </div>
                        <!-- end navbar header -->
                        <div class="hidden-sec slim-wrap">
                            <a href="{{route('acceuil_visiteur',app()->getLocale())}}" class="home-link-text"><img src="{{$parametre['url_logo_entreprise']}}" alt="" /></a>

{{--************************//FOR MOBILE**********************--}}
{{--************************//FOR MOBILE**********************--}}
                            <ul id="menu-header-menu" class="menu-items">
                                <li id="menu-item-770" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home
                            current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor
                             menu-item-has-children menu-item-770"><a href="{{ route('acceuil_visiteur',app()->getLocale()) }}">{{__("acceuil")}}</a>
                                </li>
                                <li id="menu-item-603" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-603">
                                    <a href="{{route('acceuil_blog',app()->getLocale())}}">Agronews</a>
                                </li>
                                <li id="menu-item-603" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-603">
                                    <a href="{{route('a_propos',app()->getLocale())}}">{{__("a_propos")}}</a>
                                </li>
                                <li id="menu-item-201" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-201">
                                    <a href="{{route('filieres',app()->getLocale())}}">{{__("filieres")}}</a>
                                    <ul class="sub-menu">
                                        @foreach($filieres as $item_filiere)
                                            <li id="menu-item-201" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-201">
                                                <a href="#">{{$item_filiere['nom']}}</a>
                                            </li>
                                        @endforeach
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-201">
                                            <a href="{{route('filieres',app()->getLocale())}}">{{__("toutes_filieres")}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-731" class="menu-item menu-item-type-custom menu-item-object-custom
                            current-menu-ancestor menu-item-has-children menu-item-731"><a href="{{route('services',app()->getLocale())}}">{{__("services")}}</a>
                                    <ul class="sub-menu">
                                        @foreach($services as $item_service)
                                            <li id="menu-item-405" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-405">
                                                <a href="{{route('un_service',[app()->getLocale(),$item_service['id']])}}">{{$item_service['nom']}}</a>
                                            </li>
                                        @endforeach
                                        <li id="menu-item-405" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-405">
                                            <a href="{{route('services',app()->getLocale())}}">{{__("tous_services")}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-731" class="menu-item menu-item-type-custom menu-item-object-custom
                            current-menu-ancestor menu-item-has-children menu-item-731">
                                    <a href="{{route('projets',app()->getLocale())}}">{{__("projets")}}</a>
                                    <ul class="sub-menu">
                                        @foreach($les_pays_ou_il_ya_des_projet as $item_projet)
                                            <li id="menu-item-199" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-199">
                                                <a href="{{route('projets_pays',[app()->getLocale(),$item_projet['id']])}}">{{$item_projet['nom_pays']}}</a>
                                            </li>
                                        @endforeach
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401">
                                            <a href="{{route('projets',app()->getLocale())}}">{{__("tous_projets")}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-406" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                    <a href="{{route('contact',app()->getLocale())}}">{{__("contact")}}</a>
                                </li>

                            </ul>
                        </div>
                        <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse navbar-responsive-collapse visible-sec">


        {{--************************//FOR PC**********************--}}
        {{--************************//FOR PC**********************--}}
                            <ul id="nurseryplantMainMenu" class="nav navbar-nav navbar-right">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children dropdown normal-menu has-submenu menu-item-770">
                                    <a href="{{ route('acceuil_visiteur',app()->getLocale()) }}">{{__("acceuil")}}</a>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown
                                normal-menu has-submenu menu-item-604">
                                    <a href="{{route('services',app()->getLocale())}}">{{__("services")}}</a>
                                    <ul class="dropdown-menu" >
                                        @foreach($services as $item_service)
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401">
                                                <a href="{{route('un_service',[app()->getLocale(),$item_service['id']])}}">{{$item_service['nom']}}</a>
                                            </li>
                                        @endforeach
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401">
                                            <a href="{{route('services',app()->getLocale())}}">{{__("tous_services")}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children
                                    dropdown normal-menu has-submenu menu-item-201">
                                    <a href="{{route('filieres',app()->getLocale())}}">{{__("filieres")}}</a>
                                    <ul class="dropdown-menu" >
                                        @foreach($filieres as $item_filiere)
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401">
                                                <a href="{{route('filieres',app()->getLocale())}}#{{$item_filiere['nom']}}{{$item_filiere['id']}}">{{$item_filiere['nom']}}</a>
                                            </li>
                                        @endforeach
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401">
                                            <a href="{{route('filieres',app()->getLocale())}}">{{__("toutes_filieres")}}</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children
                                    dropdown normal-menu has-submenu menu-item-201">
                                    <a href="{{route('projets',app()->getLocale())}}">{{__("projets")}}</a>
                                    <ul class="dropdown-menu" >
                                        @foreach($les_pays_ou_il_ya_des_projet as $item_pays)
                                            <li id="menu-item-199" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-199">
                                                <a href="{{route('projets_pays',[app()->getLocale(),$item_pays['id']])}}">{{$item_pays['nom_pays']}}</a>
                                            </li>
                                        @endforeach
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401">
                                            <a href="{{route('projets',app()->getLocale())}}">{{__("tous_projets")}}</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown normal-menu has-submenu menu-item-603">
                                    <a href="{{route('acceuil_blog',app()->getLocale())}}">AGRONEWS</a>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown normal-menu has-submenu menu-item-603">
                                    <a href="{{route('a_propos',app()->getLocale())}}">{{__("a_propos")}}</a>
                                </li>

                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-741">
                                    <a href="{{route('contact',app()->getLocale())}}">{{__("contact")}}</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

    </div>
{{--**************************
*************CONTENT********
**********************--}}
    @yield('contenu')


{{--**************************
*************FOOTER********
**********************--}}
    <footer class="footer" id="footer" style="padding-top: 100px">
        <div class="container">

            <div class="row">

                <div class="col-md-4 col-sm-4 footer-widget-area">
                    <div id="black-studio-tinymce-3" class="widget clearfix widget_black_studio_tinymce"><div class="footer-widget-title">
                            <h4>Contact</h4>
                        </div>
                        <div class="textwidget">
{{--                            <p>{{$parametre['description']}}</p><div class="ts-spacer" style="height:20px"></div>--}}

                            @isset($parametre['email']) <p>Email: {{$parametre['email']}}</p> @endisset

                            @isset($parametre['telephones'])<p>Contacts: {{$parametre['telephones']}}</p> @endisset
                            @isset($parametre['horraires'])<p>{{__("horraires")}}: {{$parametre['horraires']}}</p> @endisset
                            @isset($parametre['adresse'])<p>{{$parametre['adresse']}}</p> @endisset
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-sm-4 footer-widget-area">
                    <div id="themestall-1" class="widget clearfix themestall">
                        <div class="footer-widget-title"><h4>{{__("suivez_nous")}}</h4></div>
                                <div class="col-xs-12">
                                    <nav class="footer-social-link">
                                        <ul class="list-inline social">
                                            @isset($parametre['lien_facebook'])
                                                <li>
                                                    <a target="_blank" href="{{$parametre['lien_facebook']}}" title="Facebook">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                </li>
                                            @endisset
                                            @isset($parametre['lien_twitter'])
                                                <li>
                                                    <a target="_blank" href="{{$parametre['lien_twitter']}}" title="instagram">
                                                        <i class="fa fa-instagram"></i>
                                                    </a>
                                                </li>
                                            @endisset
                                            @isset($parametre['lien_youtube'])
                                                <li>
                                                    <a target="_blank" href="{{$parametre['lien_youtube']}}" title="Youtube">
                                                        <i class="fa fa-youtube"></i>
                                                    </a>
                                                </li>
                                            @endisset
                                            @isset($parametre['lien_linkedin'])
                                                <li>
                                                    <a target="_blank" href="{{$parametre['lien_linkedin']}}" title="LinkedIn">
                                                        <i class="fa fa-linkedin"></i>
                                                    </a>
                                                </li>
                                            @endisset

                                        </ul>
                                    </nav>
                                </div>

                </div>
            </div>
{{--            </div>--}}
{{--            </div>--}}


                <div class="col-md-4 col-sm-4 footer-widget-area">
                    {{--                    *************************************************SECTION NEWS LETTERS*****************************************--}}
{{--                    <h1 style="color:#fff" >News letters</h1>--}}
                     {{-- <form class="tnp-form" action="{{route('souscrire_newsletter')}}" method="get" onsubmit="return newsletter_check(this)">
                            <input style="color:#fff !important;" type="email" name="email" required class="form-control" placeholder="Email">
                          <br/>
                            <input class="btn tnp-submit" type="submit"
                                   style="background-color:#fff;border-color:#6bb42f ; !important;color:#1f2839" value="Subscribe">
                        </form>--}}

                    <div class="so-panel widget widget_newsletterwidgetminimal panel-first-child panel-last-child"
                    data-index="8">
                        <div class="tnp tnp-widget-minimal" id="footer_newsletter">
                            <h3 class="footer-widget-title" style="color:#fff"><b>Newsletter</b></h3>
                            <div class="row">
                                <div class="row">
                                    <div id="reponse_recue"></div>
                                </div>
                                <div class="{{--container--}}" style="padding: 10px">
                                    <div class="{{--col-xs-8--}}">
                                        <input class="form-control" style="color:#fff !important;" type="email" required name="email" id="email_a_enregistrer" placeholder="Email">
                                    </div>
                                    <div class="col-xs-3 text-center">
                                        <br/>
                                        <button class="btn tnp-submit" type="button" id="souscrire"
                                            style="margin-left: 0px;background-color:#fff !important;
                                            border-color:#fff!important;color:#1f2839;">{{__("souscrire")}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div id="footer_horraire">
                        @isset($parametre['horraires'])
                            <div class="col-xs-12 text-center">
                                <img src="/assets/wp-content/uploads/2017/05/topicon3.png" class="img-responsive" style="width:100px !important;" />
                            </div>
                            <div class="right-content-service">
                                <h4 class="text-center" style="color: #fff">{{$parametre['horraires']}}</h4>
                            </div>
                        @endisset
                    </div>


{{--            </div>--}}
        </div>{{-- end container --}}
    </footer>

    <div class="copyrights text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p style="text-transform: none">Copyrights © 2020 <a href="#">{{$parametre['nom_entreprise']}}</a> {{__("copyright")}}
                        |
                        {!!  __('fait_par')!!}
                    </p>
                </div>{{-- end col --}}
            </div>{{-- end row --}}
        </div>{{-- end container --}}
    </div>
</div>{{-- end wrapper --}}
<div class="dmtop"><a href="#"><i class="fa fa-angle-up"></i></a></div>

{{--<div id="yith-quick-view-modal">
    <div class="yith-quick-view-overlay"></div>
    <div class="yith-wcqv-wrapper">
        <div class="yith-wcqv-main">
            <div class="yith-wcqv-head">
                <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a>
            </div>
            <div id="yith-quick-view-content" class="woocommerce single-product"></div>
        </div>
    </div>
</div>--}}




<style type="text/css" media="all"
       id="siteorigin-panels-layouts-footer">/* Layout w592c73622bf44 */ #pgc-w592c73622bf44-0-0 , #pgc-w592c73622bf44-0-1 , #pgc-w592c73622bf44-0-2 { width:33%;width:calc(33% - ( 0.75 * 30px ) ) } #pgc-w592c73622bf44-0-3 { width:2%;width:calc(2% - ( 0.98 * 30px ) ) } #pgc-w592c73622bf44-0-4 { width:23%;width:calc(23% - ( 0.77 * 30px ) ) } #pg-w592c73622bf44-0 , #pl-w592c73622bf44 .so-panel:last-child { margin-bottom:0px } #pl-w592c73622bf44 .so-panel { margin-bottom:30px } #pg-w592c73622bf44-0.panel-no-style, #pg-w592c73622bf44-0.panel-has-style > .panel-row-style { -webkit-align-items:flex-start;align-items:flex-start } #panel-w592c73622bf44-0-4-0> .panel-widget-style { padding:15px 0px 15px 0px } @media (max-width:780px){ #pg-w592c73622bf44-0.panel-no-style, #pg-w592c73622bf44-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w592c73622bf44-0 > .panel-grid-cell , #pg-w592c73622bf44-0 > .panel-row-style > .panel-grid-cell { width:100%;margin-right:0 } #pgc-w592c73622bf44-0-0 , #pgc-w592c73622bf44-0-1 , #pgc-w592c73622bf44-0-2 , #pgc-w592c73622bf44-0-3 { margin-bottom:30px } #pl-w592c73622bf44 .panel-grid-cell { padding:0 } #pl-w592c73622bf44 .panel-grid .panel-grid-cell-empty { display:none } #pl-w592c73622bf44 .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  } /* Layout w590f7a7b23a0d */ #pgc-w590f7a7b23a0d-0-0 { width:25%;width:calc(25% - ( 0.75 * 30px ) ) } #pgc-w590f7a7b23a0d-0-1 { width:75%;width:calc(75% - ( 0.25 * 30px ) ) } #pl-w590f7a7b23a0d .so-panel { margin-bottom:30px } #pl-w590f7a7b23a0d .so-panel:last-child { margin-bottom:0px } #pg-w590f7a7b23a0d-0.panel-no-style, #pg-w590f7a7b23a0d-0.panel-has-style > .panel-row-style { -webkit-align-items:flex-start;align-items:flex-start } #panel-w590f7a7b23a0d-0-0-0> .panel-widget-style { background-color:#6bb42f;padding:20px 20px 20px 20px;border-radius: 5px } @media (max-width:780px){ #pg-w590f7a7b23a0d-0.panel-no-style, #pg-w590f7a7b23a0d-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w590f7a7b23a0d-0 > .panel-grid-cell , #pg-w590f7a7b23a0d-0 > .panel-row-style > .panel-grid-cell { width:100%;margin-right:0 } #pgc-w590f7a7b23a0d-0-0 { margin-bottom:30px } #pl-w590f7a7b23a0d .panel-grid-cell { padding:0 } #pl-w590f7a7b23a0d .panel-grid .panel-grid-cell-empty { display:none } #pl-w590f7a7b23a0d .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px } #panel-w590f7a7b23a0d-0-0-0> .panel-widget-style { padding:15px 15px 15px 15px }  } /* Layout w590edbdb4868c */ #pgc-w590edbdb4868c-0-0 , #pgc-w590edbdb4868c-0-1 { width:50%;width:calc(50% - ( 0.5 * 30px ) ) } #pl-w590edbdb4868c .so-panel { margin-bottom:30px } #pl-w590edbdb4868c .so-panel:last-child { margin-bottom:0px } @media (max-width:780px){ #pg-w590edbdb4868c-0.panel-no-style, #pg-w590edbdb4868c-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w590edbdb4868c-0 > .panel-grid-cell , #pg-w590edbdb4868c-0 > .panel-row-style > .panel-grid-cell { width:100%;margin-right:0 } #pgc-w590edbdb4868c-0-0 { margin-bottom:30px } #pl-w590edbdb4868c .panel-grid-cell { padding:0 } #pl-w590edbdb4868c .panel-grid .panel-grid-cell-empty { display:none } #pl-w590edbdb4868c .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  } /* Layout w5905805f0f26c */ #pgc-w5905805f0f26c-0-0 , #pgc-w5905805f0f26c-0-1 , #pgc-w5905805f0f26c-0-2 { width:33.3333%;width:calc(33.3333% - ( 0.66666666666667 * 30px ) ) } #pl-w5905805f0f26c .so-panel { margin-bottom:30px } #pl-w5905805f0f26c .so-panel:last-child { margin-bottom:0px } @media (max-width:780px){ #pg-w5905805f0f26c-0.panel-no-style, #pg-w5905805f0f26c-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w5905805f0f26c-0 > .panel-grid-cell , #pg-w5905805f0f26c-0 > .panel-row-style > .panel-grid-cell { width:100%;margin-right:0 } #pgc-w5905805f0f26c-0-0 , #pgc-w5905805f0f26c-0-1 { margin-bottom:30px } #pl-w5905805f0f26c .panel-grid-cell { padding:0 } #pl-w5905805f0f26c .panel-grid .panel-grid-cell-empty { display:none } #pl-w5905805f0f26c .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px }  } /* Layout w590f7a7b23d1e */ #pgc-w590f7a7b23d1e-0-0 { width:75%;width:calc(75% - ( 0.25 * 30px ) ) } #pgc-w590f7a7b23d1e-0-1 { width:25%;width:calc(25% - ( 0.75 * 30px ) ) } #pl-w590f7a7b23d1e .so-panel { margin-bottom:30px } #pl-w590f7a7b23d1e .so-panel:last-child { margin-bottom:0px } #panel-w590f7a7b23d1e-0-1-0> .panel-widget-style { background-color:#6bb42f;padding:20px 20px 20px 20px;border-radius: 5px } @media (max-width:780px){ #pg-w590f7a7b23d1e-0.panel-no-style, #pg-w590f7a7b23d1e-0.panel-has-style > .panel-row-style { -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column } #pg-w590f7a7b23d1e-0 > .panel-grid-cell , #pg-w590f7a7b23d1e-0 > .panel-row-style > .panel-grid-cell { width:100%;margin-right:0 } #pgc-w590f7a7b23d1e-0-0 { margin-bottom:30px } #pl-w590f7a7b23d1e .panel-grid-cell { padding:0 } #pl-w590f7a7b23d1e .panel-grid .panel-grid-cell-empty { display:none } #pl-w590f7a7b23d1e .panel-grid .panel-grid-cell-mobile-last { margin-bottom:0px } #panel-w590f7a7b23d1e-0-1-0> .panel-widget-style { padding:15px 15px 15px 15px }  }
</style>
{{--<script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
</script>--}}
{{--<script type="text/template" id="tmpl-variation-template">
    <div class="woocommerce-variation-description">--}}{{-- data.variation.variation_description --}}{{--</div>
    <div class="woocommerce-variation-price">--}}{{-- data.variation.price_html --}}{{--</div>
    <div class="woocommerce-variation-availability">--}}{{-- data.variation.availability_html --}}{{--</div>
</script>--}}
<script type="text/template" id="tmpl-unavailable-variation-template">
    <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
{{--

<link rel='stylesheet' id='ts-box-shortcodes-css'  href='/assets/wp-content/plugins/themestall-addons//assets/css/box-shortcodes5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='ts-content-shortcodes-css'  href='/assets/wp-content/plugins/themestall-addons//assets/css/content-shortcodes5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='ts-other-shortcodes-css'  href='/assets/wp-content/plugins/themestall-addons//assets/css/other-shortcodes5152.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='ts-galleries-shortcodes-css'  href='/assets/wp-content/plugins/themestall-addons//assets/css/galleries-shortcodes5152.css?ver=1.0' type='text/css' media='all' />
--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/yith-woocommerce-wishlist//assets/js/jquery.selectBox.min7359.js?ver=1.2.0'></script>--}}
{{--<script type='text/javascript'>
    /* <![CDATA[ */
    var yith_wcwl_l10n = {
        "ajax_url": "\/nurseryplant\/demos\/wp-admin\/admin-ajax.php",
        "redirect_to_cart": "no",
        "multi_wishlist": "",
        "hide_add_button": "1",
        "enable_ajax_loading": "",
        "ajax_loader_url": "http:\/\/theme-stall.com\/nurseryplant\/demos\/wp-content\/plugins\/yith-woocommerce-wishlist\//assets\/images\/ajax-loader-alt.svg",
        "remove_from_wishlist_after_add_to_cart": "1",
        "is_wishlist_responsive": "1",
        "labels": {
            "cookie_disabled": "We are sorry, but this feature is available only if cookies on your browser are enabled.",
            "added_to_cart_message": "<div class=\"woocommerce-notices-wrapper\"><div class=\"woocommerce-message\" role=\"alert\">Product added to cart successfully<\/div><\/div>"
        },
        "actions": {
            "add_to_wishlist_action": "add_to_wishlist",
            "remove_from_wishlist_action": "remove_from_wishlist",
            "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem",
            "load_mobile_action": "load_mobile",
            "delete_item_action": "delete_item",
            "save_title_action": "save_title",
            "save_privacy_action": "save_privacy",
            "load_fragments": "load_fragments"}};
    /* ]]> */
</script>--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/yith-woocommerce-wishlist//assets/js/jquery.yith-wcwl9b4a.js?ver=3.0.11'></script>--}}
{{--<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"http:\/\/theme-stall.com\/nurseryplant\/demos\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
    /* ]]> */
</script>--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/contact-form-7/includes/js/scripts3c21.html?ver=5.1.1'></script>--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>--}}
{{--<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {
        "ajax_url": "\/nurseryplant\/demos\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/nurseryplant\/demos\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "http:\/\/theme-stall.com\/nurseryplant\/demos\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"};
    /* ]]> */
</script>--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/frontend/add-to-cart.minf8ee.js?ver=3.5.7'></script>--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4'></script>--}}
{{--<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/nurseryplant\/demos\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/nurseryplant\/demos\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/frontend/woocommerce.minf8ee.js?ver=3.5.7'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/nurseryplant\/demos\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/nurseryplant\/demos\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_426e5cd916219ce63786f800eb14817f","fragment_name":"wc_fragments_426e5cd916219ce63786f800eb14817f"};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/frontend/cart-fragments.minf8ee.js?ver=3.5.7'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var yith_qv = {"ajaxurl":"\/nurseryplant\/demos\/wp-admin\/admin-ajax.php","loader":"http:\/\/theme-stall.com\/nurseryplant\/demos\/wp-content\/plugins\/yith-woocommerce-quick-view\//assets\/image\/qv-loader.gif","lang":""};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/wp-content/plugins/yith-woocommerce-quick-view//assets/js/frontend.min1159.js?ver=1.4.2'></script>
<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/prettyPhoto/jquery.prettyPhoto.min005e.js?ver=3.1.6'></script>
--}}
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/bootstrap5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/animate5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/lib/jquery.appearb62d.js?ver=5.1.6'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/lib/counterb62d.js?ver=5.1.6'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/lib/countdownb62d.js?ver=5.1.6'></script>
<script type='text/javascript' src='/assets/wp-includes/js/swfobjecteb77.js?ver=2.2-20120417'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/lib/modernizr.videob62d.js?ver=5.1.6'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/lib/video_backgroundb62d.js?ver=5.1.6'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/owl.carousel.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/bootstrap-select5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/lib/jquery.easing.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/jquery.slimmenu.min5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/jquery.fitvid5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/jquery.prettyPhoto5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/chart5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/slick5152.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/scriptsb62d.js?ver=5.1.6'></script>
<script type='text/javascript' src='/assets/wp-content/themes/nurseryplant/js/customb62d.js?ver=5.1.6'></script>
{{--<script type='text/javascript'>
    /* <![CDATA[ */
    var newsletter = {"messages":{"email_error":"The email is not correct","name_error":"The name is not correct","surname_error":"The last name is not correct","profile_error":"A mandatory field is not filled in","privacy_error":"You must accept the privacy statement"},"profile_max":"20"};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/wp-content/plugins/newsletter/subscription/validatef244.js?ver=6.7.4'></script>--}}
<script type='text/javascript' src='/assets/wp-includes/js/wp-embed.minb62d.js?ver=5.1.6'></script>
{{--<script type='text/javascript'>
    /* <![CDATA[ */
    var panelsStyles = {"fullContainer":"body"};
    /* ]]> */
</script>--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/siteorigin-panels/js/styling-2110.min6af1.js?ver=2.11.0'></script>--}}
{{--<script type='text/javascript'>
    /* <![CDATA[ */
    var ts_other_shortcodes = {"no_preview":"This shortcode doesn't work in live preview. Please insert it into editor and preview on the site."};
    /* ]]> */
</script>--}}
{{--<script type='text/javascript' src='/assets/wp-content/plugins/themestall-addons//assets/js/other-shortcodes5152.js?ver=1.0'></script>--}}
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCN-MbF-tNeFjUL62lar8UsgI0eE47u1Ks&amp;ver=1.0.0-beta'></script>
{{--<script type='text/javascript' src='/assets/wp-content/plugins/themestall-addons//assets/js/bg-map5152.js?ver=1.0'></script>--}}
<script type='text/javascript' src='/assets/wp-includes/js/underscore.min4511.js?ver=1.8.3'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpUtilSettings = {"ajax":{"url":"\/nurseryplant\/demos\/wp-admin\/admin-ajax.php"}};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/wp-includes/js/wp-util.minb62d.js?ver=5.1.6'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/nurseryplant\/demos\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"Sorry, no products matched your selection. Please choose a different combination.","i18n_make_a_selection_text":"Please select some product options before adding this product to your cart.","i18n_unavailable_text":"Sorry, this product is unavailable. Please choose a different combination."};
    /* ]]> */
</script>
{{--<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/frontend/add-to-cart-variation.minf8ee.js?ver=3.5.7'></script>--}}
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"","zoom_options":[],"photoswipe_enabled":"","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":""};
    /* ]]> */
</script>
{{--<script type='text/javascript' src='/assets/wp-content/plugins/woocommerce//assets/js/frontend/single-product.minf8ee.js?ver=3.5.7'></script>--}}
<script type="text/javascript">document.body.className = document.body.className.replace("siteorigin-panels-before-js","");</script>




{{--<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>--}}
@yield('script_complementaire')
<script>
    $('#souscrire').click(function () {
        let email = $('#email_a_enregistrer').val();
        $.get("{{route('souscrire_newsletter',app()->getLocale())}}?email="+email,function (reponse) {
            $('#reponse_recue').html(reponse);
            $('#email_a_enregistrer').val("");
            setTimeout( function(){$('#reponse_recue').html('')},5000);
        })
    })

</script>

</body>
</html>