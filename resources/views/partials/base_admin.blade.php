@extends('layouts.app')
@section('style_complementaire')
    <link rel="stylesheet" href="/admin_assets/css/vertical_menu.css" />

    <!-- Main Quill library -->
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">

    <style>
        .btn-actif{
            background-color: #6aa42f;
            color: #fff;
            min-width: 200px;
            margin-bottom: 10px;
        }

        .btn-inactif{
            background-color: #1f2839;
            color: #fff;
            min-width: 200px;
            margin-bottom: 10px;
        }

        .btn-actif:hover, .btn-inactif:hover{
            background-color: #fff;
            color: #1f2839;
            border: 1px solid #1f2839;
        }

        .titre_section_general{
            text-decoration: underline;
            font-weight: bold;
        }

        .div_titre_section_specifique{
            margin-bottom: 25px;
        }
        .titre_section_specifique{
            border: 2px solid #6aa42f;
            padding:10px;
            margin-bottom: 10px;
        }
    </style>

    @yield('style_perso')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="vertical-nav bg-white active" id="sidebar" style="overflow-y: auto">
            <div class="py-4 px-3 mb-4 bg-light">
                <div class="media d-flex align-items-center">
                    <div class="media-body">
                        <h4 class="m-0">Agromap</h4>
                    </div>
                </div>
                <!-- Toggle button -->
                <button type="button" class="sidebarCollapse btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4">
                    <i class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Fermer le menu</small>
                </button>
            </div>



{{--            <p class="text-gray font-weight-bold text-uppercase px-3 small pb-4 mb-0"> Tableau de bord</p>--}}
            <ul class="nav flex-column bg-white mb-0">
                <li class="nav-item">
                    <a href="{{route('home')}}" class="nav-link text-dark bg-light">
                        <i class="fa fa-th-large mr-3 text-primary fa-fw"></i>
                        Tableau de bord
                    </a>
                </li>
            </ul>

            <p class="text-gray font-weight-bold text-uppercase px-3 small mb-0">Basique</p>
            <ul class="nav flex-column bg-white mb-0">
                <li class="nav-item">
                    <a href="{{route('gestion_parametre')}}" class="nav-link text-dark bg-light">
                        <i class="fa fa-pie-chart mr-3 text-primary fa-fw"></i>
                        Information general
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('gestion_slider')}}" class="nav-link text-dark bg-light">
                        <i class="fa fa-home mr-3 text-primary fa-fw"></i>
                        Page d'acceuil
                    </a>
                </li>
            </ul>

            <p class="text-gray font-weight-bold text-uppercase px-3 small mb-0">Publications</p>

            <ul class="nav flex-column bg-white mb-0">
                <li class="nav-item">
                    <a href="{{route('gestion_projet')}}" class="nav-link text-dark bg-light">
                        <i class="fa fa-tasks mr-3 text-primary fa-fw"></i>
                        Projet
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('gestion_blog')}}" class="nav-link text-dark bg-light">
                        <i class="fab fa-newspaper-o mr-3 text-primary fa-fw"></i>
                        Blog
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('gestion_service')}}" class="nav-link text-dark bg-light">
                        <i class="fa fa-industry mr-3 text-primary fa-fw"></i>
                        Service
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('gestion_filiere')}}" class="nav-link text-dark bg-light">
                        <i class="fa fa-leaf mr-3 text-primary fa-fw"></i>
                        Filiere
                    </a>
                </li>
            </ul>

            <p class="text-gray font-weight-bold text-uppercase px-3 small mb-0">Entreprise</p>

            <ul class="nav flex-column bg-white mb-0">
                <li class="nav-item">
                    <a href="{{route('newsletter')}}" class="nav-link text-dark bg-light">
                        <i class="fa fa-envelope mr-3 text-primary fa-fw"></i>
                        Newsletter
                    </a>
                </li>

               <li class="nav-item">
                   <a href="{{route('pays_region')}}" class="nav-link text-dark bg-light">
                       <i class="fa fa-flag mr-3 text-primary fa-fw"></i>
                       Pays/Region
                   </a>
               </li>
            </ul>
        </div>
        </div>
        <!-- FIN MENU VERTICAL -->
        <!-- FIN MENU VERTICAL -->
        <!-- FIN MENU VERTICAL -->


        <!-- CONTENU DE PAGE -->
        <div class="page-content p-5 active" id="content">
            @yield('contenu')
        </div>

        @section('script_complementaire')
            <script src="/js/jquery.min.js"></script>
            <script type="text/javascript" src="/admin_assets/js/vertical_menu.js"></script>


            <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

            <script>
                function toggle_to_ajout(){
                    $("#div_liste").hide(300);
                    $("#div_ajout").show(650);

                    $("#btn_ajouter").css({"background-color":"#6aa42f","color":"#fff","border":"none"});
                    $("#btn_liste").css({"background-color":"#ccc","color":"#fff","border":"none"});
                }
                function toggle_to_liste(){
                    $("#div_ajout").hide(300);
                    $("#div_liste").show(650);

                    $("#btn_liste").css({"background-color":"#6aa42f","color":"#fff","border":"none"});
                    $("#btn_ajouter").css({"background-color":"#ccc","color":"#fff","border":"none"});
                }

                toggle_to_liste();
            </script>

            @yield('script_perso');
        @endsection
@endsection