@extends('partials.base_admin')

@section('style_perso')
    <style>
        .item_tableau{
            width: 100%;
            height: 200px;
            background-color: #1f2839;
            color: #fff;
            text-align: center;
            position: relative;
        }
        .item_tableau .text_contenu{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translateX(-50%) translateY(-50%);
            font-weight: bold;
        }

        .item_tableau .text_contenu h3{
            color: yellow;
        }

        .item_tableau:hover{
            background-color: #6aa42f;
            color: #fff;
            border-radius: 8px;
        }
    </style>

@endsection

@section('contenu')
        <!-- CONTENU DE PAGE -->
        <div class="container">
            {!! Session::get('notification','') !!}
            <div class="row">
                {{--item tableau--}}
                <div class="col-md-4 col-sm-6" style="padding: 5px">
                    <a href="{{route('gestion_projet')}}">
                        <div class="item_tableau">
                            <div class="text_contenu">
                                <h3>{{$projet}}</h3>
                                <h1>Projets</h1>
                            </div>
                        </div>
                    </a>
                </div>
                {{--item tableau--}}
                <div class="col-md-4 col-sm-6" style="padding: 5px">
                    <a href="{{route('gestion_blog')}}">
                        <div class="item_tableau">
                            <div class="text_contenu">
                                <h3>{{$nb_article}}</h3>
                                <h1>Agronews</h1>
                            </div>
                        </div>
                    </a>
                </div>
                {{--item tableau--}}
                <div class="col-md-4 col-sm-6" style="padding: 5px">
                    <a href="{{route('gestion_service')}}">
                        <div class="item_tableau">
                            <div class="text_contenu">
                                <h3>{{$services}}</h3>
                                <h1>Services</h1>
                            </div>
                        </div>
                    </a>
                </div>
                {{--item tableau--}}
                <div class="col-md-4 col-sm-6" style="padding: 5px">
                    <a href="{{route('gestion_filiere')}}">
                        <div class="item_tableau">
                            <div class="text_contenu">
                                <h3>{{$filieres}}</h3>
                                <h1>Filieres</h1>
                            </div>
                        </div>
                    </a>
                </div>
                {{--item tableau--}}
                <div class="col-md-4 col-sm-6" style="padding: 5px">
                    <a href="{{route('newsletter')}}">
                        <div class="item_tableau">
                            <div class="text_contenu">
                                <h3>{{$nb_email}}</h3>
                                <h1>Abonnés(es) Newsletter</h1>
                            </div>
                        </div>
                    </a>
                </div>
                {{--item tableau--}}
                <div class="col-md-4 col-sm-6" style="padding: 5px">
                    <a href="{{route('gestion_apropos')}}">
                        <div class="item_tableau">
                            <div class="text_contenu">
                                <h1>Information Entreprise</h1>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <!-- FIN CONTENU DE PAGE-->
@endsection
