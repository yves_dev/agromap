@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >Edition du slide / Edit project</h3>
    </div>

    <div class="container-fluid">
        <h3 style="display: inline-block;"><a href="{{route('gestion_slider')}}" class="btn btn-dark">Retour au slide</a> </h3>

        <div class="col-xs-12" style="border: 2px solid black">
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
               <h4><u>Version Française</u></h4>
               <form method="post" action="{{route('modifier_slide',$le_slide[0]['id_versionning'])}}" enctype="multipart/form-data">

                   <div class="form-group">
                       <b>Titre du slide</b>
                       <input class="form-control" name="titre_fr" value="{{$le_slide[0]['titre']}}" required/>
                   </div>
                   <div class="form-group">
                       <b>Description</b>
                       <textarea name="description_fr" rows="7" class="form-control" required/>{{$le_slide[0]['description']}}</textarea>
                   </div>
           </div>
            {{--              Version Anglaise--}}
            {{--              Version Anglaise--}}
            <div class="col-sm-6 form-group">
                <h3><u>English Version</u></h3>

                <div class="form-group">
                    <b>Slide title</b>
                    <input class="form-control" name="titre_en" value="{{$le_slide[1]['titre']}}" required/>
                </div>
                <div class="form-group">
                    <b>Description </b>
                    <textarea name="description_en" rows="7" class="form-control" required/>{{$le_slide[1]['description']}}</textarea>
                </div>
            </div>

            {{--              Version Mixte--}}
            {{--              Version Mixte--}}
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <h3>Bouton slide</h3>
                                <b>Texte du bouton</b>
                                <br/>
                                <input type="text" name="texte_bouton_fr" class="form-control" placeholder="En savoir plus" value="{{$le_slide[0]['text_bouton']}}"  />
                                <br/>
                                <b>Url de destination</b>
                                <input type="text" name="url_destination_fr" class="form-control" placeholder="http://google.com" value="{{$le_slide[0]['url_destination_bouton']}}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <b>Image d'arriere plan / Background image</b>
                            <br/>
                            <img src="{{$le_slide[0]['url_image']}}" width="100px" height="100px" />
                            <input type="file" name="banniere" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-sm-6"
                        <div class="col-sm-6">
                            <div class="form-group">
                                <h3>Slide slide</h3>
                                <b>Slide text</b>
                                <br/>
                                <input type="text" name="texte_bouton_en" class="form-control" placeholder="En savoir plus" value="{{$le_slide[1]['text_bouton']}}" />
                                <br/>
                                <b>Destination url</b>
                                <input type="text" name="url_destination_en" class="form-control" placeholder="http://google.com"  value="{{$le_slide[1]['url_destination_bouton']}}"/>
                            </div>
                        </div>
                </div>
            </div>

            <div class="container text-center">
                <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$le_slide[0]['id']}}" >
                <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$le_slide[1]['id']}}" >

                <input type="hidden" name="_method" value="put" >
                <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                    <a href="{{route('gestion_slider')}}" class="btn btn-dark">Retour au slide</a>
                    <input type="submit" class="btn btn-warning" value="Enregistrer">
            </div>
            </form>
               </div>

       </div>
@endsection