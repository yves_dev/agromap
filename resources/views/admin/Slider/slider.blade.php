@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >PAGE D'ACCEUIL</h3>
    </div>

    <div class="container-fluid text-center">
        <a href="{{route('gestion_slider')}}" class="btn btn-actif">Slider</a> &nbsp;&nbsp;
        <a href="{{route('gestion_statistique')}}" class="btn btn-inactif">Statistiques</a> &nbsp;&nbsp;&nbsp;
    </div>

    <div class="container-fluid">
        <div class="col-xs-12" style="border: 2px solid black">
            <h1 class="text-center"> Slider </h1>
            {!! Session::get('notification','') !!}
        </div>

        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Ajouter / add</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Liste /List </a> </h3>
            {!! Session::get('notification','') !!}
        </div>


        <div class="container" id="div_ajout">
            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique"> Ajouter un slide / Add slide </span>
                </h3>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {{--              Version Française--}}
                    {{--              Version Française--}}
                    <h3><u>Version Française</u></h3>
                    <form method="post" action="{{route('ajouter_au_slider')}}" enctype="multipart/form-data">

                        <div class="form-group">
                            <b>Titre du slide</b>
                            <input class="form-control" name="titre_fr"required/>
                        </div>
                        <div class="form-group">
                            <b>Description</b>
                            <textarea name="description_fr" class="form-control" rows="7" required/></textarea>
                        </div>
                </div>
                {{--              Version Anglaise--}}
                {{--              Version Anglaise--}}
                <div class="col-sm-6 form-group">
                    <h3><u>English Version</u></h3>

                    <div class="form-group">
                        <b>Slide title</b>
                        <input class="form-control" name="titre_en"required/>
                    </div>
                    <div class="form-group">
                        <b>Description </b>
                        <textarea name="description_en" class="form-control" rows="7" required/></textarea>
                    </div>
                </div>

                {{--              Version Mixte--}}
                {{--              Version Mixte--}}
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <h3>Bouton slide</h3>
                                    <b>Texte du bouton</b>
                                    <br/>
                                    <input type="text" name="texte_bouton_fr" class="form-control"placeholder="En savoir plus"/>
                                    <br/>
                                    <b>Url de destination</b>
                                    <input type="text" name="url_destination_fr" class="form-control" placeholder="http://google.com"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <b>Bannière / Banner</b>
                                <br/>
                                <input type="file" name="banniere" class="form-control" required/>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <h3>Slide slide</h3>
                                <b>Slide text</b>
                                <br/>
                                <input type="text" name="texte_bouton_en" class="form-control" placeholder="En savoir plus" />
                                <br/>
                                <b>Destination url</b>
                                <input type="text" name="url_destination_en" class="form-control" placeholder="http://google.com"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 justify-content-center">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                    <h3 class="text-center">
                        <input type="submit" class="btn btn-warning" value="Enregistrer" >
                    </h3>
                </div>
                </form>
            </div>
        </div>

        {{--        LISTE DES slide--}}
        {{--        LISTE DES slide--}}
        {{--        LISTE DES slide--}}
        <div class="container" id="div_liste">

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique"> Liste des slides / slide list</span>
                </h3>
            </div>

            <table class="text-center table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th>Nom du slide</th>
                    <th>slide name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($le_slider as $item_slide)
                    {{--                        {{dump($item_slide)}}--}}
                    <tr>
                        <td>{{$item_slide[0]['titre']}}</td>
                        <td>{{$item_slide[1]['titre']}}</td>
                        <td>
                            <a href="{{route('editer_slide',$item_slide[0]['id_versionning'])}}" class="btn btn-warning">Modifier</a>
                            <button type="button" data-toggle="modal" data-target="#ModalSuppression{{$item_slide[0]['id_versionning']}}" class="btn btn-danger">Supprimer</button>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            <div class="modal fade" id="ModalSuppression{{$item_slide[0]['id_versionning']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalh4">Supprimer le slide / Delete slide</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Voulez cous vraiment supprimer ce slide ? <br/>
                                            Are you sure ?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                            <form method="post" action="{{route('effacer_un_slide',[$item_slide[0]['id_versionning']])}}">
                                                <input type="hidden" name="_method" value="delete" />
                                                {{csrf_field()}}
                                                <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection