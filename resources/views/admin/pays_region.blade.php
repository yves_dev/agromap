@extends('partials.base_admin')

@section('contenu')


    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >Zone d'action [pays et region]</h3>
        {!! Session::get('notification','')!!}
    </div>

    <div class="container-fluid">
        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Pays</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Region</a> </h3>
        </div>

        <br/><br/>

{{--        ********************GESTION PAYS********************--}}
{{--        ********************GESTION PAYS********************--}}
        <div  id="div_ajout">
            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique"> Pays</span>
                </h3>
            </div>
             <div class="row">
            <div class="col-sm-5">
                <h3>Ajouter un pays</h3>
                <form class="col-xs-12" action="{{route('ajouter_pays')}}" method="post">
                    <div class="form-group">
                        <label>Nom du pays</label>
                        <input class="form-control" name="nom_pays" placeholder="Côte d'ivoire" required/>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" class="btn btn-primary" value="{{csrf_token()}}" >
                        <input type="submit" class="btn btn-primary" value="Enregistrer" >
                    </div>
                </form>
            </div>

            <div class="col-sm-7">
                <h3>Liste actuelle des pays</h3>
                <table class="text-center table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Nom du pays</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                            @foreach($les_pays as $item_pays)
                                <tr>
                                <td>{{$item_pays['nom_pays']}}</td>
                                <td>
                                    <button type="button" data-toggle="modal"
                                            data-target="#ModalModification{{$item_pays['id']}}"
                                            class="btn btn-warning">Modifier</button>
                                    <button type="button" data-toggle="modal"
                                            data-target="#ModalSuppression{{$item_pays['id']}}"
                                            class="btn btn-danger">Supprimer</button>
                                </td>

                                    {{--                                -----------------------MODAL DE MODIFIACTION-------------------------}}
                                    <div class="modal fade" id="ModalModification{{$item_pays['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalh4">Supprimer le filiere / Delete filiere</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="col-xs-12" action="{{route('modifier_pays',[$item_pays['id']])}}" method="post">
                                                        <div class="form-group">
                                                            <label>Nom du pays</label>
                                                            <input class="form-control" name="nom_pays" placeholder="Côte d'ivoire" value="{{$item_pays['nom_pays']}}" required/>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="hidden" name="_token" class="btn btn-primary" value="{{csrf_token()}}" >
                                                            <input type="submit" class="btn btn-warning" value="Enregistrer" >
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--                                -----------------------FIN MODAL DE MODIFICATION-------------------------}}
                                    {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                                    <div class="modal fade" id="ModalSuppression{{$item_pays['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalh4">Supprimer le filiere / Delete filiere</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Voulez cous vraiment supprimer ce filiere ? <br/>
                                                    Are you sure ?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                    <form method="post" action="{{route('effacer_pays',[$item_pays['id']])}}">
                                                        <input type="hidden" name="_method" value="delete" />
                                                        {{csrf_field()}}
                                                        <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}

                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
        {{--        ********************GESTION REGION********************--}}
        {{--        ********************GESTION REGION********************--}}
        <div id="div_liste">
            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique"> Regions</span>
                </h3>
            </div>
            <div class="row">
            <div class="col-sm-5">
                <h3 class="text-center"><u>Ajouter une zone d'action</u></h3>
                <form class="col-xs-12" method="post" action="{{route('ajouter_region')}}">
                    <div class="form-group">
                        <label>Nom de la region</label>
                        <input name="nom_region" class="form-control" placeholder="Abidjan" required/>
                    </div>
                    <div class="form-group">
                        <label>Le pays</label>
                            <select name="pays_id" class="form-control" required>
                                <option value >Choississez le pays</option>
                                @foreach($les_pays as $item_pays)
                                    <option value="{{$item_pays['id']}}">{{$item_pays['nom_pays']}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                        <input type="submit" class="btn btn-primary" value="Enregistrer" >
                    </div>
                </form>
            </div>

            <div class="col-sm-7">
                <h3 class="text-center"><u>Liste actuelle des regions</u></h3>
                <table class="text-center table-bordered" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Zone d'action</th>
                        <th>Pays</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($les_regions as $item_region)
                            <tr>
                                <td>{{$item_region['nom_region']}}</td>
                                <td>{{$item_region->pays->nom_pays}}</td>
                                <td>
                                    <button type="button" data-toggle="modal"
                                            data-target="#ModalModificationRegion{{$item_region['id']}}"
                                            class="btn btn-warning">Modifier</button>
                                    <button type="button" data-toggle="modal"
                                            data-target="#ModalSuppressionRegion{{$item_region['id']}}"
                                            class="btn btn-danger">Supprimer</button>
                                </td>

                                {{--                                -----------------------MODAL DE MODIFIACTION-------------------------}}
                                <div class="modal fade" id="ModalModificationRegion{{$item_region['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalh4">Supprimer le filiere / Delete filiere</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="col-xs-12" method="post" action="{{route('modifier_region',[$item_region['id']])}}">
                                                    <div class="form-group">
                                                        <label>Nom de la region</label>
                                                        <input name="nom_region" class="form-control" placeholder="Abidjan" value="{{$item_region->nom_region}}" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Le pays</label>
                                                        <select name="pays_id" class="form-control" required>
                                                            <option value="{{$item_region->pays->id}}" >{{$item_region->pays->nom_pays}}</option>
                                                            @foreach($les_pays as $item_pays)
                                                                <option value="{{$item_pays['id']}}">{{$item_pays['nom_pays']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                                                        <input type="submit" class="btn btn-primary" value="Enregistrer" >
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--                                -----------------------FIN MODAL DE MODIFICATION-------------------------}}
                                {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                                <div class="modal fade" id="ModalSuppressionRegion{{$item_region['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalh4">Supprimer le filiere / Delete filiere</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Voulez cous vraiment supprimer ce filiere ? <br/>
                                                Are you sure ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                <form method="post" action="{{route('effacer_region',[$item_region['id']])}}">
                                                    <input type="hidden" name="_method" value="delete" />
                                                    {{csrf_field()}}
                                                    <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
@endsection