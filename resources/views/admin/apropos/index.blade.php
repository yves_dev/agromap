@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >ENTREPRISE</h3>
    </div>

    <div class="container text-center">
        <a href="{{route('gestion_parametre')}}" class="btn btn-inactif">Information general</a> &nbsp;&nbsp;
        <a href="{{route('gestion_apropos')}}" class="btn btn-actif">A propos</a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_equipe')}}" class="btn btn-inactif"> Equipe </a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_partenaire')}}" class="btn btn-inactif"> Partenaires </a> &nbsp;&nbsp;&nbsp;
    </div>

    <div class="container-fluid">

        <div class="col-xs-12" style="border: 2px solid black">
            <h1 class="text-center"> A propos </h1>
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
               <br/>
               {{--              Version Française--}}
               {{--              Version Française--}}
               <h3><u>Version Française</u></h3>
                   <form method="post" action="{{route('modifier_apropos')}}">

                       <div class="form-group">
                           <b>Nom de l'entreprise</b>
                           <input name="nom_entreprise_fr" placeholder="Nom de l'entreprise" value="{{$apropos[0]['nom_entreprise']}}" class="form-control"required/>
                       </div>
                       <div class="form-group">
                           <br/>
                           <b>Qui sommes nous ?</b>
                                <textarea rows="7" placeholder="Descrivez qui vous êtes et ce que vous faites" class="form-control" name="nous_fr" required>{{$apropos[0]['texte_nous']}}</textarea>
                       </div>
                       <div class="form-group">
                           <b> Notre vision </b>
                           <textarea rows="7" class="form-control" name="vision_fr" placeholder="Decrivez la vision de votre entreprise..." required>{{$apropos[0]['texte_vision']}}</textarea>
                       </div>
                       </div>
                       {{--              Version Anglaise--}}
                       {{--              Version Anglaise--}}
                       <div class="col-sm-6 form-group">
                           <br/>
                           <h3><u>English Version</u></h3>

                           <div class="form-group">
                               <b>Enterprise Name</b>
                               <input name="nom_entreprise_en" value="{{$apropos[1]['nom_entreprise']}}" placeholder="Your Incorporation name..." class="form-control"required/>
                           </div>
                           <div class="form-group">
                               <br/>
                               <b>Who we are ?</b>
                               <textarea rows="7" class="form-control" name="nous_en" placeholder="Describe what you do in your incorporation..." required>{{$apropos[1]['texte_nous']}}</textarea>
                           </div>
                           <div class="form-group">
                               <b> Our vision </b>
                               <textarea rows="7" class="form-control" name="vision_en" placeholder="Describe your incorporation vision..." required>{{$apropos[1]['texte_vision']}}</textarea>
                           </div>
                       </div>

               <div class="container-fluid text-center">
                       {{csrf_field()}}
                       <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$apropos[0]['id']}}" >
                       <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$apropos[1]['id']}}" >
                       <input type="submit" class="btn btn-warning" value="Enregistrer les informations" >
               </div>
           </form>
       </div>

   </div>
@endsection

@section('script_perso')
    <script>
        new Quill('.rich_text_editor1', {theme: 'snow'});
        new Quill('.rich_text_editor2', {theme: 'snow'});
        new Quill('.rich_text_editor3', {theme: 'snow'});
        new Quill('.rich_text_editor4', {theme: 'snow'});
        new Quill('.rich_text_editor5', {theme: 'snow'});
        new Quill('.rich_text_editor6', {theme: 'snow'});
    </script>
@endsection