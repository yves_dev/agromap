@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h1 class="col-xs-12 text-center titre_section_general" >Filieres</h1>
        {!! Session::get('notification','') !!}
    </div>


    <div class="container-fluid">

        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Ajouter / add</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Liste /List </a> </h3>
        </div>


        <div class="row" id="div_ajout">

            <div class="container div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Ajouter un filiere / Add Sector</span>
                </h3>
            </div>


            <div class="row">
               <div class="col-sm-6">
    {{--              Version Française--}}
    {{--              Version Française--}}
                   <b><u>Version Française</u></b>
                       <form method="post" action="{{route('ajouter_filiere')}}" enctype="multipart/form-data">

                               <div class="form-group">
                                   <b>Denomination du filiere</b>
                                        <input class="form-control" name="nom_fr"required/>
                               </div>
                               <div class="form-group">
                                   <b>Description filiere</b>
                                   <textarea name="description_fr" class="form-control" rows="6" required/></textarea>
                               </div>
                           </div>
               {{--              Version Anglaise--}}
               {{--              Version Anglaise--}}
               <div class="col-sm-6 form-group">
                               <h3><u>English Version</u></h3>

                                   <div class="form-group">
                                       <b>Sector denomination</b>
                                          <input class="form-control" name="nom_en" required/>
                                   </div>
                                   <div class="form-group">
                                       <b>Sector description </b>
                                       <textarea name="description_en" class="form-control" rows="6" required/></textarea>
                                   </div>
                           </div>

                {{--              Version Mixte--}}
                {{--              Version Mixte--}}
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <b>Banniere de la filiere / Sector banner</b>
                                <br/>
                                <input type="file" name="banniere" class="form-control"required/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <b>icone de la filière / Sector icon</b>
                                <br/>
                                <input type="file" name="icone" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>

                   <div class="container-fluid text-center">
                        {{csrf_field()}}
                       <input type="submit" class="btn btn-warning" value="Enregistrer les informations" >
                   </div>
           </form>
        </div>
       </div>

{{--        LISTE DES filiere--}}
{{--        LISTE DES filiere--}}
{{--        LISTE DES filiere--}}
        <div class="row" id="div_liste">

            <div class="container div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Liste des filieres / Filiere list</span>
                </h3>
            </div>


            <table class="text-center table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th>Nom du filiere</th>
                    <th>filiere name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($les_filieres as $item_filiere)
{{--                        {{dump($item_filiere[0])}}--}}
                        <tr>
                            <td>{{$item_filiere[0]['nom']}}</td>
                            <td>{{$item_filiere[1]['nom']}}</td>
                            <td>
                                <a href="{{route('editer_filiere',$item_filiere[1]['id_versionning'])}}" class="btn btn-warning">Modifier</a>
                                <button type="button" data-toggle="modal" data-target="#ModalSuppression{{$item_filiere[0]['id_versionning']}}" class="btn btn-danger">Supprimer</button>
{{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                                <div class="modal fade" id="ModalSuppression{{$item_filiere[0]['id_versionning']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalh4">Supprimer le filiere / Delete filiere</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Voulez cous vraiment supprimer ce filiere ? <br/>
                                                Are you sure ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                <form method="post" action="{{route('effacer_filiere',[$item_filiere[0]['id_versionning']])}}">
                                                    <input type="hidden" name="_method" value="delete" />
                                                    {{csrf_field()}}
                                                    <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
   </div>
@endsection