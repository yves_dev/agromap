@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >Edition de la Filières / Edit sector</h3>
    </div>

    <div class="container-fluid">
        <h3 style="display: inline-block;"><a href="{{route('gestion_filiere')}}" class="btn btn-dark">Retour aux filieres</a> </h3>

        <div class="col-xs-12" style="border: 2px solid black">
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
               <h4><u>Version Française</u></h4>
               <form method="post" action="{{route('modifier_filiere',$la_filiere[0]['id_versionning'])}}" enctype="multipart/form-data">

                   <div class="form-group">
                       <b>Denomination du filiere</b>
                       <input class="form-control" name="nom_fr" value="{{$la_filiere[0]['nom']}}" required/>
                   </div>
                   <div class="form-group">
                       <b>Description filiere</b>
                       <textarea name="description_fr" class="form-control" rows="5" required/>{{$la_filiere[0]['description']}}</textarea>
                   </div>
           </div>
            {{--              Version Anglaise--}}
            {{--              Version Anglaise--}}
            <div class="col-sm-6 form-group">
                <h3><u>English Version</u></h3>

                <div class="form-group">
                    <b>Sector denomination</b>
                    <input class="form-control" name="nom_en" value="{{$la_filiere[1]['nom']}}" required/>
                </div>
                <div class="form-group">
                    <b>Sector description </b>
                    <textarea name="description_en" class="form-control" rows="5" required/>{{$la_filiere[1]['description']}}</textarea>
                </div>
            </div>

            {{--              Version Mixte--}}
            {{--              Version Mixte--}}
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <b>Banniere de la filiere / Sector banner</b>
                            <br/>
                            <img src="{{$la_filiere[0]['banniere']}}" width="100px" height="100px" />
                            <input type="file" name="banniere" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <b>icone de la filière / Sector icon</b>
                            <br/>
                            <img src="{{$la_filiere[0]['icone']}}" width="100px" height="100px" />
                            <input type="file" name="icone" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container text-center">
                    {{csrf_field()}}
                    <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$la_filiere[0]['id']}}" >
                    <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$la_filiere[1]['id']}}" >

                    <a href="{{route('gestion_filiere')}}" class="btn btn-dark">Retour aux filieres</a>
                    <input type="submit" class="btn btn-warning" value="Enregistrer" >
            </div>
            </form>
               </div>

       </div>
@endsection