@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >ENTREPRISE</h3>
    </div>

    <div class="container text-center">
        <a href="{{route('gestion_parametre')}}" class="btn btn-inactif">Information general</a> &nbsp;&nbsp;
        <a href="{{route('gestion_apropos')}}" class="btn btn-inactif">A propos</a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_equipe')}}" class="btn btn-inactif"> Equipe </a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_partenaire')}}" class="btn btn-actif"> Partenaires </a> &nbsp;&nbsp;&nbsp;
    </div>


    <div class="container-fluid">


        <div class="col-xs-12" style="border: 2px solid black">
            <h1 class="text-center"> Partenaires </h1>
            {!! Session::get('notification','') !!}
        </div>


        {{--        ********************GESTION partenaire********************--}}
        {{--        ********************GESTION partenaire********************--}}
        <div class="row" style="margin-top: 20px">
            <div class="col-sm-5">
                <h3 class="text-center"><u>Ajouter un partenaire</u></h3>
                <form class="col-xs-12" method="post" action="{{route('ajouter_partenaire')}}" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Nom du partenaire / Partenaire name</label>
                        <input name="nom_partenaire" class="form-control" placeholder="Nom du partenaire / partner name" required/>
                    </div>
                    <div class="form-group">
                        <label>Logo / Brand</label>
                        <input type="file" name="logo" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                        <input type="submit" class="btn btn-primary" value="Enregistrer" >
                    </div>
                </form>
            </div>

            <div class="col-sm-7" style="border-left: 2px solid rgba(12,16,22,0.4)">
                <h3 class="text-center"><u> Liste actuelle des partenaire </u></h3>
                <table class="text-center table-bordered" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Partenaire / Partner </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($les_partenaires as $item_partenaire)
                            <tr>
                                <td>{{$item_partenaire['nom_partenaire']}}</td>

                                <td>
                                    <button type="button" data-toggle="modal"
                                            data-target="#ModalModification{{$item_partenaire['id']}}"
                                            class="btn btn-warning">Modifier</button>
                                    <button type="button" data-toggle="modal"
                                            data-target="#ModalSuppression{{$item_partenaire['id']}}"
                                            class="btn btn-danger">Supprimer</button>
                                </td>

                                {{--                                -----------------------MODAL DE MODIFIACTION-------------------------}}
                                <div class="modal fade" id="ModalModification{{$item_partenaire['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalh4">Supprimer le filiere / Delete filiere</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="col-xs-12" method="post" action="{{route('modifier_partenaire',[$item_partenaire['id']])}}" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label>Nom du partenaire / Partner name</label>
                                                        <input name="nom_partenaire" class="form-control" placeholder="Abidjan" value="{{$item_partenaire['nom_partenaire']}}" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fichier</label>
                                                            <img src="{{$item_partenaire['logo']}}" width="100px" height="100px" />
                                                            <input type="file" name="logo" class="form-control"/>

                                                    </div>
                                                    <div class="form-group">
                                                        <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                                                        <input type="submit" class="btn btn-primary" value="Enregistrer" >
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--                                -----------------------FIN MODAL DE MODIFICATION-------------------------}}
                                {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                                <div class="modal fade" id="ModalSuppression{{$item_partenaire['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalh4">Supprimer le filiere / Delete filiere</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Voulez cous vraiment supprimer ce filiere ? <br/>
                                                Are you sure ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                <form method="post" action="{{route('effacer_partenaire',[$item_partenaire['id']])}}">
                                                    <input type="hidden" name="_method" value="delete" />
                                                    {{csrf_field()}}
                                                    <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection