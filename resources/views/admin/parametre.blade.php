@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >ENTREPRISE</h3>
    </div>

    <div class="container text-center">
        <a href="{{route('gestion_parametre')}}" class="btn btn-actif">Information general</a> &nbsp;&nbsp;
        <a href="{{route('gestion_apropos')}}" class="btn btn-inactif">A propos</a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_equipe')}}" class="btn btn-inactif"> Equipe </a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_partenaire')}}" class="btn btn-inactif"> Partenaires </a> &nbsp;&nbsp;&nbsp;
    </div>

    <div class="container-fluid">

        <div class="col-xs-12" style="border: 2px solid black">
            <h1 class="text-center"> Information general </h1>
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
                   <form method="post" action="{{route('modifier_parametre')}}" enctype="multipart/form-data">

                           <div class="form-group">
                               <br/><br/>
                               <label>Nom entreprise </label>
                                    <input type="text" name="nom_entreprise" class="form-control" value="{{$les_parametres['nom_entreprise'] }}" />
                           </div>
                           <div class="form-group">
                               <?php
                               $desc = explode("###",$les_parametres['description']);
                               $description_fr = $description_en ="";
                               if (isset($desc[0])){
                                   $description_fr  = $desc[0];

                               }
                               if (isset($desc[1])){
                                  $description_en  = $desc[1];
                               }

                               ?>
                               <br/><br/>
                               <label>Description version fr</label>
                                    <textarea type="text" rows="5" name="description_entreprise_fr" placeholder="Decrivez votre activité"
                                           class="form-control"  >{{$description_fr }}</textarea>
                           </div>
                           <div class="form-group">
                               <br/><br/>
                               <label>Description version en </label>
                                    <textarea type="text" rows="5" name="description_entreprise_en" placeholder="Decrivez votre activité"
                                           class="form-control"  > {{$description_en }} </textarea>
                           </div>

                           <div class="form-group">
                               <label>
                                   Logo
                                   @if($les_parametres['url_logo_entreprise'] != null)
                                       <img src="{{$les_parametres['url_logo_entreprise'] }}">
                                   @endif
                               </label>
                                    <input type="file" name="logo_entreprise" class="form-control" />
                           </div>

                            <div class="form-group">
                               <label>Fav icone
                                    @isset( $les_parametres['url_fav_icone'])
                                        <img src="{{$les_parametres['url_fav_icone']}}" width="50px" height="50px" >
                                    @endisset
                               </label>
                                <input type="file" name="fav_icone_entreprise" class="form-control" />

                           </div>

                           <div class="form-group">
                               <label>Telephones entreprise </label>
                               <input type="text" name="telephones_entreprise" placeholder="+225 76 67 67 87 / +239 76 56 45 78" class="form-control" value="{{$les_parametres['telephones']}}" />
                           </div>

                           <div class="form-group">
                               <label>Email entreprise </label>
                               <input type="email" name="email_entreprise" class="form-control" value="{{$les_parametres['email']}}" />
                           </div>

           </div>
                       <div class="col-sm-6 form-group">

                           <div class="form-group">
                               <br/><br/>
                               <label>Adresse entreprise </label>
                               <input type="text" name="adresse_entreprise" placeholder="Cote d'ivoire, Abidjan cocody" class="form-control" value="{{$les_parametres['adresse']}}" />
                           </div>

                           <div class="form-group">
                               <label>Horraires</label>
                               <input type="text" name="horraires_entreprise" placeholder="07H30 - 18H25" class="form-control" value="{{$les_parametres['horraires']}}" />
                           </div>

                               <div class="form-group">
                                   <label>Lien page facebook</label>
                                   <input type="url" name="lien_facebook" class="form-control"
                                          placeholder=""
                                          value="{{$les_parametres['lien_facebook']}}"
                                   />
                               </div>

                               <div class="form-group">
                                   <label>Lien chaine youtube</label>
                                   <input type="url"  name="lien_youtube" class="form-control"
                                          placeholder=""
                                          value="{{$les_parametres['lien_youtube']}}"
                                   />
                               </div>

                               <div class="form-group">
                                   <label>Lien page instagram {{--[ stocker dans twitter]--}}</label>
                                   <input type="url"  name="lien_twitter" class="form-control"
                                          placeholder=""
                                          value="{{$les_parametres['lien_twitter']}}"
                                   />
                               </div>

                               <div class="form-group">
                                   <label>Lien page linkedin</label>
                                   <input type="url"  name="lien_linkedin"
                                          class="form-control" placeholder="-aller sur youtube cliquer sur partager , -choisissez l'option <<integer>>
                                          , cliquez sur copier et coller ici"
                                          value="{{$les_parametres['lien_linkedin']}}" />
                               </div>

                       </div>

                   <div class="container-fluid text-center">
                       {{csrf_field()}}
                           <input type="submit" class="btn btn-warning" value="Enregistrer les modifications" >
                   </div>
                   </form>
               </div>

           </div>
@endsection