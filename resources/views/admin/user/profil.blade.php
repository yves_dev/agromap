@extends('partials.base_admin')

@section('contenu')

    {!! Session::get('notification','') !!}
    {{--                                -----------------------MODAL DE MODIFIACTION-------------------------}}
    <div id="Modifier_mot_de_passe" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalh4">Modifier le profil</h5>
                </div>
                <div class="modal-body">
                    <form class="col-xs-12" action="{{route('modifier_profil')}}" method="post">
                        <div class="form-group">
                            <label>Nom</label>
                            <input class="form-control" name="nom" placeholder="Votre nom" value="{{\Illuminate\Support\Facades\Auth::user()->name}}" required/>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" name="email" placeholder="email" value="{{\Illuminate\Support\Facades\Auth::user()->email}}" required/>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <label>Nouveau Mot de passe (laissez vide si vous ne voulez pas changé) </label>
                            <input class="form-control" type="password" name="nouveau_mdp" placeholder="Nouveau mot de passe"/>
                            <br/>
                        </div>
                        <div class="container-fluid text-center">
                            <input type="hidden" name="id_utilisateur"value="{{\Illuminate\Support\Facades\Auth::user()->id}}" required/>
                            <input type="hidden" name="_token" class="btn btn-primary" value="{{csrf_token()}}" >
                            <input type="submit" class="btn btn-warning" value="Enregistrer" >
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{--                                -----------------------FIN MODAL DE MODIFICATION-------------------------}}
@endsection
@section('script_perso')
    <script>
        $(document).ready(function() {
            $("body").css("background-color", "#1f2839");
        });
    </script>
@endsection