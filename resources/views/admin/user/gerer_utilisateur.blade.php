@extends('partials.base_admin')


@section('contenu')
    <div class="container">
        {!! Session::get('notification','') !!}

        <div class="row" id="div_liste" >
            <h3 class="container text-center"
                style="padding: 8px;border: 2px solid black">
                Liste des utilisateurs
            </h3>


            <div class="container">


                <table class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                    <th>Nom</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Creer / Modifier par</th>
                    </thead>
                @foreach($liste_utilisateur as $item_utilisateur)
                    <tbody>
                        <tr>
                        <td>{{$item_utilisateur['name']}}</td>
                        <td>{{$item_utilisateur['email']}}</td>
                        <td>
                        <form method="post" action="{{route('modifier_type_utilisateur',$item_utilisateur['id'])}}">
                            <select name="type">
                                <option value="{{$item_utilisateur['type']}}">{{$item_utilisateur['type']}}</option>
                                <option value="editeur">Editeur</option>
                                <option value="Admin">Administrateur</option>
                            </select>
                            </td>
                            <td>
                                @if($item_utilisateur['creer_par'] !=null)
                                    {{\App\User::where(['id' => $item_utilisateur['creer_par']])->first()->name}}
                                @endif
                            <td>
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="put" class="btn btn-warning">
                                <input type="submit" value="Modifier" class="btn btn-warning">
                        </form>
                            <button type="button" data-toggle="modal"
                                    data-target="#ModalSuppression{{$item_utilisateur['id']}}"
                                    class="btn btn-danger">Supprimer</button>


                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            <div class="modal fade" id="ModalSuppression{{$item_utilisateur['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalh4">Supprimer / Delete </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Voulez vous vraiment supprimer cet utilisateur ? <br/>
                                            Are you sure ?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                            <form method="post" action="{{route('effacer_utilisateur',[$item_utilisateur['id']])}}">
                                                <input type="hidden" name="_method" value="delete" />
                                                {{csrf_field()}}
                                                <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
            </div>

        </div>

    </div>
@endsection

@section('script_perso')

{{--    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>--}}
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script>
        function activer_quill() {
                new Quill('#rich_message',
                    {placeholder: 'Composez votre article...',
                        theme: 'snow'
                    }
                );
        }

        function recuperer_les_donnees(){
            for(i=1;i<=3;i++){
                var myEditor = document.getElementById('rich_message');

                var text_area_message = document.getElementById('text_area_message');

                var html = myEditor.children[0].innerHTML;

                text_area_message.innerText = html;
            }

        }
        //****************************************** //
        window.onload = function(){
            activer_quill();
        };
        document.onkeyup = function (event) {
            recuperer_les_donnees();
        };
        document.onmousemove = function (event) {
            recuperer_les_donnees();
        };
        document.onclick = function (event) {
            recuperer_les_donnees();
        };


        $(document).ready(function() {
            $('.dataTable').DataTable();
        } );
    </script>
@endsection