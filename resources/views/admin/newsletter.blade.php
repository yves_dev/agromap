@extends('partials.base_admin')

@section('style_perso')
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />
@endsection

@section('contenu')
    <div class="container-fluid table">
        <h1 class="col-xs-12 text-center titre_section_general" >Newsletter</h1>
        {!! Session::get('notification','') !!}
    </div>

    <div class="container">

        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_liste()" class="btn btn-primary" id="btn_liste">Envoyer un message</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_ajout()" class="btn btn-warning" id="btn_ajouter">Liste des abonnés </a> </h3>
        </div>

        <div class="row" id="div_liste">

            <h3 class="container text-center"
                style="padding: 8px;border: 2px solid black">
                Envoyer un email aux abonné-es
            </h3>

            <div class="container text-center" >
                <div class="row">
                    <div class="col-md-8">
                        <form  id="contact_form" name="contact_form" autocomplete="off" action="{{route('envoyer_mail')}}" method="post" >

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form_name">Nom expediteur <small>*</small></label>
                                        <input name="nom_entreprise" class="form-control" type="text" placeholder="Nom de l'organisation" value="{{$parametre['nom_entreprise']}}" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email <small>*</small></label>
                                        <input name="email_reponse" class="form-control required email" type="email" placeholder="Votre Email / Enter Email" value="{{$parametre['email']}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form_name">Sujet / Subject <small>*</small></label>
                                        <input name="sujet" class="form-control required" type="text" placeholder="Sujet / Subject" required>
                                    </div>
                                </div>
                                {{--<div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="form_phone">Telephone / Phone *</label>
                                        <input name="contact" class="form-control" type="text" placeholder="Telephone / Enter Phone" value="{{$parametre['telephones']}}" required>
                                    </div>
                                </div>--}}
                            </div>

                            <div class="form-group">
                                <label for="form_name">Message *</label>

                                <div id="rich_message"></div>
                                <textarea name="message" id="text_area_message" class="form-control required" style="display: none" rows="5" placeholder="Votre message / Enter Message" required></textarea>
                            </div>
                            <div class="form-group">
                                {{csrf_field()}}
                                <button type="submit" data-loading-text="Please wait...">Envoyer votre message / Send your message</button>
                                <button type="reset" class="col-xs-offset-1 btn btn-dark btn-theme-colored">Vider / Reset</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 d-none d-sm-block"
                         style="width: 100%;min-height:350px;background: url('/uploaded_image/newsletter/Newsletter.jpg');background-size: cover">
                    </div>
                </div>
            </div>

        </div>

        <div class="row" id="div_ajout" >

            {{--                                -----------------------MODAL D'AJOUT-------------------------}}
            <div class="modal fade" id="ModalAjout" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalh4">Ajouter un email</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{route('ajouter_email')}}">
                                <input type="email" name="email" placeholder="Entrer l'email" required />
                                {{csrf_field()}}
                                <input type="submit" class="btn btn-primary" value="Enregistrer"/>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>

                        </div>
                    </div>
                </div>
            </div>
            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
            <h3 class="container text-center"
                style="padding: 8px;border: 2px solid black">
                Liste des abonné-es à la newsletter
            </h3>


            <div class="container" >

                <button type="button" data-toggle="modal"
                        data-target="#ModalAjout"
                        class="btn btn-inactif" style="margin: 10px">Ajouter une adresse email</button>

                <table class="table table-bordered table-striped table-hover dataTable">
                @foreach($liste_email as $item_email)
                    <tr>
                        <td>{{$item_email['email']}}</td>
                        <td>

                            <button type="button" data-toggle="modal"
                                    data-target="#ModalSuppression{{$item_email['id']}}"
                                    class="btn btn-danger">Supprimer</button>


                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            <div class="modal fade" id="ModalSuppression{{$item_email['id']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalh4">Supprimer / Delete </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Voulez cous vraiment supprimer cet email ? <br/>
                                            Are you sure ?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                            <form method="post" action="{{route('effacer_email',[$item_email['id']])}}">
                                                <input type="hidden" name="_method" value="delete" />
                                                {{csrf_field()}}
                                                <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                        </td>
                    </tr>
                @endforeach
            </table>
            </div>

        </div>

    </div>
@endsection

@section('script_perso')

{{--    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>--}}
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script>
        function activer_quill() {
                new Quill('#rich_message',
                    {placeholder: 'Composez votre article...',
                        theme: 'snow'
                    }
                );
        }

        function recuperer_les_donnees(){
            for(i=1;i<=3;i++){
                var myEditor = document.getElementById('rich_message');

                var text_area_message = document.getElementById('text_area_message');

                var html = myEditor.children[0].innerHTML;

                text_area_message.innerText = html;
            }

        }
        //****************************************** //
        window.onload = function(){
            activer_quill();
        };
        document.onkeyup = function (event) {
            recuperer_les_donnees();
        };
        document.onmousemove = function (event) {
            recuperer_les_donnees();
        };
        document.onclick = function (event) {
            recuperer_les_donnees();
        };


        $(document).ready(function() {
            $('.dataTable').DataTable();
        } );
    </script>
@endsection