@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h1 class="col-xs-12 text-center titre_section_general" >Projets</h1>
        {!! Session::get('notification','') !!}
    </div>


    <div class="container text-center" style="margin: 10px auto">
        <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Ajouter / add</a> </h3>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Liste /List </a> </h3>
    </div>

    <div class="container-fluid" id="div_ajout">

        <div class="col-xs-12 div_titre_section_specifique" >
            <h3 class="text-center">
                <span class="titre_section_specifique">Ajouter un projet / Add project</span>
            </h3>
        </div>

                   <form method="post" action="{{route('ajouter_projet')}}" enctype="multipart/form-data">

                       <div class="row">
                            <div class="col-sm-6">
                           {{--              Version Française--}}
                           {{--              Version Française--}}
                           <h4><u>Version Française</u></h4>
                           <div class="form-group">
                               <label>Nom du projet *</label>
                                    <input name="nom_fr" class="form-control" required placeholder="PROJET D’AGROFORESTERIE...." />
                           </div>
                           <div class="form-group">
                               <label>Etat du projet *</label>
                               <select name="etat_fr" class="form-control" >
                                   <option value="ouvert" >Ouvert</option>
                                   <option value="fermer" >Fermer</option>
                               </select>
                           </div>

                           <div class="form-group">
                               <label>Resumé du projet *</label>
                               <textarea name="resume_fr" class="form-control" required
                                         placeholder="Entrer le résumé du projet..." rows="8" ></textarea>
                           </div>
                            <div class="form-group">
                                <label>Methode de travail / Approche de realisation</label>
                                <textarea name="methode_fr" class="form-control" rows="8"></textarea>
                            </div>
                           <div class="form-group">
                               <label>Objectif du projet *</label>
                               <textarea name="objectif_fr" class="form-control" required rows="8"
                                         placeholder="Entrer les objectif du projet..." ></textarea>
                           </div>
                           <div class="form-group">
                               <label>Résultats attendus *</label>
                               <textarea name="resultat_fr" class="form-control" required rows="8"
                                         placeholder="Entrer les résultats attendus..." ></textarea>
                           </div>
                           <div class="form-group">
                               <label>Partenaires du projet</label>
                               <textarea name="partenaire_fr" class="form-control" rows="8"
                                         placeholder="Entrer le partenaires qui projet et une description de leur apports." ></textarea>
                           </div>


                           <div class="form-group">
                               <label>Région du pays / Region of country *</label>

                               <datalist id="liste_region">
                                   @foreach($les_regions as $item_region)
                                       <option value="{{$item_region['nom_region']}}">{{$item_region->pays->nom_pays}} - {{$item_region['nom_region']}}</option>
                                   @endforeach
                               </datalist>
                               <input list="liste_region" placeholder="Choissisez la region" required name="nom_region" required class="form-control">

                               <br/>
                               <input type="checkbox" style="padding:25px" id="nouvelle_region" name="est_une_nouvelle_region" onclick="showPays()">
                               <label>C'est une nouvelle region</label>
                               <br/>
                               <select id="choisir_pays" name="pays_id" class="form-control" style="display: none" autocomplete="no">
                                   <option value="-1" >Choississez le pays</option>
                                   @foreach($les_pays as $item_pays)
                                       <option value="{{$item_pays['id']}}">{{$item_pays->nom_pays}}</option>
                                   @endforeach
                               </select>
                           </div>


                           <div class="form-group">
                               <label>Lien video youtube</label>
                               <textarea name="lien_video" class="form-control"
                                         placeholder="-aller sur youtube cliquer sur partager , -choisissez l'option <<integer>> , cliquez sur copier et coller ici" ></textarea>
                           </div>
                     </div>
                       {{--              Version Anglaise--}}
                       {{--              Version Anglaise--}}
                          <div class="col-sm-6 form-group">
                           <h3><u>English Version</u></h3>
                           <div class="form-group">
                               <label>Project name *</label>
                                    <input name="nom_en" class="form-control" placeholder="Enter Project Name...."  required/>
                           </div>
                           <div class="form-group">
                               <label>Project state *</label>
                               <select name="etat_en" class="form-control" >
                                   <option value="open" >Open</option>
                                   <option value="close" >Close</option>
                               </select>
                           </div>
                           <div class="form-group">
                               <label>Resume project *</label>
                               <textarea name="resume_en" class="form-control" rows="8"
                                         placeholder="Enter project summary..." rows="8" required></textarea>
                           </div>
                          <div class="form-group">
                              <label>Work Method / Realisation Approach</label>
                              <textarea name="methode_en" class="form-control" rows="8"></textarea>
                          </div>
                           <div class="form-group">
                               <label>Project goals *</label>
                               <textarea name="objectif_en" class="form-control" required rows="8"
                                         placeholder="Enter Project goals..." ></textarea>
                           </div>
                           <div class="form-group">
                               <label>Expected results *</label>
                               <textarea name="resultats_en" class="form-control" required rows="8"
                                         placeholder="Enter expected result..." ></textarea>
                           </div>
                           <div class="form-group">
                               <label>Project Partners</label>
                               <textarea name="partenaire_en" class="form-control" required rows="8"
                                         placeholder="Enter the project partners and a description of their contributions..." ></textarea>
                           </div>

                           {{--              Version Mixte--}}
                           {{--              Version Mixte--}}

                           <div class="form-group">
                               <label>Banniere / Banner *</label>
                               <br/>
                               <input type="file" name="image_illustration" class="form-control" required>
                           </div>

                           <div class="form-group">
                               <label>Gallerie d'images (facultatif) / Picture gallery <br/> Vous pouvez choisir plusieurs images / you can load more than one picture</label>
                               <input type="file" name="gallery_image[]" class="form-control" multiple>
                           </div>

                       </div>
                       </div>

                       {{--STATISTIQUE--}}
                        <div class="row">


                            <div class="form-group col-md-6">
                                <label>Nombre Hectar restauré</label>
                                <input type="text" name="hectar" class="form-control" placeholder="124537" />
                            </div>

                            <div class="form-group col-md-6">
                                <label>Nombre d'arbre plantés</label>
                                <input type="text" name="arbre" class="form-control" placeholder="124537" />
                            </div>

                            <div class="form-group col-md-6">
                                <label>Secquestration carbonne</label>
                                <input type="text" name="carbonne" class="form-control" placeholder="124537" />
                            </div>
                            <div class="form-group col-md-6">
                                <label>Perdiode de travail / Work period</label>
                                <input type="text" name="periode_travail" class="form-control" placeholder="07/10/2019 - 08/03/2020"  />
                            </div>

                        </div>

                       {{--DIVERS--}}
                       <div class="row" style="margin-bottom: 8%">
                           <div class="col-md-6">
                               <h4>Texte divers</h4>
                               <div id="rich_text_editor_fr"></div>
                               <textarea id="divers_fr" class="d-none"  name="divers_fr"></textarea>
                            </div>

                           <div class="col-md-6">
                               <h4>Other text</h4>
                               <div id="rich_text_editor_en"></div>
                               <textarea id="divers_en" class="d-none" name="divers_en"></textarea>
                            </div>
                       </div>


                       <div class="col-xs-12 justify-content-center">
                               <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                           <h3 class="text-center">
                               <input type="submit" class="btn btn-warning" value="Enregistrer" >
                           </h3>
                       </div>
                   </form>
   </div>

{{--        LISTE DES PROJET--}}
{{--        LISTE DES PROJET--}}
{{--        LISTE DES PROJET--}}
        <div class="container-fluid" id="div_liste">

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Liste des projets / Project list</span>
                </h3>
                <a href="https://www.google.com/maps/d/edit?mid=18k0aQZesopK2g0zHPLJIXxqY7qa97n8L&usp=sharing"class="btn btn-dark" >
                    <h5> Mettre a jour la carte des projets </h5>
                </a>

            </div>


            <table class="text-center table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th>Nom du projet</th>
                    <th>Project name</th>
                    <th> Pays / Country </th>
                    <th> Region </th>
                    <th>Etat/State</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($les_projets as $item_projet)
{{--                        {{dump($item_projet[0])}}--}}
                        <tr>
                            <td style="width: 40%">{{$item_projet[0]['nom']}}</td>
                            <td>{{$item_projet[1]['nom']}}</td>
                            <td>{{$item_projet[0]->region->pays->nom_pays}} </td>
                            <td>{{$item_projet[0]->region->nom_region}} </td>
                            <td>{{$item_projet[0]['etat']}} / {{$item_projet[1]['etat']}}</td>
                            <td style="min-width: 100px !important;" >
                                <a href="{{route( 'editer_projet',[$item_projet[0]['id_versionning'] ] )}}" class="btn btn-warning"> <i class="fa fa-pencil"></i></a>
                                <button type="button" data-toggle="modal" data-target="#ModalSuppression{{$item_projet[0]['id_versionning']}}" class="btn btn-danger"><i class="fa fa-trash"></i></button>
{{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                                <div class="modal fade" id="ModalSuppression{{$item_projet[0]['id_versionning']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Supprimer le projet / Delete project</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Voulez cous vraiment supprimer ce projet ? <br/>
                                                Are you sure ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                <form method="post" action="{{route('effacer_projet',[$item_projet[0]['id_versionning']])}}">
                                                    <input type="hidden" name="_method" value="delete" />
                                                    {{csrf_field()}}
                                                    <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
           </div>
@endsection

@section('script_perso')
    <script>
        // var q1 = new Quill('#rich_text_editor1', {theme: 'snow'});
        function activer_quill() {
                new Quill('#rich_text_editor_fr', {placeholder: 'Entrer le texte...',theme: 'snow'});
                new Quill('#rich_text_editor_en', {placeholder: 'Enter the text...',theme: 'snow'});
        }

        function recuperer_les_donnees(){
                var myEditorFR = document.getElementById('rich_text_editor_fr');
                var myEditorEN = document.getElementById('rich_text_editor_en');

                var text_area_fr = document.getElementById('divers_fr');
                var text_area_en = document.getElementById('divers_en');

                var html_fr = myEditorFR.children[0].innerHTML;
                var html_en = myEditorEN.children[0].innerHTML;

                text_area_fr.innerText = html_fr;
                text_area_en.innerText = html_en;

        }

        function showPays() {
            if(document.getElementById("choisir_pays").style.display == "none"){
                document.getElementById("choisir_pays").style.display = "block"
            }else{
                document.getElementById("choisir_pays").style.display = "none";
            }
        }
        // ****************************************** //
        window.onload = function(){
            activer_quill();
        };
        document.onkeyup = function (event) {
            recuperer_les_donnees();
        };
        document.onmousemove = function (event) {
            recuperer_les_donnees();
        };
        document.onclick = function (event) {
            recuperer_les_donnees();
        };
    </script>
@endsection