@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >Edition du Projet / Edit project</h3>
    </div>

    <div class="container-fluid">
        <h3 style="display: inline-block;"><a href="{{route('gestion_projet')}}" class="btn btn-dark">Retour au projet</a> </h3>

        <div class="col-xs-12" style="border: 2px solid black">
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
               <h4><u>Version Française</u></h4>
                   <form method="post" action="{{route('modifier_projet',[$le_projet[0]['id_versionning']])}}" enctype="multipart/form-data">
                           <div class="form-group">
                               <label>Nom du projet</label>
                                    <input name="nom_fr" class="form-control" value="{{$le_projet[0]['nom']}}" />
                           </div>
                           <div class="form-group">
                               <label>Etat du projet</label>
                               <select name="etat_fr" class="form-control" >
                                   <option value="{{$le_projet[0]['etat']}}" >{{$le_projet[0]['etat']}}</option>
                                   <option value="ouvert" >Ouvert</option>
                                   <option value="fermer" >Fermer</option>
                               </select>
                           </div>
                           <div class="form-group">
                               <label>Resumé du projet</label>
                               <textarea name="resume_fr" class="form-control" rows="8">{{$le_projet[0]['resume']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Methode de travail / Approche de realisation</label>
                               <textarea name="methode_fr" class="form-control" rows="8">{{$le_projet[0]['methode']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Objectif du projet</label>
                               <textarea name="objectif_fr" class="form-control"  rows="8">{{$le_projet[0]['objectif']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Résultats attendus</label>
                               <textarea name="resultat_fr" class="form-control" rows="8">{{$le_projet[0]['resultats_attendus']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Partenaires du projet</label>
                               <textarea name="partenaire_fr" class="form-control"  rows="8">{{$le_projet[0]['partenaires']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Image d'illustration (facultatif) / Illustration picture (optionnal)</label>
                               <img src="{{$le_projet[0]['image_illustration']}}" width="150px" height="150px"/>
                               <br/>
                               <input type="file" name="image_illustration" class="form-control">
                           </div>
                       </div>
                       {{--              Version Anglaise--}}
                       {{--              Version Anglaise--}}
                       <div class="col-sm-6 form-group">
                           <h3><u>English Version</u></h3>
                           <div class="form-group">
                               <label>Project name</label>
                                    <input name="nom_en" class="form-control" value="{{$le_projet[1]['nom']}}" />
                           </div>
                           <div class="form-group">
                               <label>Project state</label>
                               <select name="etat_en" class="form-control" >
                                   <option value="{{$le_projet[1]['etat']}}" >{{$le_projet[1]['etat']}}</option>
                                   <option value="open" >Open</option>
                                   <option value="close" >Close</option>
                               </select>
                           </div>
                           <div class="form-group">
                               <label>Resume project</label>
                               <textarea name="resume_en" class="form-control" rows="8"
                                         placeholder="Enter project summary..." >{{$le_projet[1]['resume']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Work Method / Realisation Approach</label>
                               <textarea name="methode_en" class="form-control" rows="8">{{$le_projet[1]['methode']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Project goals</label>
                               <textarea name="objectif_en" class="form-control" rows="8"
                                         placeholder="Enter Project goals..." >{{$le_projet[1]['objectif']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Expected results</label>
                               <textarea name="resultats_en" class="form-control" rows="8"
                                         placeholder="Enter expected result..." >{{$le_projet[1]['resultats_attendus']}}</textarea>
                           </div>
                           <div class="form-group">
                               <label>Project Partners</label>
                               <textarea name="partenaire_en" class="form-control" rows="8"
                                         placeholder="Enter the project partners and a description of their contributions..." >{{$le_projet[1]['partenaires']}}</textarea>
                           </div>

                           {{--              Version Mixte--}}
                           {{--              Version Mixte--}}

                          {{-- <div class="form-group">
                               <label>Région du pays / Region of country</label>
                               <select name="region_id" class="form-control" >
                                   <option value="{{$le_projet[0]->region->id}}" >{{$le_projet[0]->region->pays->nom_pays}} - {{$le_projet[0]->region->nom_region}}</option>
                                   @foreach($les_regions as $item_region)
                                       <option value="{{$item_region['id']}}">{{$item_region->pays->nom_pays}} - {{$item_region['nom_region']}}</option>
                                   @endforeach
                               </select>
                           </div>--}}

                           <div class="form-group">
                               <label>Région du pays / Region of country</label>

                               <datalist id="liste_region">
                                   @foreach($les_regions as $item_region)
                                       <option value="{{$item_region['nom_region']}}">{{$item_region->pays->nom_pays}} - {{$item_region['nom_region']}}</option>
                                   @endforeach
                               </datalist>
                               <input list="liste_region" placeholder="Choissisez la region" name="nom_region" required class="form-control" value="{{$le_projet[0]->region->nom_region}}">

                               <br/>
                               <input type="checkbox" style="padding:25px" id="nouvelle_region" name="est_une_nouvelle_region" onclick="showPays()">
                               <label>C'est une nouvelle region</label>
                               <br/>
                               <select id="choisir_pays" name="pays_id" class="form-control" style="display: none" autocomplete="no">
                                   <option value="-1" >Choississez le pays</option>
                                   @foreach($les_pays as $item_pays)
                                       <option value="{{$item_pays['id']}}">{{$item_pays->nom_pays}}</option>
                                   @endforeach
                               </select>
                           </div>


                           <div class="form-group">
                               <label>Ajouter des images a la gallerie(facultatif) /Add Picture to gallery
                               <input type="file" name="gallery_image[]" class="form-control" multiple>
                           </div>

                           <div class="form-group">
                               <label>Lien video youtube</label>
                               <textarea name="lien_video" class="form-control"
                                         placeholder="-aller sur youtube cliquer sur partager , -choisissez l'option <<integer>> , cliquez sur copier et coller ici" >{{$le_projet[0]['youtube_video']}}</textarea>
                           </div>
                       </div>
                    <div class="container">

                        {{--STATISTIQUE--}}
                        <div class="row">


                            <div class="form-group col-md-6">
                                <label>Nombre Hectar restauré</label>
                                <input type="text" name="hectar" class="form-control" placeholder="124537" value="{{$le_projet[0]['hectar']}}" />
                            </div>

                            <div class="form-group col-md-6">
                                <label>Nombre d'arbre plantés</label>
                                <input type="text" name="arbre" class="form-control" placeholder="124537" value="{{$le_projet[0]['arbre']}}" />
                            </div>

                            <div class="form-group col-md-6">
                                <label>Secquestration carbonne</label>
                                <input type="text" name="carbonne" class="form-control" placeholder="124537" value="{{$le_projet[0]['carbonne']}}" />
                            </div>

                            <div class="form-group col-md-6">
                                <label>Perdiode de travail / Work period</label>
                                <input type="text" name="periode_travail" class="form-control" placeholder="07/10/2019 - 08/03/2020" value="{{$le_projet[0]['periode_travail']}}" />
                            </div>
                        </div>
                    </div>
                    <div class="container" style="margin-bottom:8%  ">
                        {{--DIVERS--}}
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Texte divers</h4>
                                <div id="rich_text_editor_fr"> {!! $le_projet[0]['text_divers']!!}</div>
                                <textarea id="divers_fr" class="d-none" rows="8" name="divers_fr"> {{$le_projet[0]['text_divers']}}</textarea>
                            </div>

                            <div class="col-md-6">
                                <h4>Texte divers</h4>
                                <div id="rich_text_editor_en"> {!! $le_projet[1]['text_divers'] !!}</div>
                                <textarea id="divers_en" class="d-none"  rows="8" name="divers_en"> {{$le_projet[1]['text_divers']}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <h3>Image presente dans la gallerie / Picture in gallerie</h3>
                        <div class="row">
                            <input type="hidden" name="liste_img_gallery_actuelle" value="{{$le_projet[0]['liste_urls_gallerie_image']}}">
                            @foreach(explode('#',$le_projet[0]['liste_urls_gallerie_image']) as $image)
                                @if($image)
                                    <div class="col-sm-3">
                                        <img src="{{$image}}" width="100px" height="100px"/>
                                        <br/>
                                        <label>Retirer /remove </label><input type="checkbox" name="a_retirer_de_la_gallerie[]" value="{{$image}}">
                                    </div>
                                @endif
                            @endforeach
                            <hr/>
                        </div>
                    </div>
                   <div class="col-xs-12 text-center" style="width: 100%">
                           {{csrf_field()}}
                           <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$le_projet[0]['id']}}" >
                           <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$le_projet[1]['id']}}" >
                           <a href="{{route('gestion_projet')}}" class="btn btn-dark">Retour au projet</a>
                           <input type="submit" class="btn btn-warning" value="Enregistrer les modification / Save " >
                   </div>
                   </form>
               </div>

       </div>
@endsection

@section('script_perso')
    <script>
        // var q1 = new Quill('#rich_text_editor1', {theme: 'snow'});
        function activer_quill() {
            new Quill('#rich_text_editor_fr', {placeholder: 'Entrer le texte...',theme: 'snow'});
            new Quill('#rich_text_editor_en', {placeholder: 'Enter the text...',theme: 'snow'});
        }

        function recuperer_les_donnees(){
            var myEditorFR = document.getElementById('rich_text_editor_fr');
            var myEditorEN = document.getElementById('rich_text_editor_en');

            var text_area_fr = document.getElementById('divers_fr');
            var text_area_en = document.getElementById('divers_en');

            var html_fr = myEditorFR.children[0].innerHTML;
            var html_en = myEditorEN.children[0].innerHTML;

            text_area_fr.innerText = html_fr;
            text_area_en.innerText = html_en;

        }

        function showPays() {
            if(document.getElementById("choisir_pays").style.display == "none"){
                document.getElementById("choisir_pays").style.display = "block"
            }else{
                document.getElementById("choisir_pays").style.display = "none";
            }
        }
        // ****************************************** //
        window.onload = function(){
            activer_quill();
        };
        document.onkeyup = function (event) {
            recuperer_les_donnees();
        };
        document.onmousemove = function (event) {
            recuperer_les_donnees();
        };
        document.onclick = function (event) {
            recuperer_les_donnees();
        };
    </script>
@endsection