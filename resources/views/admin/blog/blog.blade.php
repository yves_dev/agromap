@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h1 class="col-xs-12 text-center titre_section_general" >Blog</h1>
        {!! Session::get('notification','') !!}
    </div>

    <div class="container-fluid">

        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Ajouter / add</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Liste /List </a> </h3>
        </div>

        <div class="container" id="div_ajout">

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Ajouter au blog / Add to blog</span>
                </h3>
            </div>


            <div class="row">
               <div class="col-sm-6">
                {{--              Version Française--}}
                {{--              Version Française--}}
                <h3><u>Version Française</u></h3>
                <form method="post" action="{{route('ajouter_au_blog')}}" enctype="multipart/form-data">


                    <div class="form-group">
                        <label>Titre de l'article</label>
                        <input class="form-control" name="titre_fr" required>
                    </div>

                    <div class="form-group">
                        <label>Resumé</label>
                        <textarea name="resume_fr" class="form-control hidden" rows="4" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Banniere de l'article / Post banner</label>
                        <input type="file" class="form-control" name="banniere" required>
                    </div>
                </div>
                {{--              Version Anglaise--}}
                {{--              Version Anglaise--}}
                <div class="col-sm-6 form-group">
                    <h3><u>English Version</u></h3>

                    <div class="form-group">
                        <label>Article title</label>
                        <input class="form-control" name="titre_en" required>
                    </div>
                    <div class="form-group">
                        <label>Resume</label>
                        <textarea name="resume_en" class="form-control" rows="4" required></textarea>
                    </div>

                    <div class="form-group">
                        <label>Video</label>
                        <textarea name="video" rows="3" class="form-control" required
                                  placeholder="-aller sur youtube cliquer sur partager , -choisissez l'option <<integer>> , cliquez sur copier et coller ici"></textarea>
                    </div>
                </div>

                {{--              Version Mixte--}}
                {{--              Version Mixte--}}
             </div>

            {{--            <div class="container">--}}
            <h3><u>Redigez votre article en français</u></h3>
            <br/>
            <div class="container" id="article_version_fr"></div>
            <div class="row">
                <textarea id="textarea_article_fr" class="d-none" name="article_version_fr" cols="120" required></textarea>
            </div>
            {{--            </div>--}}

            {{--            <div class="container">--}}
            <div class="container">
                <h3><br/><u>Write your post in english</u></h3>
            </div>
            <div class="container" id="article_version_en"></div>
            <textarea id="textarea_article_en" class="d-none" name="article_version_en" cols="120" required></textarea>
            {{--            </div>--}}

            <div class="col-xs-12 justify-content-center">
                <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer">
                <h3 class="text-center">
                    <input type="submit" class="btn btn-warning" value="Enregistrer">
                </h3>
            </div>
        </form>
    </div>
    </div>

    {{--        LISTE DES blog--}}
    {{--        LISTE DES blog--}}
    {{--        LISTE DES blog--}}
    <div class="container-fluid" id="div_liste" >

        <div class="col-xs-12 div_titre_section_specifique" >
            <h3 class="text-center">
                <span class="titre_section_specifique">Liste des articles / Posts list</span>
            </h3>
        </div>

        <table class="text-center table-bordered" style="width: 100%">
            <thead>
            <tr>
                <th>Titre article</th>
                <th>Post title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($les_articles as $item_article)
                {{--                        {{dump($item_article[0])}}--}}
                <tr>
                    <td>{{$item_article[0]['titre']}}</td>
                    <td>{{$item_article[1]['titre']}}</td>
                    <td>
                        <a href="{{route('editer_article',[$item_article[0]['id_versionning']])}}"
                           class="btn btn-warning">Modifier</a>
                        <button type="button" data-toggle="modal"
                                data-target="#ModalSuppression{{$item_article[0]['id_versionning']}}"
                                class="btn btn-danger">Supprimer
                        </button>
                        {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                        <div class="modal fade" id="ModalSuppression{{$item_article[0]['id_versionning']}}"
                             tabindex="-1" role="dialog" aria-labelledby="exampleModallabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModallabel">Supprimer le blog / Delete blog</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Voulez cous vraiment supprimer ce blog ? <br/>
                                        Are you sure ?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer /
                                            Close
                                        </button>
                                        <form method="post"
                                              action="{{route('effacer_un_article',[$item_article[0]['id_versionning']])}}">
                                            <input type="hidden" name="_method" value="delete"/>
                                            {{csrf_field()}}
                                            <input type="submit" class="btn btn-danger"
                                                   value="Oui je suis sûr. / yes i want to continue."/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
@endsection

@section('script_perso')
    <script>
        // var q1 = new Quill('#rich_text_editor1', {theme: 'snow'});
        function activer_quill() {
            var options = {placeholder: 'Composez votre article...', theme: 'snow'};
            new Quill('#article_version_fr', options);
            new Quill('#article_version_en', options);
        }

        function recuperer_les_donnees() {
            var myEditor1 = document.getElementById('article_version_fr');
            var myEditor2 = document.getElementById('article_version_en');
            // console.log(myEditor1);

            var text_area_en = document.getElementById('textarea_article_en');
            var text_area_fr = document.getElementById('textarea_article_fr');

            var html_fr = myEditor1.children[0].innerHTML;
            var html_en = myEditor2.children[0].innerHTML;

            text_area_fr.innerText = html_fr;
            text_area_en.innerText = html_en;

        }

        //****************************************** //
        window.onload = function () {
            activer_quill();

            document.onkeyup = function (event) {
                recuperer_les_donnees();
            };
            document.onmousemove = function (event) {
                recuperer_les_donnees();
            };
            document.onclick = function (event) {
                recuperer_les_donnees();
            };
        };

    </script>
@endsection