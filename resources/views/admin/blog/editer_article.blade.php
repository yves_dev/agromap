@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >Edition de l'article / Edit post</h3>
    </div>

    <div class="container-fluid">
        <h3 style="display: inline-block;"><a href="{{route('gestion_blog')}}" class="btn btn-dark">Retour</a> </h3>

        <div class="col-xs-12" style="border: 2px solid black">
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
               <h4><u>Version Française</u></h4>
               <form method="post" action="{{route('modifier_article',[$article[0]['id_versionning']])}}" enctype="multipart/form-data">

                       <div class="form-group">
                           <h4>Titre de l'article</h4>
                           <input class="form-control" name="titre_fr" value="{{$article[0]['titre']}}">
                       </div>

                       <div class="form-group">
                           <h4>Resumé</h4>
                           <textarea name="resume_fr" class="form-control">{{$article[0]['resume']}}</textarea>
                       </div>
                       <div class="form-group">
                           <h4>Banniere de l'article / Post banner</h4>
                           <img src="{{$article[0]['banniere']}}" width="100px" height="100px">
                           <input type="file" class="form-control" name="banniere">
                       </div>
               </div>
            {{--              Version Anglaise--}}
            {{--              Version Anglaise--}}
                        <div class="col-sm-6 form-group">
                            <h3><u>English Version</u></h3>

                            <div class="form-group">
                                <h4>Article title</h4>
                                <input class="form-control" name="titre_en" value="{{$article[1]['titre']}}">
                            </div>
                            <div class="form-group">
                                <h4>Resume</h4>
                                <textarea name="resume_en" class="form-control">{{$article[1]['resume']}}</textarea>
                            </div>
                            <div class="form-group">
                                <h4>Video</h4>
                                <textarea name="video" rows="3"
                                          class="form-control"
                                          placeholder="-aller sur youtube cliquer sur partager , -choisissez l'option <<integer>> , cliquez sur copier et coller ici"
                                >{{$article[0]['video']}}</textarea>
                            </div>
                            {{--              Version Mixte--}}
                            {{--              Version Mixte--}}
                        </div>


                        {{--            <div class="container">--}}
                            <h3><u>Redigez votre article en français</u></h3>
                            <div class="container" id="article_version_fr" style="margin-bottom: 20px">{!! $article[0]['article'] !!}</div>
                            <textarea id="textarea_article_fr" name="article_version_fr" class="d-none"></textarea>
                            {{--            </div>--}}
                        {{--            <div class="container">--}}
                        <h3><u>Write your post in english</u></h3>
                        <div class="container" id="article_version_en">{!! $article[1]['article'] !!}</div>
                        <textarea id="textarea_article_en" name="article_version_en" class="d-none"></textarea>
                        {{--            </div>--}}

                        <div class="container col-xs-12 justify-content-center">
                            <br/>
                            <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                            <h3 class="text-center">
                                <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$article[0]['id']}}" >
                                <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$article[1]['id']}}" >
                                <input type="hidden" name="_method" value="put" >

                                <a href="{{route('gestion_blog')}}" class="btn btn-dark">Retour</a>
                                <input type="submit" class="btn btn-warning" value="Enregistrer" >
                            </h3>
                        </div>
                </form>
       </div>

       </div>
@endsection

@section('script_perso')
    <script>
        // var q1 = new Quill('#rich_text_editor1', {theme: 'snow'});
        function activer_quill() {
            var options =  { placeholder: 'Composez votre article...',theme: 'snow'};
            new Quill('#article_version_fr',options);
            new Quill('#article_version_en',options);
        }

        function recuperer_les_donnees(){
            var myEditor1 = document.getElementById('article_version_fr');
            var myEditor2 = document.getElementById('article_version_en');
            // console.log(myEditor1);

            var text_area_fr = document.getElementById('textarea_article_fr');
            var text_area_en = document.getElementById('textarea_article_en');

            var html_fr = myEditor1.children[0].innerHTML;
            var html_en = myEditor2.children[0].innerHTML;

            text_area_fr.innerText = html_fr;
            text_area_en.innerText = html_en;

        }
        //****************************************** //
        window.onload = function(){
            activer_quill();

            document.onkeyup = function (event) {
                recuperer_les_donnees();
            };
            document.onmousemove = function (event) {
                recuperer_les_donnees();
            };
            document.onclick = function (event) {
                recuperer_les_donnees();
            };
        };

    </script>
@endsection