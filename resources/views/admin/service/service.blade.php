@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h1 class="col-xs-12 text-center titre_section_general" >Service</h1>
        {!! Session::get('notification','') !!}
    </div>

    <div class="container-fluid">

        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Ajouter / add</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Liste /List </a> </h3>
        </div>


        <div class="container" id="div_ajout">

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Ajouter un service / Add service</span>
                </h3>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {{--              Version Française--}}
                    {{--              Version Française--}}
                    <h3><u>Version Française</u></h3>
                    <form method="post" action="{{route('ajouter_service')}}" enctype="multipart/form-data">

                        <div class="form-group">
                            <label>Denomination du service</label>
                            <input class="form-control" name="nom_service_fr" required>
                        </div>
                        <div class="form-group">
                            <label>Courte description</label>
                            <textarea rows="6" name="description_fr" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Premiere section du service</label>
                            <div id="rich_text_editor_fr_1"></div>
                            <textarea id="text_area_fr_1" name="section_fr_1" class="d-none" required></textarea>
                        </div>


                        <div class="form-group">
                            <label>Deuxieme section du service</label>
                            <div id="rich_text_editor_fr_2"></div>
                            <textarea id="text_area_fr_2" name="section_fr_2" class="d-none" required></textarea>
                        </div>

                        <div class="form-group">
                            <label>Troisieme section du service</label>
                            <div id="rich_text_editor_fr_3"></div>
                            <textarea id="text_area_fr_3" name="section_fr_3" class="d-none" required></textarea>
                        </div>

                </div>
                {{--              Version Anglaise--}}
                {{--              Version Anglaise--}}
                <div class="col-sm-6 form-group">
                    <h3><u>English Version</u></h3>

                    <div class="form-group">
                        <label>Service denomination</label>
                        <input class="form-control" name="nom_service_en" required>
                    </div>

                    <div class="form-group">
                        <label>Short description</label>
                        <textarea rows="6" name="description_en" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label>First paragraph of section description </label>
                        <div id="rich_text_editor_en_1"></div>
                        <textarea id="text_area_en_1" name="section_en_1" class="d-none" required></textarea>
                    </div>

                    <div class="form-group">
                        <label>Second paragraph of section description</label>
                        <div id="rich_text_editor_en_2"></div>
                        <textarea id="text_area_en_2" name="section_en_2" class="d-none" required></textarea>
                    </div>

                    <div class="form-group">
                        <label>Third paragraph of section description</label>
                        <div id="rich_text_editor_en_3"></div>
                        <textarea id="text_area_en_3" name="section_en_3" class="d-none" required></textarea>
                    </div>
                    {{--              Version Mixte--}}
                    {{--              Version Mixte--}}
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <label>Bannière / Banner</label>
                        <br/>
                        <input type="file" name="image_section1" class="form-control" required>
                    </div>
                    <div class="col-md-4">
                        <label>Image section 1</label>
                        <br/>
                        <input type="file" name="image_section2" class="form-control">
                    </div>
                    <div class="col-md-4">
                        <label>Image section 2</label>
                        <br/>
                        <input type="file" name="image_section3" class="form-control">
                    </div>

                </div>

                <div class="container-fluid text-center">
                    {{csrf_field()}}
                    <br/><br/>
                    <input type="submit" class="btn btn-warning" value="Enregistrer">
                </div>
                </form>
            </div>
        </div>

{{--        LISTE DES service--}}
{{--        LISTE DES service--}}
{{--        LISTE DES service--}}
        <div class="container-fluid" id="div_liste" >

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Liste des services / Service list</span>
                </h3>
            </div>


            <table class="text-center table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th>Nom du service</th>
                    <th>service name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($les_services as $item_service)
{{--                        {{dump($item_service[0])}}--}}
                        <tr>
                            <td>{{$item_service[0]['nom']}}</td>
                            <td>{{$item_service[1]['nom']}}</td>
                            <td>
                                <a href="{{route('editer_service',[$item_service[0]['id_versionning']])}}" class="btn btn-warning">Modifier</a>
                                <button type="button" data-toggle="modal" data-target="#ModalSuppression{{$item_service[0]['id_versionning']}}" class="btn btn-danger">Supprimer</button>
{{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                                <div class="modal fade" id="ModalSuppression{{$item_service[0]['id_versionning']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModallabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModallabel">Supprimer le service / Delete service</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Voulez cous vraiment supprimer ce service ? <br/>
                                                Are you sure ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                <form method="post" action="{{route('effacer_service',[$item_service[0]['id_versionning']])}}">
                                                    <input type="hidden" name="_method" value="delete" />
                                                    {{csrf_field()}}
                                                    <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
   </div>
@endsection

@section('script_perso')
    <script>
        // var q1 = new Quill('#rich_text_editor1', {theme: 'snow'});
        function activer_quill() {
            for(i=1;i<=3;i++){
                new Quill('#rich_text_editor_fr_'+i,
                    {
                        placeholder: 'Composez votre article...',
                        theme: 'snow'
                    }
                );
                new Quill('#rich_text_editor_en_'+i, {theme: 'snow'});
            }
        }

        function recuperer_les_donnees(){
            for(i=1;i<=3;i++){
                var myEditor1 = document.getElementById('rich_text_editor_en_'+i);
                var myEditor2 = document.getElementById('rich_text_editor_fr_'+i);

                var text_area_en = document.getElementById('text_area_en_'+i);
                var text_area_fr = document.getElementById('text_area_fr_'+i);

                var html_en = myEditor1.children[0].innerHTML;
                var html_fr = myEditor2.children[0].innerHTML;

                text_area_en.innerText = html_en;
                text_area_fr.innerText = html_fr;
            }

        }
        //****************************************** //
        window.onload = function(){
            activer_quill();
        };
        document.onkeyup = function (event) {
            recuperer_les_donnees();
        };
        document.onmousemove = function (event) {
            recuperer_les_donnees();
        };
        document.onclick = function (event) {
            recuperer_les_donnees();
        };
    </script>
@endsection