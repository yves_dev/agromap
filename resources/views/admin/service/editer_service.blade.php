@extends('partials.base_admin')

@section('style_perso')
    <style>
        h5{
            font-weight: bold;
        }
    </style>
@endsection

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >Edition du service / Edit Service</h3>
    </div>

    <div class="container-fluid">
        <h3 style="display: inline-block;"><a href="{{route('gestion_service')}}" class="btn btn-dark">Retour au service</a> </h3>

        <div class="col-xs-12" style="border: 2px solid black">
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
               <h5><u>Version Française</u></h5>
               <form method="post" action="{{route('modifier_service',[$le_service[0]['id_versionning']])}}" enctype="multipart/form-data">

                   <div class="form-group">
                       <h5>Denomination du service</h5>
                       <input class="form-control" name="nom_service_fr" value="{{$le_service[0]['nom']}}">
                   </div>

                   <div class="form-group">
                       <h5>Courte description</h5>
                       <textarea rows="6" name="description_fr" class="form-control">{{$le_service[0]['description']}}</textarea>
                   </div>

                   <hr style="background-color: #1f2839;"/>
                   <div class="form-group">
                       <h5>Premiere section du service</h5>
                       <div id="rich_text_editor_fr_1" style="margin-bottom: 20px">{!! $le_service[0]['text_section1']!!}</div>
                       <textarea id="text_area_fr_1"  name="section_fr_1" class="d-none" ></textarea>
                   </div>
                   <hr style="background-color: #1f2839;"/>

                   <div class="form-group">
                       <h5>Deuxieme section du service</h5>
                       <div id="rich_text_editor_fr_2" style="margin-bottom: 20px">{!! $le_service[0]['text_section2']!!}</div>
                       <textarea id="text_area_fr_2" name="section_fr_2" class="d-none"  ></textarea>
                   </div>
                   <hr style="background-color: #1f2839;"/>

                   <div class="form-group">
                       <h5>Troisieme section du service</h5>
                       <div id="rich_text_editor_fr_3" style="margin-bottom: 20px">{!! $le_service[0]['text_section3']!!}</div>
                       <textarea id="text_area_fr_3"  name="section_fr_3" class="d-none" ></textarea>
                   </div>

           </div>
            {{--              Version Anglaise--}}
            {{--              Version Anglaise--}}
            <div class="col-sm-6 form-group">
                <h3><u>English Version</u></h3>

                <div class="form-group">
                    <h5>Service denomination</h5>
                    <input class="form-control" name="nom_service_en" value="{{$le_service[1]['nom']}}">
                </div>


                <div class="form-group">
                    <h5>Short description</h5>
                    <textarea rows="6" name="description_en" class="form-control">{{$le_service[1]['description']}}</textarea>
                </div>
                <hr style="background-color: #1f2839;"/>

                <div class="form-group">
                    <h5>First paragraph of section description </h5>
                    <div id="rich_text_editor_en_1" style="margin-bottom: 20px">{!! $le_service[1]['text_section1']!!}</div>
                    <textarea id="text_area_en_1" name="section_en_1"  class="d-none"></textarea>
                </div>
                <hr style="background-color: #1f2839;"/>

                <div class="form-group">
                    <h5>Second paragraph of section description</h5>
                    <div id="rich_text_editor_en_2" style="margin-bottom: 20px">{!! $le_service[1]['text_section2']!!}</div>
                    <textarea id="text_area_en_2" name="section_en_2" class="d-none" ></textarea>
                </div>
                <hr style="background-color: #1f2839;"/>

                <div class="form-group">
                    <h5>Third paragraph of section description</h5>
                    <div id="rich_text_editor_en_3" style="margin-bottom: 20px">{!! $le_service[1]['text_section3']!!}</div>
                    <textarea id="text_area_en_3" name="section_en_3" class="d-none" ></textarea>
                </div>
                {{--              Version Mixte--}}
                {{--              Version Mixte--}}
            </div>

            <div class="row">

                <div class="col-md-4">
                    <h5>Bannière / Banne</h5>
                    <img src="{{$le_service[0]['image_section1']}}" width="100px" height="100px">
                    <input type="file" name="image_section1" class="form-control">
                </div>

                <div class="col-md-4">
                    <h5>Image section 1</h5>
                    @if($le_service[0]['image_section2'] !=null)
                        <img src="{{$le_service[0]['image_section2']}}" width="100px" height="100px">
                        <input type="checkbox" name="retirer_image_section2" id="retirer_img_2">
                        <b for="retirer_img_2">Retirer l'image</b>
                    @endif
                    <br/>
                    <input type="file" name="image_section2" class="form-control">
                </div>
                <div class="col-md-4">
                    <h5>Image section 2 </h5>
                    @if($le_service[0]['image_section3'] != null )
                        <img src="{{$le_service[0]['image_section3']}}" width="100px" height="100px">
                        <input type="checkbox" name="retirer_image_section3" id="retirer_img_3">
                        <b for="retirer_img_3">Retirer l'image</b>
                    @endif
                    <input type="file" name="image_section3" class="form-control">
                </div>

            </div>

            <div class="container-fluid text-center">
                <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$le_service[0]['id']}}" >
                <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$le_service[1]['id']}}" >

                {{csrf_field()}}
                <br/><br/>
                <a href="{{route('gestion_service')}}" class="btn btn-dark">Retour au service</a>
                <input type="submit" class="btn btn-warning" value="Enregistrer" >
            </div>
            </form>
               </div>

       </div>
@endsection

@section('script_perso')
    <script>
        // var q1 = new Quill('#rich_text_editor1', {theme: 'snow'});
        function activer_quill() {
            for(i=1;i<=3;i++){
                new Quill('#rich_text_editor_en_'+i, {theme: 'snow'});
                new Quill('#rich_text_editor_fr_'+i, {theme: 'snow'});
            }
        }

        function recuperer_les_donnees(){
            for(i=1;i<=3;i++){
                var myEditor1 = document.getElementById('rich_text_editor_en_'+i);
                var myEditor2 = document.getElementById('rich_text_editor_fr_'+i);

                var text_area_en = document.getElementById('text_area_en_'+i);
                var text_area_fr = document.getElementById('text_area_fr_'+i);

                var html_en = myEditor1.children[0].innerHTML;
                var html_fr = myEditor2.children[0].innerHTML;

                text_area_en.innerText = html_en;
                text_area_fr.innerText = html_fr;
            }

        }
        //****************************************** //
        window.onload = function(){
            activer_quill();
        };
        document.onkeyup = function (event) {
            recuperer_les_donnees();
        };
        document.onmousemove = function (event) {
            recuperer_les_donnees();
        };
        document.onclick = function (event) {
            recuperer_les_donnees();
        };
    </script>
@endsection