@extends('partials.base_admin')

@section('contenu')


    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >ENTREPRISE</h3>
    </div>

    <div class="container text-center">
        <a href="{{route('gestion_parametre')}}" class="btn btn-inactif">Information general</a> &nbsp;&nbsp;
        <a href="{{route('gestion_apropos')}}" class="btn btn-inactif">A propos</a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_equipe')}}" class="btn btn-actif"> Equipe </a> &nbsp;&nbsp;&nbsp;
        <a href="{{route('gestion_partenaire')}}" class="btn btn-inactif"> Partenaires </a> &nbsp;&nbsp;&nbsp;
    </div>


    <div class="container-fluid">

        <div class="col-xs-12" style="border: 2px solid black">
            <h1 class="text-center"> Equipe </h1>
            {!! Session::get('notification','') !!}
        </div>

        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Ajouter / add</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Liste /List </a> </h3>
        </div>


        <div id="div_ajout" >
            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique"> Ajouter un menbre / Add menber </span>
                </h3>
            </div>
            <div  class="row">
               <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
               <b><u>Version Française</u></b>
                   <form method="post" action="{{route('ajouter_a_equipe')}}" enctype="multipart/form-data">

                       <div class="form-group">
                           <br/>
                           <b>Nom complet du menbre / Menber name</b>
                                <input class="form-control" name="nom_complet" required/>
                       </div>
                       <div class="form-group">
                           <b>Fonction</b>
                           <input class="form-control" name="fonction_fr" placeholder="Dev informatique..." required/>
                       </div>
                       <div class="form-group">
                           <b>Courte Description</b>
                           <textarea name="description_fr" placeholder="" class="form-control"required/></textarea>
                       </div>
           </div>
                       {{--              Version Anglaise--}}
                       {{--              Version Anglaise--}}
               <div class="col-sm-6 form-group">
                       <h3><u>English Version</u></h3>

                       <div class="form-group">
                           <b>his Job</b>
                           <input class="form-control" name="fonction_en" placeholder="Dev informatique..." required/>
                       </div>
                       <div class="form-group">
                           <b>little Description</b>
                           <textarea name="description_en" placeholder="" class="form-control"required/></textarea>
                       </div>

                       <div class="form-group">
                           <b>Sa photo / His photo</b>
                           <br/>
                           <input type="file" name="photo" class="form-control"/>
                       </div>
                   </div>

               <div class="container-fluid text-center">
                   <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                   <input type="submit" class="btn btn-warning" value="Enregistrer" >
               </div>
           </div>
           </form>
       </div>

{{--        LISTE DES equipe--}}
{{--        LISTE DES equipe--}}
{{--        LISTE DES equipe--}}
        <div id="div_liste" >

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Liste des equipe / equipe list</span>
                </h3>
            </div>
            <div class="row">
                 <table class="text-center table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th>Nom du equipe / Menber Name</th>
                    <th>Fonction / Job</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($equipe as $item_menbre)
{{--                        {{dump($item_menbre[0])}}--}}
                        <tr>
                            <td>{{$item_menbre[0]['nom_personne']}}</td>
                            <td>{{$item_menbre[0]['fonction']}} / {{$item_menbre[1]['fonction']}}</td>
                            <td>
                                <button type="button" data-toggle="modal"
                                        data-target="#ModalModificationRegion{{$item_menbre[1]['id_versionning']}}"
                                        class="btn btn-warning">Modifier</button>
                                <button type="button" data-toggle="modal" data-target="#ModalSuppression{{$item_menbre[0]['id_versionning']}}" class="btn btn-danger">Supprimer</button>



                                  {{--                                -----------------------MODAL DE MODIFIACTION-------------------------}}
                                {{--                                -----------------------MODAL DE MODIFIACTION-------------------------}}
                             {{--                                -----------------------MODAL DE MODIFIACTION-------------------------}}

                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="ModalModificationRegion{{$item_menbre[1]['id_versionning']}}">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalh4">Modifier le menbre / Update menber</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="row" method="post" action="{{route('modifier_menbre_equipe',[$item_menbre[0]['id_versionning']])}}" enctype="multipart/form-data">

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <br/>
                                                            <b>Nom complet du menbre / Menber name</b>
                                                            <input class="form-control" name="nom_complet" value="{{$item_menbre[0]['nom_personne']}}" required/>
                                                        </div>
                                                        <div class="form-group">
                                                            <b>Fonction</b>
                                                            <input class="form-control" name="fonction_fr" placeholder="Dev informatique..." value="{{$item_menbre[0]['fonction']}}" required/>
                                                        </div>
                                                        <div class="form-group">
                                                            <b>Courte Description</b>
                                                            <textarea name="description_fr" placeholder="" class="form-control"required>{{$item_menbre[0]['description']}}</textarea>
                                                        </div>
                                                    </div>
                                                    {{--              Version Anglaise--}}
                                                    {{--              Version Anglaise--}}
                                                    <div class="col-sm-6 form-group">
                                                        <h3><u>English Version</u></h3>

                                                        <div class="form-group">
                                                            <b>Sa photo / His photo</b>
                                                            <br/>
                                                            <img src="{{$item_menbre[0]['url_photo']}}" width="100px" height="100px" />
                                                            <input type="file" name="photo" class="form-control"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <b>his Job</b>
                                                            <input class="form-control" name="fonction_en" placeholder="Dev informatique..." value="{{$item_menbre[1]['fonction']}}" required/>
                                                        </div>
                                                        <div class="form-group">
                                                            <b>little Description</b>
                                                            <textarea name="description_en" placeholder="" class="form-control"required>{{$item_menbre[1]['description']}}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="container-fluid">
                                                        <input type="hidden" name="_token" value="{{csrf_token()}}" class="btn btn-primary" value="Enregistrer" >
                                                        <h3 class="text-center">
                                                            <input type="hidden" name="_method" value="put" />
                                                            <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$item_menbre[0]['id']}}" >
                                                            <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$item_menbre[1]['id']}}" >
                                                            <input type="submit" class="btn btn-warning" value="Enregistrer" >
                                                        </h3>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                            </div>
                                </div>
                        </div>
                    </div>
                                {{--                                -----------------------FIN MODAL DE MODIFICATION-------------------------}}

                                {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                                <div class="modal fade" id="ModalSuppression{{$item_menbre[0]['id_versionning']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalh4">Supprimer le equipe / Delete equipe</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Voulez cous vraiment supprimer ce equipe ? <br/>
                                                Are you sure ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                                <form method="post" action="{{route('effacer_menbre_equipe',[$item_menbre[0]['id_versionning']])}}">
                                                    <input type="hidden" name="_method" value="delete" />
                                                    {{csrf_field()}}
                                                    <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
   </div>
@endsection
