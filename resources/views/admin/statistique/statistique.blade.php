@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >PAGE D'ACCEUIL</h3>
    </div>

    <div class="container-fluid text-center">
        <a href="{{route('gestion_slider')}}" class="btn btn-inactif">Slider</a> &nbsp;&nbsp;
        <a href="{{route('gestion_statistique')}}" class="btn btn-actif">Statistiques</a> &nbsp;&nbsp;&nbsp;
    </div>


    <div class="container-fluid">


        <div class="col-xs-12" style="border: 2px solid black">
            <h1 class="text-center"> Statistiques </h1>
            {!! Session::get('notification','') !!}
        </div>


        <div class="container text-center" style="margin: 10px auto">
            <h3 style="display: inline-block;"><a href="#0" onclick="toggle_to_ajout()" class="btn btn-primary" id="btn_ajouter">Ajouter / add</a> </h3>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <h3 style="display: inline-block;"><a href="#1" onclick="toggle_to_liste()" class="btn btn-warning" id="btn_liste">Liste /List </a> </h3>
        </div>

        <div class="container" id="div_ajout">

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Ajouter un statistique / Add statistique</span>
                </h3>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    {{--              Version Française--}}
                    {{--              Version Française--}}
                    <h3><u>Version Française</u></h3>
                    <form method="post" action="{{route('ajouter_au_statistique')}}" enctype="multipart/form-data">

                        <div class="form-group">
                            <b>Titre du statistique</b>
                            <input class="form-control" name="titre_fr" placeholder="Foret restorée" required/>
                        </div>
                        <div class="form-group">
                            <b>chiffre ou pourcentage</b>
                            <input type="number" name="description_fr" class="form-control" placeholder="342735657" required/>
    {{--                        <textarea name="description_fr" class="form-control" placeholder="Description ou chiffre..." required/></textarea>--}}
                        </div>
                </div>
                {{--              Version Anglaise--}}
                {{--              Version Anglaise--}}
                <div class="col-sm-6 form-group">
                    <h3><u>English Version</u></h3>

                    <div class="form-group">
                        <b>Statistique title</b>
                        <input class="form-control" name="titre_en" placeholder="Save tree" required/>
                    </div>
                    <div class="form-group">
                        <b>Image illustration / Illustration image</b>
                        <br/>
                        <input type="file" name="banniere" class="form-control"required/>
                    </div>
                  {{--  <div class="form-group">
                        <b>Numbre / Percent</b>
                        <input type="number" name="description_en" class="form-control" placeholder="176376993"  required/>
                    </div>--}}
                </div>

                {{--              Version Mixte--}}
                {{--              Version Mixte--}}
              {{--  <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <b>Image illustration / Illustration image</b>
                                <br/>
                                <input type="file" name="banniere" class="form-control"required/>
                            </div>
                        </div>
                    </div>
                </div>--}}

                <div class="container-fluid text-center">
                    {{csrf_field()}}
                    <h3 class="text-center">
                        <input type="submit" class="btn btn-warning" value="Enregistrer les modifications" >
                    </h3>
                </div>
                </form>
            </div>
        </div>

        {{--        LISTE DES statistique--}}
        {{--        LISTE DES statistique--}}
        {{--        LISTE DES statistique--}}
        <div class="container" id="div_liste">

            <div class="col-xs-12 div_titre_section_specifique" >
                <h3 class="text-center">
                    <span class="titre_section_specifique">Liste des statistiques / statistic list</span>
                </h3>
            </div>


            <table class="text-center table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th>Nom du statistique / Statistique name</th>
                    <th> Chiffre / Number</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($les_statistiques as $item_statistique)
                    {{--                        {{dump($item_statistique)}}--}}
                    <tr>
                        <td>{{$item_statistique[0]['titre']}} / {{$item_statistique[1]['titre']}}</td>
                        <td>{{$item_statistique[0]['description']}}</td>
                        <td>
                            <a href="{{route('editer_statistique',$item_statistique[0]['id_versionning'])}}" class="btn btn-warning">Modifier</a>
                            <button type="button" data-toggle="modal" data-target="#ModalSuppression{{$item_statistique[0]['id_versionning']}}" class="btn btn-danger">Supprimer</button>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                            <div class="modal fade" id="ModalSuppression{{$item_statistique[0]['id_versionning']}}" tabindex="-1" role="dialog" aria-h4ledby="exampleModalh4" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalh4">Supprimer le statistique / Delete statistique</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Voulez cous vraiment supprimer ce statistique ? <br/>
                                            Are you sure ?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer / Close</button>
                                            <form method="post" action="{{route('effacer_une_statistique',[$item_statistique[0]['id_versionning']])}}">
                                                <input type="hidden" name="_method" value="delete" />
                                                {{csrf_field()}}
                                                <input type="submit" class="btn btn-danger" value="Oui je suis sûr. / yes i want to continue."/>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--                                -----------------------MODAL DE SUPPRESSION-------------------------}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection