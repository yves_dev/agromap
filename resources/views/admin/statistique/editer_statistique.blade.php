@extends('partials.base_admin')

@section('contenu')

    <div class="container-fluid table">
        <h3 class="col-xs-12 text-center titre_section_general" >Edition de la statistique / Edit statistic</h3>
    </div>

    <div class="container-fluid">
        <h3 style="display: inline-block;"><a href="{{route('gestion_statistique')}}" class="btn btn-dark">Retour aux statistique</a> </h3>

        <div class="col-xs-12" style="border: 2px solid black">
            {!! Session::get('notification','') !!}
        </div>

        <div class="row">
           <div class="col-sm-6">
{{--              Version Française--}}
{{--              Version Française--}}
               <h4><u>Version Française</u></h4>
               <form method="post" action="{{route('modifier_statistique',$la_statistique[0]['id_versionning'])}}" enctype="multipart/form-data">

                   <div class="form-group">
                       <b>Titre du slide</b>
                       <input class="form-control" name="titre_fr" value="{{$la_statistique[0]['titre']}}" required/>
                   </div>
                   <div class="form-group">
                       <b>Chiffre</b>
                       <input type="number" name="description_fr" value="{{$la_statistique[0]['description']}}" class="form-control" placeholder="176376993"  required/>
                   </div>
           </div>
            {{--              Version Anglaise--}}
            {{--              Version Anglaise--}}
            <div class="col-sm-6 form-group">
                <h3><u>English Version</u></h3>

                <div class="form-group">
                    <b>Slide title</b>
                    <input class="form-control" name="titre_en" value="{{$la_statistique[1]['titre']}}" required/>
                </div>
                <div class="form-group">
                    <b>Image d'arriere plan / Background image</b>
                    <br/>
                    <img src="{{$la_statistique[0]['url_image']}}" width="100px" height="100px" />
                    <input type="file" name="banniere" class="form-control"/>
                </div>
            </div>


            <div class="container text-center">
                <input type="hidden" class="btn btn-warning" name="id_version_fr" value="{{$la_statistique[0]['id']}}" >
                <input type="hidden" class="btn btn-warning" name="id_version_en" value="{{$la_statistique[1]['id']}}" >
                <input type="hidden" name="_method" value="put" >
                {{csrf_field()}}
                <a href="{{route('gestion_statistique')}}" class="btn btn-dark">Retour aux statistique</a>
                <input type="submit" class="btn btn-warning" value="Enregistrer" >
            </div>
            </form>
               </div>

       </div>
@endsection