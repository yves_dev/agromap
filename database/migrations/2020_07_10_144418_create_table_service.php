<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->text('text_section1');
            $table->text('text_section2');
            $table->text('text_section3');
            $table->string('image_section1');
            $table->string('image_section2')->nullable();
            $table->string('image_section3')->nullable();
            $table->enum('langue',['fr','en']);
            $table->integer('id_versionning');
            $table->index('id_versionning');
            $table->index('langue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
