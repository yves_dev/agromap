<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedTableSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->id();
            $table->string('titre');
            $table->text('description');
            $table->string('url_image');

            $table->enum('langue',['fr','en']);
            $table->integer('id_versionning');

            $table->index('langue');
            $table->index('id_versionning');
            $table->timestamps();
        });

//           $url_slide ='/uploaded_image/slider/slide';
//               for($i=0;$i<5;$i++){
//        //            VERSION FR
//                    $slide = new \App\donnees_acceuil();
//                    $slide->titre=$i.'titre fr';
//                    $slide->description ='description fr';
//                   $slide->url_image =  $url_slide.$i.'.png';
//
//                    $slide->langue = 'fr';
//                    $slide->id_versionning = $i;
//                    $slide->save();
//
//        //            VERSION EN
//                    $slide = new \App\donnees_acceuil();
//                    $slide->titre = $i.'titre en';
//                    $slide->description =' description en';
//                    $slide->url_image = $url_slide.$i.'.png';
//
//                    $slide->langue = 'en';
//                    $slide->id_versionning =i;
//                    $slide->save();
//                }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
