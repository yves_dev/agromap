<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEquipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipe', function (Blueprint $table) {
            $table->id();
            $table->string('url_photo');
            $table->string('nom_personne');
            $table->string('fonction');
            $table->string('description');

            $table->enum('langue',['fr','en']);
            $table->integer('id_versionning');
            $table->index('id_versionning');
            $table->index('langue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipe');
    }
}
