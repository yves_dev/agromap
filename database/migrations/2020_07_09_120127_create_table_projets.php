<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProjets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projets', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('etat');
            $table->text('resume');
            $table->text('objectif');
            $table->text('resultats_attendus');
            $table->text('partenaires');
            $table->text('image_illustration');
            $table->text('liste_urls_gallerie_image');
            $table->enum('langue',['en','fr']);
            $table->integer('id_versionning');
            $table->index('id_versionning');
            $table->index('langue');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projets');
    }
}
