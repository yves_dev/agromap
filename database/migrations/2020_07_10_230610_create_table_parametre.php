<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableParametre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametres', function (Blueprint $table) {
            $table->id();
            $table->string('nom_entreprise');
            $table->string('url_logo_entreprise');
            $table->string('url_fav_icone')->nullable();
            $table->string('email')->nullable();
            $table->string('telephones')->nullable();
            $table->string('adresse')->nullable();
            $table->string('horraires')->nullable();
            $table->string('lien_facebook')->nullable();
            $table->string('lien_twitter')->nullable();
            $table->string('lien_youtube')->nullable();
            $table->string('lien_linkedin')->nullable();
            $table->timestamps();
        });

        $init_parametre = new \App\parametre();
        $init_parametre->nom_entreprise = "Agromap";
        $init_parametre->url_logo_entreprise = "/uploaded_image/parametres/logo_entreprise.jpg";
        $init_parametre->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametres');
    }
}
