<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompleteProjectTableAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projets', function (Blueprint $table) {
            $table->string('hectar')->after('etat')->nullable();
            $table->string('arbre')->after('hectar')->nullable();
            $table->string('carbonne')->after('arbre')->nullable();
            $table->text('methode')->after('carbonne')->nullable();
            $table->string('periode_travail')->after('methode')->nullable();
            $table->text('text_divers')->after('liste_urls_gallerie_image')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projets', function (Blueprint $table) {
            $table->dropColumn('hectar');
            $table->dropColumn('arbre');
            $table->dropColumn('carbonne');
            $table->dropColumn('methode');
            $table->dropColumn('periode_travail');
            $table->dropColumn('text_divers');
        });
    }
}
