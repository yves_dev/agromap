<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedTableStatistique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistiques', function (Blueprint $table) {
            $table->id();
            $table->string('titre');
            $table->string('description');
            $table->string('url_image');

            $table->enum('langue',['fr','en']);
            $table->integer('id_versionning');

            $table->index('langue');
            $table->index('id_versionning');
            $table->timestamps();
        });
        
//        $url_statistique ='/uploaded_image/statistique/stat';
//        for($i=0;$i<5;$i++){
//            //            VERSION FR
//            $statistique = new \App\donnees_acceuil();
//            $statistique->titre=$i.'titre fr';
//            $statistique->description ='description fr';
//            $statistique->url_image =  $url_statistique.$i.'.png';
//
//            $statistique->langue = 'fr';
//            $statistique->id_versionning = $i;
//            $statistique->save();
//
//            //            VERSION EN
//            $statistique = new \App\donnees_acceuil();
//            $statistique->titre = $i.'titre en';
//            $statistique->description =' description en';
//            $statistique->url_image = $url_statistique.$i.'.png';
//
//            $statistique->langue = 'en';
//            $statistique->id_versionning =i;
//            $statistique->save();
//        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistiques');
    }
}
