<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableApropos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apropos', function (Blueprint $table) {
            $table->id();
            $table->string('nom_entreprise')->default('Agromap');
            $table->text('texte_nous');
            $table->text('texte_vision');

            $table->enum('langue',['fr','en']);
            $table->integer('id_versionning');
            $table->index('id_versionning')->default(1);
            $table->index('langue');
            $table->timestamps();
        });

        $init_value_fr = new \App\apropos();
//        VERSION FR
        $init_value_fr->nom_entreprise = 'Agromap fr';
        $init_value_fr->texte_nous = 'qui sommes nous  fr';
        $init_value_fr->texte_vision = 'notre vision fr';
        $init_value_fr->langue = 'fr';
        $init_value_fr->id_versionning = 1;
        $init_value_fr->save();

//        VERSION EN
        $init_value_en = new \App\apropos();
        $init_value_en->nom_entreprise = 'Agromap en';
        $init_value_en->texte_nous = 'who we are en';
        $init_value_en->texte_vision = 'our vision en';
        $init_value_en->langue = 'en';
        $init_value_en->id_versionning =  1;
        $init_value_en->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apropos');
    }
}
