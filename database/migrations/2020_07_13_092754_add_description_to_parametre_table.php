<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionToParametreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parametres', function (Blueprint $table) {
            $table->text('description')
                        ->after('nom_entreprise')
                        ->default('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elit turpis, commodo vel metus sed, auctor hendrerit libero. Nullam id ex dolor. Etiam dictum ex ac justo consequat, quis mollis augue egestas. Sed ut semper massa, et convallis sem. Integer ante orci, commodo et quam consequat, sodales maximus magna. Curabitur maximus lectus scelerisque sapien viverra, a condimentum lorem commodo. Donec rutrum eu mauris ut tincidunt. In gravida elit vel interdum auctor. Pellentesque pellentesque libero lectus, a convallis mauris vehicula eu. Morbi ligula nibh, malesuada quis mi a, efficitur ultrices tortor. Cras imperdiet, mi mattis eleifend tempor, massa libero finibus arcu, eu feugiat ex lacus sit amet dolor. Nam nec eleifend nisi, vitae rhoncus mauris. Aliquam tellus nisl, varius eget risus sed, aliquet iaculis lacus. Donec lobortis blandit iaculis. Nunc rutrum faucibus metus sit amet euismod. Praesent in nulla dictum, sodales metus consequat, ultricies nulla.');
            $table->text('details_horraires')
                        ->after('horraires')
                        ->default( 'Lundi : 07H30 - 17H30<br/>
                                    Mardi : 07H30 - 17H30<br/>
                                    Mercredi : 07H30 - 17H30<br/>
                                    Jeudi : 07H30 - 17H30<br/>
                                    Vendredi : 07H30 - 17H30<br/>
                                    SAMEDI : Fermé<br/>
                                    DIMANCHE : Fermé##
                                    Monday : 07Ham - 05Hpm<br/>
                                    Tuesday : 07Ham - 05Hpm<br/>
                                    Wednesday : 07Ham - 05Hpm<br/>
                                    Thurday : 07Ham - 05Hpm<br/>
                                    Friday : 07Ham - 05Hpm<br/>
                                    Saturday : Close<br/>
                                    Sunday : Close
                        ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parametres', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('details_horraires');
        });
    }
}
